/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eviv.bbox;

import eviv.bb.web.ElectionInfo;

/**
 *
 * @author abrioso
 */
public class BallotBoxUtil {

    public static Boolean deliverVote(java.lang.String electionID, java.lang.String voterID, byte[] vote) {
        eviv.bb.web.BBBBox_Service service = new eviv.bb.web.BBBBox_Service();
        eviv.bb.web.BBBBox port = service.getBBBBoxPort();
        return port.deliverVote(electionID, voterID, vote);
    }

    private static java.util.List<eviv.bb.web.ElectionPartialInfo> getElectionList() {
        eviv.bb.web.BBBBox_Service service = new eviv.bb.web.BBBBox_Service();
        eviv.bb.web.BBBBox port = service.getBBBBoxPort();
        return port.getElectionList();
    }

    private static ElectionInfo getElectionInfo(java.lang.String electionID) {
        eviv.bb.web.BBBBox_Service service = new eviv.bb.web.BBBBox_Service();
        eviv.bb.web.BBBBox port = service.getBBBBoxPort();
        return port.getElectionInfo(electionID);
    }
 
    
    
    
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eviv.bbox.web;

import eviv.bbox.BallotBoxUtil;
import eviv.bbox.entities.Votes;
import java.util.Date;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

/**
 *
 * @author abrioso
 */
@WebService(serviceName = "BallotBox")
@Stateless()
public class BallotBox {

    /** This is a sample web service operation */
    @WebMethod(operationName = "hello")
    public String hello(@WebParam(name = "name") String txt) {
        return "Hello " + txt + " !";
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "sendVote")
    public Boolean sendVote(@WebParam(name = "electionID") String electionID, @WebParam(name = "voterID") String voterID, @WebParam(name = "vote") byte[] vote) {
        //TODO write your implementation code here:
        
        Votes voteBBox = new Votes(electionID, voterID);
        voteBBox.setCastDate(new Date(System.currentTimeMillis()));
        voteBBox.setSent2BB(false);
        voteBBox.setVote(vote);
        
        
         //Gets an Entity Manager & Transaction
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("BallotBoxPU");
        EntityManager em = emf.createEntityManager();
        
        //Persist the Voters
        EntityTransaction trans = em.getTransaction();
        trans.begin();
        em.persist(voteBBox);
        trans.commit();
        
        //Close the Entity Manager & Factory
        em.close();
        emf.close();
        
        Boolean received = false;
        
        received = BallotBoxUtil.deliverVote(electionID, voterID, vote);
                
        if(received != null && received) {
            voteBBox.setSent2BB(true);
     
            //Gets an Entity Manager & Transaction
            emf = Persistence.createEntityManagerFactory("BallotBoxPU");
            em = emf.createEntityManager();

            //Persist the Voters
            trans = em.getTransaction();
            trans.begin();
            em.merge(voteBBox);
            trans.commit();

            //Close the Entity Manager & Factory
            em.close();
            emf.close();         
        }
        
        
        return true;
        
    
    }
        
}

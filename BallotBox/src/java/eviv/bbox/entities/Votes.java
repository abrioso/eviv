/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eviv.bbox.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author abrioso
 */
@Entity
@Table(name = "Votes")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Votes.findAll", query = "SELECT v FROM Votes v"),
    @NamedQuery(name = "Votes.findByElectionID", query = "SELECT v FROM Votes v WHERE v.votesPK.electionID = :electionID"),
    @NamedQuery(name = "Votes.findByVoterID", query = "SELECT v FROM Votes v WHERE v.votesPK.voterID = :voterID"),
    @NamedQuery(name = "Votes.findByCastDate", query = "SELECT v FROM Votes v WHERE v.castDate = :castDate"),
    @NamedQuery(name = "Votes.findBySent2BB", query = "SELECT v FROM Votes v WHERE v.sent2BB = :sent2BB")})
public class Votes implements Serializable {
    @Column(name = "castDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date castDate;
    @Lob
    @Column(name = "vote")
    private byte[] vote;
    @Lob
    @Column(name = "receipt")
    private byte[] receipt;
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected VotesPK votesPK;
    @Column(name = "sent2BB")
    private Boolean sent2BB;

    public Votes() {
    }

    public Votes(VotesPK votesPK) {
        this.votesPK = votesPK;
    }

    public Votes(String electionID, String voterID) {
        this.votesPK = new VotesPK(electionID, voterID);
    }

    public VotesPK getVotesPK() {
        return votesPK;
    }

    public void setVotesPK(VotesPK votesPK) {
        this.votesPK = votesPK;
    }

    public Boolean getSent2BB() {
        return sent2BB;
    }

    public void setSent2BB(Boolean sent2BB) {
        this.sent2BB = sent2BB;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (votesPK != null ? votesPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Votes)) {
            return false;
        }
        Votes other = (Votes) object;
        if ((this.votesPK == null && other.votesPK != null) || (this.votesPK != null && !this.votesPK.equals(other.votesPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "eviv.bbox.entities.Votes[ votesPK=" + votesPK + " ]";
    }

    public Date getCastDate() {
        return castDate;
    }

    public void setCastDate(Date castDate) {
        this.castDate = castDate;
    }

    public byte[] getVote() {
        return vote;
    }

    public void setVote(byte[] vote) {
        this.vote = vote;
    }

    public byte[] getReceipt() {
        return receipt;
    }

    public void setReceipt(byte[] receipt) {
        this.receipt = receipt;
    }
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eviv.verifierapp;

import eviv.commons.rmi.TrusteeRemote;
import eviv.commons.types.TrusteeElection;
import eviv.commons.util.Converters;
import gsd.inescid.crypto.ElGamalEncryption;
import gsd.inescid.crypto.ElGamalKeyFactory;
import gsd.inescid.crypto.ElGamalKeyPair;
import gsd.inescid.crypto.ElGamalKeyParameters;
import gsd.inescid.crypto.ElGamalPrivateKey;
import gsd.inescid.crypto.ElGamalPublicKey;
import gsd.inescid.crypto.util.CryptoUtil;
import gsd.inescid.markpledge3.MP3AggregatedElectionVotes;
import gsd.inescid.markpledge3.MP3Parameters;
import gsd.inescid.markpledge3.MP3VoteAndReceipt;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.math.BigInteger;
import java.rmi.RemoteException;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.SignatureException;
import java.security.SignedObject;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.InvalidNameException;
import javax.naming.ldap.LdapName;
import javax.naming.ldap.Rdn;
import org.bouncycastle.openssl.PEMReader;
import org.bouncycastle.openssl.PasswordFinder;

/**
 *
 * @author abrioso
 */
public class VerifierAppRMI implements TrusteeRemote {

    private X509Certificate cert;
    private PrivateKey kpriv;
    
   
    private ArrayList<TrusteeElection> elections;
    public static final int[] KEY_SIZE = {// p ,  q 
        1024, 512};

    /**
     * Creates & Initializes a new VerifierAppRMI
     * @param certFile The trustee's certificate
     * @param keyFile  The trustee's certificate key
     */
    public VerifierAppRMI(String certFile, String keyFile) {

        InputStream inStream;
        try {
            inStream = new FileInputStream(certFile);
            CertificateFactory cf = CertificateFactory.getInstance("X.509", "BC");
            this.cert = ((X509Certificate) cf.generateCertificate(inStream));
            inStream.close();

            System.out.println("Certificado lido: \n" + cert.toString());

            inStream = new FileInputStream(keyFile);

            KeyFactory kf = KeyFactory.getInstance("X.509", "BC");

            byte[] kprivBytes = new byte[(int) keyFile.length()];

            BufferedReader br = new BufferedReader(new FileReader(keyFile));

            KeyPair kp = (KeyPair) new PEMReader(br, new PasswordFinder() {

                @Override
                public char[] getPassword() {
                    return "trustee".toCharArray();
                }
            }).readObject();


            this.kpriv = kp.getPrivate();

            inStream.close();

            System.out.println("Chave lida: \n" + kpriv.toString());

            //TODO: Tratar melhor as excepcoes
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(VerifierAppRMI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchProviderException ex) {
            Logger.getLogger(VerifierAppRMI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(VerifierAppRMI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (CertificateException ex) {
            Logger.getLogger(VerifierAppRMI.class.getName()).log(Level.SEVERE, null, ex);
        }

        //Import Known Elections
//        elections = VerifierAppUtil.ImportElections(getTrusteeID());
        if (elections == null) {
            elections = new ArrayList<TrusteeElection>();
//            VerifierAppUtil.ExportElections(elections, getTrusteeID());
        }
        

    }

    @Override
    public X509Certificate getCertificate() throws RemoteException {
        return this.cert;
    }

    /**
     * Generate the election's Key
     * @param electionID
     * @return
     * @throws RemoteException
     */
    @Override
    public byte[] generateElectionKey(String electionID) throws RemoteException {


        SecureRandom r = new SecureRandom();
        String hashFunction = "SHA-256";
        ElGamalKeyParameters keyParam = null;
        ElGamalKeyPair generatedkeyPair;
 
        System.out.println("Generating key (p=" + VerifierAppRMI.KEY_SIZE[0]
                + " q=" + VerifierAppRMI.KEY_SIZE[1] + ")");
        try {
            keyParam = new ElGamalKeyParameters(VerifierAppRMI.KEY_SIZE[0], VerifierAppRMI.KEY_SIZE[1], r, hashFunction);
        } catch (GeneralSecurityException ex) {
            Logger.getLogger(VerifierAppRMI.class.getName()).log(Level.SEVERE, null, ex);
        }
        generatedkeyPair = ElGamalKeyFactory.createKeyPair(keyParam, r);
       
        if (addElectionKey(electionID, generatedkeyPair)) {


//TODO: Utilizar Converters.objectToByte(generatedkeyPair.publicKey)            
            ObjectOutputStream outStream = null;
            ByteArrayOutputStream outByteStream = null;
            outByteStream = new ByteArrayOutputStream();
            
            try {
                outStream = new ObjectOutputStream(outByteStream);
                outStream.writeObject(generatedkeyPair.publicKey);
                outStream.flush();
            } catch (IOException ex) {
                Logger.getLogger(VerifierAppRMI.class.getName()).log(Level.SEVERE, null, ex);
            }

            byte[] outBytes = null;
            outBytes = outByteStream.toByteArray();


            return outBytes;
            
        } else {
            return null;
        }

    }

    @Override
    public byte[] signElectionKey(String electionID) throws RemoteException {

        ElGamalPublicKey ekpub = getElectionPubKey(electionID);

//TODO: Utilizar Converters.objectToSignedObject(ekpub, kpriv)            
        
        SignedObject so = null;
        Signature sig;
        try {
            // sig = Signature.getInstance(kpriv.getAlgorithm());
            sig = Signature.getInstance("MD5WithRSA");

            
            so = new SignedObject(ekpub, kpriv, sig);

        } catch (IOException ex) {
            Logger.getLogger(VerifierAppRMI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidKeyException ex) {
            Logger.getLogger(VerifierAppRMI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SignatureException ex) {
            Logger.getLogger(VerifierAppRMI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(VerifierAppRMI.class.getName()).log(Level.SEVERE, null, ex);
        }


//TODO: Utilizar Converters.objectToByte(so)  
        
        ObjectOutputStream outStream = null;
        ByteArrayOutputStream outByteStream = null;
        outByteStream = new ByteArrayOutputStream();

        try {
            outStream = new ObjectOutputStream(outByteStream);
            outStream.writeObject(so);
            outStream.flush();
        } catch (IOException ex) {
            Logger.getLogger(VerifierAppRMI.class.getName()).log(Level.SEVERE, null, ex);
        }

        byte[] outBytes = null;
        outBytes = outByteStream.toByteArray();


        //TODO: Mudar retorno
        return outBytes;

    }

    private ElGamalPublicKey getElectionPubKey(String electionID) {

        //     for (ElectionKeyPair ekp : electionsKeys) {
        for (TrusteeElection te : elections) {
            if (te.getElectionID().equals(electionID)) {
                //              return ekp.getElectionKeyPair().publicKey;
                return te.getKpub();

            }
        }


        return null;

    }

    private String getTrusteeID() {
        String trusteeDN = cert.getSubjectX500Principal().getName();
        String trusteeID = null;
       
        LdapName ldapDN = null;
        try {
            ldapDN = new LdapName(trusteeDN);
        } catch (InvalidNameException ex) {
            Logger.getLogger(VerifierAppRMI.class.getName()).log(Level.SEVERE, null, ex);
        }

        for (Rdn rdn : ldapDN.getRdns()) {

            if (rdn.getType().equals("CN")) {
                trusteeID = rdn.getValue().toString();
            }

            
        }

        return trusteeID;

    }

    @Override
    public boolean addElection(String electionID, eviv.commons.types.ElectionInfo info) throws RemoteException {
    
        TrusteeElection election = getElection(electionID);


        if (election == null) {

            election = new TrusteeElection();

        }
        election.setElectionID(electionID);
        election.setNumCandidates(info.getNumCandidates());
     //   election.setElectionParam(info.);
     //   election.setKpub(generatedkeyPair.publicKey);
     //   election.setKpriv(generatedkeyPair.privateKey);

        if(elections.add(election)) {
//             VerifierAppUtil.ExportElections(elections, getTrusteeID());
             return true;
         } else {
             return false;
         }
         
         
    }
    
    
    @Override
    public boolean setElectionNumVotes(String electionID, int numVotes) throws RemoteException {
    
        TrusteeElection election = getElection(electionID);


        if (election != null) {

        
        election.setNumVotes(numVotes);
//             VerifierAppUtil.ExportElections(elections, getTrusteeID());
             return true;
         } else {
             return false;
         }
         
         
    }
    
    
    
    private boolean addElectionKey(String electionID, ElGamalKeyPair keys) {
        
        
        TrusteeElection election = getElection(electionID);
        
        if (election != null) {

            election.setKpub(keys.publicKey);
            election.setKpriv(keys.privateKey);
            
//TODO: Verificar  se param deve ser kpuv.g ou outro BigInt gerado pelo Trustee?
            election.setElectionParam(new MP3Parameters(keys.publicKey, keys.publicKey.g));
            return true;
        } else {
            return false;
        }
        
    }
    
    @Override
    public byte[] findElectionPubKey(String electionID) {
        ElGamalPublicKey kpub = getElectionPubKey(electionID);
        
//TODO: Utilizar Converters.objectToByte(kpub)            

        
        if (kpub != null) {
            ObjectOutputStream outStream = null;
            ByteArrayOutputStream outByteStream = null;
            outByteStream = new ByteArrayOutputStream();

            try {
                outStream = new ObjectOutputStream(outByteStream);
                outStream.writeObject(kpub);
                outStream.flush();
            } catch (IOException ex) {
                Logger.getLogger(VerifierAppRMI.class.getName()).log(Level.SEVERE, null, ex);
            }

            byte[] outBytes = null;
            outBytes = outByteStream.toByteArray();
            
            
            return outBytes;
        } else {
            return null;
        }
        
    }
    
    public TrusteeElection getElection(String electionID) {
        TrusteeElection election = null;
        
        for(TrusteeElection e : this.elections) {
            if(e.getElectionID().equals(electionID)) {
                election = e;
                break;
            }
        }
        
        return election;
    }
    
    
    @Override
    public byte[] generateChallenge(String electionID) {
        
        BigInteger chal = null;
        
        byte[] chalBytes =null;
        
        SecureRandom r = new SecureRandom();
        
        TrusteeElection election = getElection(electionID);
        
        ElGamalPublicKey kpKey = election.getKpub();
        
        chal = CryptoUtil.generateRandomNumber(kpKey.q, r);
        
        SignedObject so = Converters.objectToSignedObject(chal, kpriv);
        
        chalBytes = Converters.objectToByte(so);
        
        return chalBytes;
    
    }
    
    
    //TODO: Mudar de byte[] para MP3VoteAndReceipt[] 
    @Override
    public byte[] aggregateAndSignVotes(String electionID, byte[] byteVotes) {


        TrusteeElection election = getElection(electionID);

        if (election != null) {

            MP3AggregatedElectionVotes aggVotes = aggregateVotes(electionID, byteVotes);


     //         BigInteger[] decryptedAggVotes = decryptVotesAggregation(aggVotes, election.getKpriv());

//TODO: Utilizar Converters.objectToSignedObject(aggVotes, kpriv)            

            SignedObject so = null;
            Signature sig;
            try {
                // sig = Signature.getInstance(kpriv.getAlgorithm());
                sig = Signature.getInstance("MD5WithRSA");
                so = new SignedObject(aggVotes, kpriv, sig);
            } catch (IOException ex) {
                Logger.getLogger(VerifierAppRMI.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InvalidKeyException ex) {
                Logger.getLogger(VerifierAppRMI.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SignatureException ex) {
                Logger.getLogger(VerifierAppRMI.class.getName()).log(Level.SEVERE, null, ex);
            } catch (NoSuchAlgorithmException ex) {
                Logger.getLogger(VerifierAppRMI.class.getName()).log(Level.SEVERE, null, ex);
            }
            ObjectOutputStream outStream = null;
            ByteArrayOutputStream outByteStream = null;
            outByteStream = new ByteArrayOutputStream();
            try {
                outStream = new ObjectOutputStream(outByteStream);
                outStream.writeObject(so);
                outStream.flush();
            } catch (IOException ex) {
                Logger.getLogger(VerifierAppRMI.class.getName()).log(Level.SEVERE, null, ex);
            }
            byte[] outBytes = null;
            outBytes = outByteStream.toByteArray();

            return outBytes;
        } else {
            return null;
        }

    }
    
    
    private MP3AggregatedElectionVotes aggregateVotes(String electionID, byte[] byteVotes) {
        
        MP3VoteAndReceipt[] votes = null;
        try {
            ByteArrayInputStream bis = new ByteArrayInputStream(byteVotes);
            ObjectInput in;
       
            in = new ObjectInputStream(bis);
       
            votes = (MP3VoteAndReceipt[]) in.readObject();
        
            bis.close();
            in.close();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(VerifierAppRMI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(VerifierAppRMI.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        
        MP3AggregatedElectionVotes aggVotes = null; //new MP3AggregatedElectionVotes(REGISTRY_PORT, votes, null, electionID);
        
       
 //       MP3VoteAndReceipt[] votes = getVotes(rawVotes);
        
        
        TrusteeElection election = getElection(electionID);
        if(election != null) {
            try {
                aggVotes = new MP3AggregatedElectionVotes(election.getNumCandidates(), votes, election.getElectionParam(), "SHA-1");
            } catch (NoSuchAlgorithmException ex) {
                Logger.getLogger(VerifierAppRMI.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        return aggVotes;
        
    }
    
    @Override
       public byte[] decryptAggregatedVotes(String electionID, byte[] byteAggVotes) {


        TrusteeElection election = getElection(electionID);

        if (election != null) {

            
//TODO: Utilizar Converters.byteToObject(byteAggVotes)            

            
        MP3AggregatedElectionVotes aggVotes = null;
                   
        try {
                ByteArrayInputStream bis = new ByteArrayInputStream(byteAggVotes);
                ObjectInput in = new ObjectInputStream(bis);
            
                SignedObject signedVote = (SignedObject) in.readObject();
            
                bis.close();
                in.close();
                
                aggVotes = (MP3AggregatedElectionVotes) signedVote.getObject();
            
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(VerifierAppRMI.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(VerifierAppRMI.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            
            //TODO: We also need to decrypt the votes
            BigInteger[] decryptedAggVotes = decryptVotesAggregation(aggVotes, election.getKpriv());

//TODO: Gerar VerificationData referente a decifra dos AggVotes 

//TODO: Utilizar Converters.objectToSignedObject(decryptedAggVotes, kpriv)             
            
            SignedObject so = null;
            Signature sig;
            try {
                // sig = Signature.getInstance(kpriv.getAlgorithm());
                sig = Signature.getInstance("MD5WithRSA");
                so = new SignedObject(decryptedAggVotes, kpriv, sig);
            } catch (IOException ex) {
                Logger.getLogger(VerifierAppRMI.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InvalidKeyException ex) {
                Logger.getLogger(VerifierAppRMI.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SignatureException ex) {
                Logger.getLogger(VerifierAppRMI.class.getName()).log(Level.SEVERE, null, ex);
            } catch (NoSuchAlgorithmException ex) {
                Logger.getLogger(VerifierAppRMI.class.getName()).log(Level.SEVERE, null, ex);
            }
            
//TODO: Utilizar Converters.objectToByte(so) 
            
            ObjectOutputStream outStream = null;
            ByteArrayOutputStream outByteStream = null;
            outByteStream = new ByteArrayOutputStream();
            try {
                outStream = new ObjectOutputStream(outByteStream);
                outStream.writeObject(so);
                outStream.flush();
            } catch (IOException ex) {
                Logger.getLogger(VerifierAppRMI.class.getName()).log(Level.SEVERE, null, ex);
            }
            byte[] outBytes = null;
            outBytes = outByteStream.toByteArray();

            return outBytes;
        } else {
            return null;
        }

    }
    
    
    
    // RJoaquim Code - MP3PerformanceTest
    private static BigInteger[] decryptVotesAggregation(MP3AggregatedElectionVotes aggregatedVotes, ElGamalPrivateKey kpri)
	{
		ElGamalEncryption encryptedVotes[] = aggregatedVotes.aggregatedVotes;
		BigInteger results[] = new BigInteger[encryptedVotes.length]; // array with one position for each candidate
		
		for(int i=0; i<encryptedVotes.length; i++)
			results[i] = kpri.decryptQOrderMessage(encryptedVotes[i]);
		return results;
	}

    

   
    // RJoaquim Code - MP3PerformanceTest
    private static HashMap<BigInteger,Integer> createDecodingMap(int numberOfVotes, MP3Parameters mp3Param)
	{
		HashMap<BigInteger,Integer> map = new HashMap<BigInteger,Integer>((2*numberOfVotes)+1);
		BigInteger powers;
		
		map.put(BigInteger.ONE, 0);
		//positive values
		powers = mp3Param.BASE_VOTE_GENERATOR;
		map.put(powers, 1);
		for(int i=2; i<=numberOfVotes; i++)
		{
			powers = (powers.multiply(mp3Param.BASE_VOTE_GENERATOR)).mod(mp3Param.ELECTION_PUBLIC_KEY.p);
			map.put(powers, i);
		}
		
		//negative values
		powers = mp3Param.BASE_VOTE_GENERATOR_INVERSE;
		map.put(powers, -1);
		numberOfVotes = -numberOfVotes;
		for(int i=-2; i>=numberOfVotes; i--)
		{
			powers = (powers.multiply(mp3Param.BASE_VOTE_GENERATOR_INVERSE)).mod(mp3Param.ELECTION_PUBLIC_KEY.p);
			map.put(powers, i);
		}
			
		return map;
	}
    
    // RJoaquim Code - MP3PerformanceTest
    private static int[] translateResults(BigInteger[] decryptedResults, 
									HashMap<BigInteger, Integer> map, int numberOfAggregatedVotes)
	{
		int translatedResults[] = new int[decryptedResults.length];
		for(int i=0; i< translatedResults.length; i++)
			translatedResults[i] = (map.get(decryptedResults[i]) + numberOfAggregatedVotes) / 2;
		return translatedResults;
	}
    
    
    
    private int[] generateResults(String electionID, byte[] raw_aggVotes, byte[] raw_decryptedAggVotes) {
        
        int[] results = null;
        
        TrusteeElection election = getElection(electionID);
        
        MP3AggregatedElectionVotes aggVotes = null;
        
        BigInteger[] decryptedAggVotes = null;
        
        try {
                ByteArrayInputStream bis = new ByteArrayInputStream(raw_aggVotes);
                ObjectInput in = new ObjectInputStream(bis);
            
                SignedObject signedVote = (SignedObject) in.readObject();
            
                bis.close();
                in.close();
                
                aggVotes = (MP3AggregatedElectionVotes) signedVote.getObject();
            
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(VerifierAppRMI.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(VerifierAppRMI.class.getName()).log(Level.SEVERE, null, ex);
            }
        
        try {
                ByteArrayInputStream bis = new ByteArrayInputStream(raw_decryptedAggVotes);
                ObjectInput in = new ObjectInputStream(bis);
            
                SignedObject signedVote = (SignedObject) in.readObject();
            
                bis.close();
                in.close();
                
                decryptedAggVotes = (BigInteger[]) signedVote.getObject();
            
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(VerifierAppRMI.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(VerifierAppRMI.class.getName()).log(Level.SEVERE, null, ex);
            }
        

        if(election != null && aggVotes != null) {

            //TODO: Verificar se deve ser o Number_Of_Aggregated_Votes ou o Number_Of_Votes 
            // -> Os valores podem ser diferentes se houverem votos invalidos 
            HashMap<BigInteger,Integer> map = createDecodingMap(election.getNumVotes(), election.getElectionParam());
            
            results = translateResults(decryptedAggVotes, map, aggVotes.NUMBER_OF_AGGREGATED_VOTES);
		
            

            
                
        }
        return results;
    }
    
   
    
    @Override
    public byte[] generateAndSignResults(String electionID, byte[] raw_aggVotes, byte[] raw_decryptedAggVotes) {
        
        int[] results = generateResults(electionID, raw_aggVotes, raw_decryptedAggVotes);
        
       
         SignedObject so = null;
            Signature sig;
            try {
                // sig = Signature.getInstance(kpriv.getAlgorithm());
                sig = Signature.getInstance("MD5WithRSA");
                so = new SignedObject(results, kpriv, sig);
            } catch (IOException ex) {
                Logger.getLogger(VerifierAppRMI.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InvalidKeyException ex) {
                Logger.getLogger(VerifierAppRMI.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SignatureException ex) {
                Logger.getLogger(VerifierAppRMI.class.getName()).log(Level.SEVERE, null, ex);
            } catch (NoSuchAlgorithmException ex) {
                Logger.getLogger(VerifierAppRMI.class.getName()).log(Level.SEVERE, null, ex);
            }
            ObjectOutputStream outStream = null;
            ByteArrayOutputStream outByteStream = null;
            outByteStream = new ByteArrayOutputStream();
            try {
                outStream = new ObjectOutputStream(outByteStream);
                outStream.writeObject(so);
                outStream.flush();
            } catch (IOException ex) {
                Logger.getLogger(VerifierAppRMI.class.getName()).log(Level.SEVERE, null, ex);
            }
            byte[] outBytes = null;
            outBytes = outByteStream.toByteArray();

            return outBytes;
        
        
        
        
    }
    
    
    
     
}

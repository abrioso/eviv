/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eviv.verifierapp;

import gsd.inescid.crypto.ElGamalKeyPair;
import java.io.Serializable;

/**
 *
 * @author abrioso
 */
@Deprecated
public class ElectionKeyPair implements Serializable{
    
    private String electionID;
    private ElGamalKeyPair electionKeyPair;

    public ElectionKeyPair(String electionID, ElGamalKeyPair electionKeyPair) {
        this.electionID = electionID;
        this.electionKeyPair = electionKeyPair;
    }
    
    
    

    /**
     * Get the value of electionID
     *
     * @return the value of electionID
     */
    public String getElectionID() {
        return electionID;
    }

    /**
     * Set the value of electionID
     *
     * @param electionID new value of electionID
     */
    public void setElectionID(String electionID) {
        this.electionID = electionID;
    }

    
    

    /**
     * Get the value of electionKeyPair
     *
     * @return the value of electionKeyPair
     */
    public ElGamalKeyPair getElectionKeyPair() {
        return electionKeyPair;
    }

    /**
     * Set the value of electionKeyPair
     *
     * @param electionKeyPair new value of electionKeyPair
     */
    public void setElectionKeyPair(ElGamalKeyPair electionKeyPair) {
        this.electionKeyPair = electionKeyPair;
    }

}

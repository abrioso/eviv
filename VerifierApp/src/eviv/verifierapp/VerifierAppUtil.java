/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eviv.verifierapp;

import eviv.bb.web.ElectionInfo;
import eviv.bb.web.ElectionPartialInfo;
import eviv.bb.web.Votes;
import eviv.commons.rmi.TrusteeRemote;
import eviv.commons.types.TrusteeElection;
import eviv.commons.util.Converters;
import gsd.inescid.markpledge3.MP3AggregatedElectionVotes;
import gsd.inescid.markpledge3.MP3VoteAndReceipt;
import java.awt.Component;
import java.awt.FileDialog;
import java.awt.Frame;
import java.io.*;
import java.math.BigInteger;
import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.security.NoSuchAlgorithmException;
import java.security.SignedObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

/**
 *
 * @author abrioso
 */
public class VerifierAppUtil {

    public static void loadBC() {
        if (java.security.Security.getProvider("BC") == null) {
            java.security.Security.addProvider(new BouncyCastleProvider());
        }
    }

        public static byte[] getElectionPubKey(String electionID) {

        Registry trusteeRegistry = null;
        TrusteeRemote trusteeRemote = null;

        byte[] kpub = null;

        try {
            trusteeRegistry = LocateRegistry.getRegistry(TrusteeRemote.REGISTRY_PORT);

            trusteeRemote = (TrusteeRemote) trusteeRegistry.lookup(TrusteeRemote.REGISTRY_NAME);

            kpub = trusteeRemote.findElectionPubKey(electionID);

        } catch (RemoteException ex) {
            Logger.getLogger(VerifierAppUtil.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NotBoundException ex) {
            Logger.getLogger(VerifierAppUtil.class.getName()).log(Level.SEVERE, null, ex);
        }

        return kpub;
    }

        
        
    private static TrusteeRemote connect2TrusteeRMI() {

        Registry trusteeRegistry = null;
        TrusteeRemote trusteeRemote = null;

        try {
            trusteeRegistry = LocateRegistry.getRegistry(TrusteeRemote.REGISTRY_PORT);

            trusteeRemote = (TrusteeRemote) trusteeRegistry.lookup(TrusteeRemote.REGISTRY_NAME);

        } catch (RemoteException ex) {
            Logger.getLogger(VerifierAppUtil.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NotBoundException ex) {
            Logger.getLogger(VerifierAppUtil.class.getName()).log(Level.SEVERE, null, ex);
        }

        return trusteeRemote;
    }
        
    public static byte[] aggregateVotes(String electionID, List<Votes> rawVotes) {
        byte[] signedAggVotes = null;

        byte[] byteVotes = null;
       
        
        //TODO: Convert List<Votes> to List<MP3VoteAndReceipt>
        
        MP3VoteAndReceipt[] votes = getVotes(rawVotes);
        
        
        
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ObjectOutput out = new ObjectOutputStream(bos);

            out.writeObject(votes);
            byteVotes = bos.toByteArray();

            out.close();
            bos.close();

        } catch (IOException ex) {
            Logger.getLogger(VerifierAppUtil.class.getName()).log(Level.SEVERE, null, ex);
        }

        TrusteeRemote trusteeRemote = connect2TrusteeRMI();
        
        
        if (trusteeRemote != null && byteVotes != null) {
            try {
                boolean setElectionNumVotes = trusteeRemote.setElectionNumVotes(electionID, votes.length);
                signedAggVotes = trusteeRemote.aggregateAndSignVotes(electionID, byteVotes);
            } catch (RemoteException ex) {
                Logger.getLogger(VerifierAppUtil.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        return signedAggVotes;
        
    }
    
    @Deprecated
    public static byte[] decryptAggregateVotes(String electionID, byte[] aggVotesBytes) {
       
        TrusteeRemote trusteeRemote = connect2TrusteeRMI();
        byte[] decryptedAggVotes = null;
        
        
        if (trusteeRemote != null && aggVotesBytes != null) {
            try {
                decryptedAggVotes = trusteeRemote.decryptAggregatedVotes(electionID, aggVotesBytes);
            } catch (RemoteException ex) {
                Logger.getLogger(VerifierAppUtil.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        return decryptedAggVotes;
        
        
    }

    
    private MP3AggregatedElectionVotes aggregateVotes(String electionID, byte[] byteVotes) {
        
        MP3VoteAndReceipt[] votes = null;
        try {
            ByteArrayInputStream bis = new ByteArrayInputStream(byteVotes);
            ObjectInput in;
       
            in = new ObjectInputStream(bis);
       
            votes = (MP3VoteAndReceipt[]) in.readObject();
        
            bis.close();
            in.close();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(VerifierAppRMI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(VerifierAppRMI.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        MP3AggregatedElectionVotes aggVotes = null; //new MP3AggregatedElectionVotes(REGISTRY_PORT, votes, null, electionID);
        //       MP3VoteAndReceipt[] votes = getVotes(rawVotes);
        
        int numCand = getElectionInfo(electionID).getNumCandidates();
        
//TODO: Corrigir
//                aggVotes = new MP3AggregatedElectionVotes(numCand, votes, election.getElectionParam(), "SHA-1");
            
        
        
        return aggVotes;
        
        
        
    }
    
      private static MP3VoteAndReceipt[] getVotes(List<Votes> rawVotes) {

        ArrayList<MP3VoteAndReceipt> votes_list = new ArrayList<MP3VoteAndReceipt>();
        
        for (Votes v : rawVotes) {
            
            MP3VoteAndReceipt vote = null; 
            
            try {
                ByteArrayInputStream bis = new ByteArrayInputStream(v.getVoteInfo());
                ObjectInput in = new ObjectInputStream(bis);
            
                SignedObject signedVote = (SignedObject) in.readObject();
            
                bis.close();
                in.close();
                
                vote = (MP3VoteAndReceipt) signedVote.getObject();
            
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(VerifierAppRMI.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(VerifierAppRMI.class.getName()).log(Level.SEVERE, null, ex);
            }

                        
                votes_list.add(vote);
            
        }

        MP3VoteAndReceipt[] votes = votes_list.toArray(new MP3VoteAndReceipt[0]);

        return votes;
        

    }
    
   
    
    public static void refreshPane(JPanel panel) {
        
   //     List<String> electionList = null;
   //     electionList = getElectionList();
        
        
        if (panel.isShowing()) {
            
            List<ElectionPartialInfo> elections = VerifierAppUtil.getElectionList();

              
                if (!elections.isEmpty()) {

                                //TODO - Distinguish which Panel is selected

                for (Component c : panel.getComponents()) {
                    if (
                            (c != null)
                         //   && (c.getClass() == JComboBox.class)
                            && (c.getName() != null)
                            && (c.getName().equals("electionIDSelection"))) {
                        JComboBox cbox = (JComboBox) c;
                        cbox.removeAllItems();
                        for (ElectionPartialInfo eid : elections) {
                            cbox.addItem(eid.getElectionID() + " - " + eid.getElectionDesc());
                        }
                        
//                        cbox.setSelectedIndex(0);
                        
                    }
                }

            }

        }
    }
    
    
    
    
    private static eviv.commons.types.ElectionInfo ConvertEInfo(eviv.bb.web.ElectionInfo info) {
       
        eviv.commons.types.ElectionInfo electionInfo = new eviv.commons.types.ElectionInfo();
        
        electionInfo.setElectionID(info.getElectionID());
        electionInfo.setElectionDesc(info.getElectionDesc());
        electionInfo.setNumCandidates(info.getNumCandidates());
        electionInfo.setElectionCandidates(new ArrayList<String>(info.getElectionCandidates()));
   //     electionInfo.setElectionDates(info.getElectionDates().toArray(new Date[0]));
        electionInfo.setIsPhaseFinished(info.getIsPhaseFinished().toArray(new Boolean[4]));
        electionInfo.setElectionPubKey(info.getElectionPubKey().toArray(new Byte[0]));
        
        return electionInfo;
        
    }
        
    
    public static int[] generateResults(String electionID) {
        
        int[] results = null;
        
        byte[] aggVotes = getAggregatedVotes(electionID);
        byte[] decryptedAggVotes = getDecryptedAggregatedVotes(electionID);
        
        
        TrusteeRemote connect2TrusteeRMI = connect2TrusteeRMI();
        
        byte[] generateAndSignResults = null;
        
        try {
            generateAndSignResults = connect2TrusteeRMI.generateAndSignResults(electionID, aggVotes, decryptedAggVotes);
 //           setResults(electionID, generateAndSignResults);
        } catch (RemoteException ex) {
            Logger.getLogger(VerifierAppUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        try {
                ByteArrayInputStream bis = new ByteArrayInputStream(generateAndSignResults);
                ObjectInput in = new ObjectInputStream(bis);
            
                SignedObject signedVote = (SignedObject) in.readObject();
            
                bis.close();
                in.close();
                
                results = (int[]) signedVote.getObject();
            
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(VerifierAppRMI.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(VerifierAppRMI.class.getName()).log(Level.SEVERE, null, ex);
            }
        
        
                
        return results;
        
    }


    //TODO: Corrigir
    private int[] generateResults(String electionID, byte[] raw_aggVotes, byte[] raw_decryptedAggVotes) {
        
        int[] results = null;
        
    /*    TrusteeElection election = getElection(electionID);
        
        MP3AggregatedElectionVotes aggVotes = null;
        
        BigInteger[] decryptedAggVotes = null;
        
        try {
                ByteArrayInputStream bis = new ByteArrayInputStream(raw_aggVotes);
                ObjectInput in = new ObjectInputStream(bis);
            
                SignedObject signedVote = (SignedObject) in.readObject();
            
                bis.close();
                in.close();
                
                aggVotes = (MP3AggregatedElectionVotes) signedVote.getObject();
            
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(VerifierAppRMI.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(VerifierAppRMI.class.getName()).log(Level.SEVERE, null, ex);
            }
        
        try {
                ByteArrayInputStream bis = new ByteArrayInputStream(raw_decryptedAggVotes);
                ObjectInput in = new ObjectInputStream(bis);
            
                SignedObject signedVote = (SignedObject) in.readObject();
            
                bis.close();
                in.close();
                
                decryptedAggVotes = (BigInteger[]) signedVote.getObject();
            
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(VerifierAppRMI.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(VerifierAppRMI.class.getName()).log(Level.SEVERE, null, ex);
            }
        

        if(election != null && aggVotes != null) {

            //TODO: Verificar se deve ser o Number_Of_Aggregated_Votes ou o Number_Of_Votes 
            // -> Os valores podem ser diferentes se houverem votos invalidos 
            HashMap<BigInteger,Integer> map = createDecodingMap(election.getNumVotes(), election.getElectionParam());
            
            results = translateResults(decryptedAggVotes, map, aggVotes.NUMBER_OF_AGGREGATED_VOTES);
		
            

            
                
        } */
        return results;
    }
    
     
    
    


    private static java.util.List<eviv.bb.web.ElectionPartialInfo> getElectionList() {
        eviv.bb.web.BBVerifier_Service service = new eviv.bb.web.BBVerifier_Service();
        eviv.bb.web.BBVerifier port = service.getBBVerifierPort();
        return port.getElectionList();
    }

    public static ElectionInfo getElectionInfo(java.lang.String electionID) {
        eviv.bb.web.BBVerifier_Service service = new eviv.bb.web.BBVerifier_Service();
        eviv.bb.web.BBVerifier port = service.getBBVerifierPort();
        return port.getElectionInfo(electionID);
    }

    public static java.util.List<eviv.bb.web.Votes> getVotes(java.lang.String electionID) {
        eviv.bb.web.BBVerifier_Service service = new eviv.bb.web.BBVerifier_Service();
        eviv.bb.web.BBVerifier port = service.getBBVerifierPort();
        return port.getVotes(electionID);
    }

    public static byte[] getAggregatedVotes(java.lang.String electionID) {
        eviv.bb.web.BBVerifier_Service service = new eviv.bb.web.BBVerifier_Service();
        eviv.bb.web.BBVerifier port = service.getBBVerifierPort();
        return port.getAggregatedVotes(electionID);
    }

    public static byte[] getDecryptedAggregatedVotes(java.lang.String electionID) {
        eviv.bb.web.BBVerifier_Service service = new eviv.bb.web.BBVerifier_Service();
        eviv.bb.web.BBVerifier port = service.getBBVerifierPort();
        return port.getDecryptedAggregatedVotes(electionID);
    }

    public static byte[] getResults(java.lang.String electionID) {
        eviv.bb.web.BBVerifier_Service service = new eviv.bb.web.BBVerifier_Service();
        eviv.bb.web.BBVerifier port = service.getBBVerifierPort();
        return port.getResults(electionID);
    }

    
    

}

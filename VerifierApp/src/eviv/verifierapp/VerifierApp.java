/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eviv.verifierapp;

import eviv.verifierapp.ui.VerifierAppGUI;

/**
 *
 * @author abrioso
 */
public class VerifierApp {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
          java.awt.EventQueue.invokeLater(new Runnable() {

            @Override
            public void run() {
                new VerifierAppGUI().setVisible(true);
            }
        });
    }
}

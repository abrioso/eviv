/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eviv.er;

import eviv.er.bb.services.ElectionInfo;

/**
 *
 * @author abrioso
 */
public class ElectionRegistrarUtil {

    public static java.util.List<eviv.er.bb.services.ElectionPartialInfo> getElectionList() {
        eviv.er.bb.services.BBER_Service service = new eviv.er.bb.services.BBER_Service();
        eviv.er.bb.services.BBER port = service.getBBERPort();
        return port.getElectionList();
    }

    public static ElectionInfo getElectionInfo(java.lang.String electionID) {
        eviv.er.bb.services.BBER_Service service = new eviv.er.bb.services.BBER_Service();
        eviv.er.bb.services.BBER port = service.getBBERPort();
        return port.getElectionInfo(electionID);
    }

    public static Boolean registerVoter(java.lang.String electionID, java.lang.String voterID) {
        eviv.er.bb.services.BBER_Service service = new eviv.er.bb.services.BBER_Service();
        eviv.er.bb.services.BBER port = service.getBBERPort();
        return port.registerVoter(electionID, voterID);
    }
    
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eviv.er.web;

import eviv.er.ElectionRegistrarUtil;
import eviv.er.bb.services.ElectionInfo;
import eviv.er.bb.services.ElectionPartialInfo;
import eviv.er.entities.RegisteredVoters;
import java.util.Date;
import java.util.List;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

/**
 *
 * @author abrioso
 */
@WebService(serviceName = "ElectionRegistration")
@Stateless()
public class ElectionRegistration {

    /** This is a sample web service operation */
    @WebMethod(operationName = "hello")
    public String hello(@WebParam(name = "name") String txt) {
        return "Hello " + txt + " !";
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "getElectionList")
    public List<eviv.er.bb.services.ElectionPartialInfo> getElectionList() {
        
        List<ElectionPartialInfo> elections = null;
        
        elections = ElectionRegistrarUtil.getElectionList();
        
        return elections;
        
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "getElectionInfo")
    public ElectionInfo getElectionInfo(@WebParam(name = "electionID") String electionID) {
        
        ElectionInfo elInfo = null;
        
        elInfo = ElectionRegistrarUtil.getElectionInfo(electionID);
        
        return elInfo;
        
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "registerVoter")
    public Boolean registerVoter(@WebParam(name = "electionID") String electionID, @WebParam(name = "voterID") String voterID) {
    
        RegisteredVoters voter = new RegisteredVoters(electionID, voterID);
        voter.setRegistryDate(new Date(System.currentTimeMillis()));
        voter.setSent2BB(false);
        
        
        
         //Gets an Entity Manager & Transaction
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("ElectionRegistrarPU");
        EntityManager em = emf.createEntityManager();
        
        //Persist the Voters
        EntityTransaction trans = em.getTransaction();
        trans.begin();
        em.persist(voter);
        trans.commit();
        
        //Close the Entity Manager & Factory
        em.close();
        emf.close();
        
        Boolean received = false;
        
        received = ElectionRegistrarUtil.registerVoter(electionID, voterID);
        
        if(received != null && received) {
            voter.setSent2BB(true);
     
            //Gets an Entity Manager & Transaction
            emf = Persistence.createEntityManagerFactory("ElectionRegistrarPU");
            em = emf.createEntityManager();

            //Persist the Voters
            trans = em.getTransaction();
            trans.begin();
            em.merge(voter);
            trans.commit();

            //Close the Entity Manager & Factory
            em.close();
            emf.close();         
        }
        
        
        return true;
        
    
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eviv.er.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author abrioso
 */
@Entity
@Table(name = "RegisteredVoters")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RegisteredVoters.findAll", query = "SELECT r FROM RegisteredVoters r"),
    @NamedQuery(name = "RegisteredVoters.findByElectionID", query = "SELECT r FROM RegisteredVoters r WHERE r.registeredVotersPK.electionID = :electionID"),
    @NamedQuery(name = "RegisteredVoters.findByVoterID", query = "SELECT r FROM RegisteredVoters r WHERE r.registeredVotersPK.voterID = :voterID"),
    @NamedQuery(name = "RegisteredVoters.findByRegistryDate", query = "SELECT r FROM RegisteredVoters r WHERE r.registryDate = :registryDate"),
    @NamedQuery(name = "RegisteredVoters.findBySent2BB", query = "SELECT r FROM RegisteredVoters r WHERE r.sent2BB = :sent2BB")})
public class RegisteredVoters implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected RegisteredVotersPK registeredVotersPK;
    @Column(name = "registryDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date registryDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "sent2BB")
    private boolean sent2BB;

    public RegisteredVoters() {
    }

    public RegisteredVoters(RegisteredVotersPK registeredVotersPK) {
        this.registeredVotersPK = registeredVotersPK;
    }

    public RegisteredVoters(RegisteredVotersPK registeredVotersPK, boolean sent2BB) {
        this.registeredVotersPK = registeredVotersPK;
        this.sent2BB = sent2BB;
    }

    public RegisteredVoters(String electionID, String voterID) {
        this.registeredVotersPK = new RegisteredVotersPK(electionID, voterID);
    }

    public RegisteredVotersPK getRegisteredVotersPK() {
        return registeredVotersPK;
    }

    public void setRegisteredVotersPK(RegisteredVotersPK registeredVotersPK) {
        this.registeredVotersPK = registeredVotersPK;
    }

    public Date getRegistryDate() {
        return registryDate;
    }

    public void setRegistryDate(Date registryDate) {
        this.registryDate = registryDate;
    }

    public boolean getSent2BB() {
        return sent2BB;
    }

    public void setSent2BB(boolean sent2BB) {
        this.sent2BB = sent2BB;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (registeredVotersPK != null ? registeredVotersPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RegisteredVoters)) {
            return false;
        }
        RegisteredVoters other = (RegisteredVoters) object;
        if ((this.registeredVotersPK == null && other.registeredVotersPK != null) || (this.registeredVotersPK != null && !this.registeredVotersPK.equals(other.registeredVotersPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "eviv.er.entities.RegisteredVoters[ registeredVotersPK=" + registeredVotersPK + " ]";
    }
    
}

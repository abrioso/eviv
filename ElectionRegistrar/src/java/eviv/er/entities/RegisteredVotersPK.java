/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eviv.er.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author abrioso
 */
@Embeddable
public class RegisteredVotersPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "electionID")
    private String electionID;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "voterID")
    private String voterID;

    public RegisteredVotersPK() {
    }

    public RegisteredVotersPK(String electionID, String voterID) {
        this.electionID = electionID;
        this.voterID = voterID;
    }

    public String getElectionID() {
        return electionID;
    }

    public void setElectionID(String electionID) {
        this.electionID = electionID;
    }

    public String getVoterID() {
        return voterID;
    }

    public void setVoterID(String voterID) {
        this.voterID = voterID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (electionID != null ? electionID.hashCode() : 0);
        hash += (voterID != null ? voterID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RegisteredVotersPK)) {
            return false;
        }
        RegisteredVotersPK other = (RegisteredVotersPK) object;
        if ((this.electionID == null && other.electionID != null) || (this.electionID != null && !this.electionID.equals(other.electionID))) {
            return false;
        }
        if ((this.voterID == null && other.voterID != null) || (this.voterID != null && !this.voterID.equals(other.voterID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "eviv.er.entities.RegisteredVotersPK[ electionID=" + electionID + ", voterID=" + voterID + " ]";
    }
    
}

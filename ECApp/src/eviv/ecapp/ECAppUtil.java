/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eviv.ecapp;

import eviv.commons.rmi.HSMSimulRemote;
import eviv.commons.rmi.VSTRemote;
import eviv.ecapp.bbclient.ElectionInfo;
import java.awt.FileDialog;
import java.awt.Frame;
import java.io.IOException;
import java.io.StringWriter;
import java.rmi.*;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.security.PublicKey;
import java.security.cert.X509Certificate;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openssl.PEMWriter;

/**
 *
 * @author abrioso
 */
public class ECAppUtil {

    public static void loadBC() {
        if (java.security.Security.getProvider("BC") == null) {
            java.security.Security.addProvider(new BouncyCastleProvider());
        }
    }

    public String changeCertificate() {
        //Iniciar variaveis base
        ECAppRMI ecAppRMI = null;
        HSMSimulRemote ecAppRMIRemote = null;
        String certName = null;


        FileDialog fd = new FileDialog(new Frame(),
                "Open Cert file...", FileDialog.LOAD);
        fd.setVisible(true);
        String certFilePath = fd.getDirectory() + fd.getFile();

        fd = new FileDialog(new Frame(),
                "Open Key file...", FileDialog.LOAD);
        fd.setVisible(true);
        String keyFilePath = fd.getDirectory() + fd.getFile();

        //TODO: falta verificar se existe erro ao iniciar o cert


        System.out.println("Openning Cert file " + certFilePath
                + "\n Openning Key file " + keyFilePath);


        //TODO: fazer verificacao de que nao ainda nao foi adicionado
        loadBC();
        // java.security.Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());

        try {
            ecAppRMI = new ECAppRMI(certFilePath, keyFilePath);
            ecAppRMIRemote = (HSMSimulRemote) UnicastRemoteObject.exportObject(ecAppRMI, 0);
            Registry registry = LocateRegistry.createRegistry(ECAppRMI.REGISTRY_PORT);
            //         Registry registry = LocateRegistry.getRegistry();
            registry.bind(ECAppRMI.REGISTRY_NAME, ecAppRMIRemote);
        } catch (RemoteException ex) {
            Logger.getLogger(ECAppUtil.class.getName()).log(Level.SEVERE, null, ex);
        } catch (AlreadyBoundException ex) {
            Logger.getLogger(ECAppRMI.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            certName = ecAppRMIRemote.getCertificate().getSubjectDN().getName();
        } catch (RemoteException ex) {
            Logger.getLogger(ECAppUtil.class.getName()).log(Level.SEVERE, null, ex);
        }

        return certName;

    }

    /** Generates a VST certificate
     * VST and ECHSM must be running in the system
     * 
     * @see VSTRemote
     * @see HSMSimulRemote
     * 
     * @param voterID       ID of the voter
     * @return 
     */
    public String generateVSTCertificate(String voterID) {
        String certString = null;

        X509Certificate cert = null;
        PublicKey kPub = null;

        Registry vstRegistry = null;
        VSTRemote vstRemote = null;

        Registry ecRegistry = null;
        HSMSimulRemote ecRemote = null;


        loadBC();


        try {
            vstRegistry = LocateRegistry.getRegistry(VSTRemote.REGISTRY_PORT);
            vstRemote = (VSTRemote) vstRegistry.lookup(VSTRemote.REGISTRY_NAME);
            if (vstRemote.initVST(voterID)) {
                kPub = vstRemote.getKeyPair().getPublic();
                System.out.println("VST de " + vstRemote.getVoterID()
                        + " inicializado com kPub:\n" + vstRemote.getKeyPair().getPublic().toString());
//                System.out.println("VST INFO: " + vstRemote.toString());
//TODO: Acrecentar excepcao
            }
        }catch (NoSuchObjectException ex) {
            
            JOptionPane.showMessageDialog(null, "Couldn't connect to VST. \n"
                    + " Please check if it is "
                    + "connected or isn't initiated.\n" + ex.getMessage(),
                    "Couldn't connect to VST" ,
                    JOptionPane.ERROR_MESSAGE);
          //  Logger.getLogger(ECAppUtil.class.getName()).log(Level.SEVERE, null, ex);
        
        }catch (ConnectException ex) {
            
            JOptionPane.showMessageDialog(null, "Couldn't connect to VST. \n"
                    + " Please check if it is "
                    + "connected or isn't initiated.\n" + ex.getMessage(),
                    "Couldn't connect to VST" ,
                    JOptionPane.ERROR_MESSAGE);
          //  Logger.getLogger(ECAppUtil.class.getName()).log(Level.SEVERE, null, ex);
        
        }catch (RemoteException ex) {
            //TODO: Apresentar uma dialog box e mudar 
            Logger.getLogger(ECAppUtil.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NotBoundException ex) {
            Logger.getLogger(ECAppUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
        //vst.initVST("A000");
        try {
            ecRegistry = LocateRegistry.getRegistry(HSMSimulRemote.REGISTRY_PORT);
            ecRemote = (HSMSimulRemote) ecRegistry.lookup(HSMSimulRemote.REGISTRY_NAME);

            //TODO: o que e suposto realizar-se quando nao e possivel fazer init ao VST?
            if (kPub != null) {
                cert = ecRemote.createCertificate(voterID, kPub);
            } else {
                //TODO: fazer aviso mais visivel ou Excepcao
                System.out.println("It\'s not possible to generate " 
                        + voterID + "\'s Certificate "
                        + "(kPub is null). Is the VST already Initiated?");
                return null;
            }
        } catch (NotBoundException ex) {
            Logger.getLogger(ECAppUtil.class.getName()).log(Level.SEVERE, null, ex);
        } catch (AccessException ex) {
            Logger.getLogger(ECAppUtil.class.getName()).log(Level.SEVERE, null, ex);
        } catch (RemoteException ex) {
            //TODO: Apresentar uma dialog box e mudar 
            Logger.getLogger(ECAppUtil.class.getName()).log(Level.SEVERE, null, ex);
        }


        if (cert != null) {
            StringWriter strWriter = new StringWriter();


            PEMWriter certWriter = new PEMWriter(strWriter);
            try {
                certWriter.writeObject(cert);
                certWriter.close();

                //     BufferedWriter certBuf = new BufferedWriter
            } catch (IOException ex) {
                Logger.getLogger(ECAppUtil.class.getName()).log(Level.SEVERE, null, ex);
            }


            certString = strWriter.toString();


//        PEMWriter

            return certString;
        }
        
        return null;

    }
    
    
    /**
     * Convert Date to XMLGregorianCalendar
     * @param d The date to be converted
     * @return the converted XMLGregorianCalendar
     */
    public static XMLGregorianCalendar String2XMLGregorianCalendar(String str){
        
        Date d = null;
        GregorianCalendar cal = new GregorianCalendar();
        XMLGregorianCalendar xmlCal = null;
        
        
        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy '@' HH:mm");
        try {
            d = dateFormatter.parse(str);
        } catch (ParseException ex) {
            Logger.getLogger(ECAppUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        cal.setTimeInMillis(d.getTime());
        
        try {
            xmlCal = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
        } catch (DatatypeConfigurationException ex) {
            Logger.getLogger(ECAppUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
        

        return xmlCal;
        
        
        
    }

    public static String addVoter(java.lang.String voterID, java.lang.String voterCertificate) {
        eviv.ecapp.bbclient.BBEC_Service service = new eviv.ecapp.bbclient.BBEC_Service();
        eviv.ecapp.bbclient.BBEC port = service.getBBECPort();
        return port.addVoter(voterID, voterCertificate);
    }

    public static Boolean addElection(java.lang.String electionID, java.lang.String electionDesc) {
        eviv.ecapp.bbclient.BBEC_Service service = new eviv.ecapp.bbclient.BBEC_Service();
        eviv.ecapp.bbclient.BBEC port = service.getBBECPort();
        return port.addElection(electionID, electionDesc);
    }

    public static java.util.List<eviv.ecapp.bbclient.ElectionPartialInfo> getElectionList() {
        eviv.ecapp.bbclient.BBEC_Service service = new eviv.ecapp.bbclient.BBEC_Service();
        eviv.ecapp.bbclient.BBEC port = service.getBBECPort();
        return port.getElectionList();
    }
    
    
    

    public static ElectionInfo getElectionInfo(java.lang.String electionID) {
        eviv.ecapp.bbclient.BBEC_Service service = new eviv.ecapp.bbclient.BBEC_Service();
        eviv.ecapp.bbclient.BBEC port = service.getBBECPort();
        return port.getElectionInfo(electionID);
    }

    public static Boolean updateElection(java.lang.String electionID, eviv.ecapp.bbclient.ElectionInfo electionInfo) {
        eviv.ecapp.bbclient.BBEC_Service service = new eviv.ecapp.bbclient.BBEC_Service();
        eviv.ecapp.bbclient.BBEC port = service.getBBECPort();
        return port.updateElection(electionID, electionInfo);
    }

 
    
   public static byte[] signElectionRMI(byte[] election){
       
       Registry ecRegistry = null;
       HSMSimulRemote ecRemote = null;
        try {
            ecRegistry = LocateRegistry.getRegistry(HSMSimulRemote.REGISTRY_PORT);
            ecRemote = (HSMSimulRemote) ecRegistry.lookup(HSMSimulRemote.REGISTRY_NAME);
        } catch (NotBoundException ex) {
            Logger.getLogger(ECAppUtil.class.getName()).log(Level.SEVERE, null, ex);
        } catch (AccessException ex) {
            Logger.getLogger(ECAppUtil.class.getName()).log(Level.SEVERE, null, ex);
        } catch (RemoteException ex) {
            Logger.getLogger(ECAppUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
        
       byte[] signedElection = null;
        try {
            signedElection = ecRemote.signElection(election);
        } catch (RemoteException ex) {
            Logger.getLogger(ECAppUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
       
       return signedElection;
   }

    public static byte[] getElection2Sign(java.lang.String electionID) {
        eviv.ecapp.bbclient.BBEC_Service service = new eviv.ecapp.bbclient.BBEC_Service();
        eviv.ecapp.bbclient.BBEC port = service.getBBECPort();
        return port.getElection2Sign(electionID);
    }

    public static Boolean signElection(java.lang.String electionID, byte[] signedElection) {
        eviv.ecapp.bbclient.BBEC_Service service = new eviv.ecapp.bbclient.BBEC_Service();
        eviv.ecapp.bbclient.BBEC port = service.getBBECPort();
        return port.signElection(electionID, signedElection);
    }
    
    
    
    
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eviv.ecapp;

import eviv.commons.rmi.HSMSimulRemote;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.math.BigInteger;
import java.rmi.RemoteException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.SignedObject;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bouncycastle.asn1.ASN1Object;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x500.X500NameBuilder;
import org.bouncycastle.asn1.x500.style.BCStyle;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.X509v3CertificateBuilder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.openssl.PEMReader;
import org.bouncycastle.openssl.PasswordFinder;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;

/**
 *
 * @author abrioso
 */
public class ECAppRMI implements HSMSimulRemote {

    private X509Certificate cert;
    private PrivateKey kpriv;

    public ECAppRMI(String certFile, String keyFile) {

        InputStream inStream;
        try {
            inStream = new FileInputStream(certFile);
            CertificateFactory cf = CertificateFactory.getInstance("X.509", "BC");
            this.cert = ((X509Certificate) cf.generateCertificate(inStream));
            inStream.close();
            
            System.out.println("Certificado lido: \n" + cert.toString());
            
            
            inStream = new FileInputStream(keyFile);
//            PrivateKeyFactory kprivFact = PrivateKeyFactory.createKey(inStream);
  
            KeyFactory kf = KeyFactory.getInstance("X.509", "BC");
  
            byte[] kprivBytes = new byte[(int) keyFile.length()];
  //          inStream.read(kprivBytes);
            
            //this.kpriv = kf.generatePrivate(new X509EncodedKeySpec(kprivBytes));

            
 //           DataInputStream dis = new DataInputStream(inStream);
//.readFully(kprivBytes);
            
 //           PEMReader pemr = new PEMReader(dis);
            
 //           KeyPair kp = (KeyPair) new PEMReader(br).readObject();
   
            BufferedReader br = new BufferedReader(new FileReader(keyFile));
            
    //        final char[] pwd = "evivec".toCharArray();
            
            
            
            KeyPair kp = (KeyPair) new PEMReader(br, new PasswordFinder() {

                @Override
                public char[] getPassword() {
                    return "evivec".toCharArray();
                }
            }).readObject();
            
            
            this.kpriv = kp.getPrivate();
            
//            this.kpriv = kf.generatePrivate(new X509EncodedKeySpec(kprivBytes));
 //           this.kpriv = kf.generatePrivate(new X509EncodedKeySpec(kprivBytes));

            
//            this.kpriv = kf.generatePrivate(PrivateKeyFactory.createKey(inStream));
  
                       
            inStream.close();
            
            
            System.out.println("Chave lida: \n" + kpriv.toString());


        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(ECAppRMI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchProviderException ex) {
            Logger.getLogger(ECAppRMI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ECAppRMI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (CertificateException ex) {
            Logger.getLogger(ECAppRMI.class.getName()).log(Level.SEVERE, null, ex);
        }



    }

    @Override
    public X509Certificate getCertificate() throws RemoteException {
        return this.cert;
    }

    /**
     * Create the Voter's Certificate from it's ID and Public Key
     * 
     * @param voterID   The Voter Identifier
     * @param kpub      The Voter's Public Key
     * @return          The Voter's signed Certificate 
     * @throws RemoteException
     */
    @Override
    public X509Certificate createCertificate(String voterID, PublicKey kpub) throws RemoteException {
            X509Certificate voterCert = null;
            
     //       X509v3CertificateBuilder
       
            //Mudar de Hastable para outra Collection (HashTable deprecated)
////            Hashtable attrs = new Hashtable();
//            
//            attrs.put(X509Principal.C,"PT");
//            attrs.put(X509Principal.O,"EVIV");
//            attrs.put(X509Principal.L,"Lisbon");
//            attrs.put(X509Principal.ST,"Lisbon");
//          //  attrs.put(X509Principal.E,"eviv@ist.utl.pt");
//            attrs.put(X509Principal.OU,"Voters");
//            attrs.put(X509Principal.CN,voterID);

        //TODO: Substitutir X509V3CertificateGenerator (Deprescated) por X509v3CertificateBuilder      
        //    X509v3CertificateBuilder certBuild = new X509v3CertificateBuilder(null, BigInteger.ONE, null, null, null, null) 
            
            X500NameBuilder nameBuilder = new X500NameBuilder(BCStyle.INSTANCE);
            
            nameBuilder.addRDN(BCStyle.C, "PT");
            nameBuilder.addRDN(BCStyle.O, "EVIV");
            nameBuilder.addRDN(BCStyle.L, "Lisbon");
            nameBuilder.addRDN(BCStyle.ST, "Lisbon");
            nameBuilder.addRDN(BCStyle.OU, "Voters");
            nameBuilder.addRDN(BCStyle.CN, voterID);
            
            
            //TODO: Tem de exitir uma forma melhor de fazer isto:
            Calendar cal = new GregorianCalendar();
            cal.add(GregorianCalendar.YEAR, 1);
            
            
            X509v3CertificateBuilder certBuild = null;
            X509CertificateHolder certHolder = null;

            
  //          X509V3CertificateGenerator certGen = new X509V3CertificateGenerator();
          try {
            certBuild = new X509v3CertificateBuilder(
                    new X500Name(getCertificate().getSubjectX500Principal().getName()), 
                    BigInteger.ZERO, new GregorianCalendar().getTime(), cal.getTime(), 
                    nameBuilder.build(),
                    new SubjectPublicKeyInfo((ASN1Sequence)ASN1Object.fromByteArray(kpub.getEncoded())));
                            //new DefaultSignatureAlgorithmIdentifierFinder().find(kpub.getAlgorithm()), kpub.getEncoded()));
            
        
            certHolder = certBuild.build(new JcaContentSignerBuilder("SHA1withRSA").setProvider("BC").build(kpriv));
        
            voterCert = new JcaX509CertificateConverter().setProvider("BC").getCertificate(certHolder);
            
        } catch (OperatorCreationException ex) {
            Logger.getLogger(ECAppRMI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (CertificateException ex) {
            Logger.getLogger(ECAppRMI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ECAppRMI.class.getName()).log(Level.SEVERE, null, ex);
        }
            
            
            
            //TODO: SerialNumber unico deve ser incrementado (por servico externo?)
//            certGen.setSerialNumber(BigInteger.valueOf(1));
//            
//      //      certGen.setIssuerDN(PrincipalUtil.getSubjectX509Principal(cert));
//            certGen.setIssuerDN(cert.getSubjectX500Principal());
//        
//            //TODO: Corrigir as datas (validade desde agora ate 1 ano apos) - usar Calendar em vez de Date
//            certGen.setNotBefore(new Date(System.currentTimeMillis()));
//            certGen.setNotAfter(new Date((System.currentTimeMillis() + 31536000)));
//            
//            certGen.setSubjectDN(new X509Principal(attrs));
//            
//            certGen.setPublicKey(kpub);
//            
//            //TODO: Confirmar Algoritmo
//            certGen.setSignatureAlgorithm("SHA256WithRSAEncryption");
//            try {
//                cert = certGen.generate(kpriv);
//            } catch (CertificateEncodingException ex) {
//                Logger.getLogger(ECAppRMI.class.getName()).log(Level.SEVERE, null, ex);
//            } catch (IllegalStateException ex) {
//                Logger.getLogger(ECAppRMI.class.getName()).log(Level.SEVERE, null, ex);
//            } catch (NoSuchAlgorithmException ex) {
//                Logger.getLogger(ECAppRMI.class.getName()).log(Level.SEVERE, null, ex);
//            } catch (SignatureException ex) {
//                Logger.getLogger(ECAppRMI.class.getName()).log(Level.SEVERE, null, ex);
//            } catch (InvalidKeyException ex) {
//                Logger.getLogger(ECAppRMI.class.getName()).log(Level.SEVERE, null, ex);
//            } 
            
            return voterCert;
        
        } 
        
    
    
    
    //TODO: APAGAR!!
    @Override
    public X509Certificate signCertificate(Certificate crt) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    
    @Override
    public byte[] signElection(byte[] election) throws RemoteException {

 //       ElGamalPublicKey ekpub = getElectionPubKey(electionKey);


      //  Elections election = (Elections) obj;
        

        /*     ObjectOutputStream outStream = null;
        ByteArrayOutputStream outByteStream = null;
        try {
        outStream = new ObjectOutputStream(outByteStream);
        outStream.writeObject(ekpub);
        outStream.flush();
        } catch (IOException ex) {
        Logger.getLogger(TrusteeAppRMI.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        byte[] outBytes = null;
        outBytes = outByteStream.toByteArray();
        
        
        generatedkeyPair.publicKey
         */


        SignedObject so = null;
        Signature sig;
        try {
            // sig = Signature.getInstance(kpriv.getAlgorithm());
            sig = Signature.getInstance("MD5WithRSA");

            
            so = new SignedObject(election, kpriv, sig);

        } catch (IOException ex) {
            Logger.getLogger(ECAppRMI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidKeyException ex) {
            Logger.getLogger(ECAppRMI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SignatureException ex) {
            Logger.getLogger(ECAppRMI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(ECAppRMI.class.getName()).log(Level.SEVERE, null, ex);
        }



        ObjectOutputStream outStream = null;
        ByteArrayOutputStream outByteStream = null;
        outByteStream = new ByteArrayOutputStream();

        try {
            outStream = new ObjectOutputStream(outByteStream);
            outStream.writeObject(so);
            outStream.flush();
        } catch (IOException ex) {
            Logger.getLogger(ECAppRMI.class.getName()).log(Level.SEVERE, null, ex);
        }

        byte[] outBytes = null;
        outBytes = outByteStream.toByteArray();


        //TODO: Mudar retorno
        return outBytes;

    }

    
   
}

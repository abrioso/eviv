/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eviv.trusteeapp;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author abrioso
 * @deprecated It's best to use Collection<ElectionKeyPair>
 */
@Deprecated
public class ElectionKeys implements Serializable{
    
    ArrayList<ElectionKeyPair> keys;

    public ElectionKeys() {
        this.keys = new ArrayList<ElectionKeyPair>();        
    }

    public ArrayList<ElectionKeyPair> getKeys() {
        return keys;
    }

    public int getSize() {
        return keys.size();
    }

    public void setKeys(ArrayList<ElectionKeyPair> keys) {
        this.keys = keys;
    }
    
    
    public boolean add(ElectionKeyPair keyPair) {
        return keys.add(keyPair);
    }
    
    public boolean remove(ElectionKeyPair keyPair) {
        return keys.remove(keyPair);
    }
    
 /*   public ElectionKeyPair getElectionKey(String electionID) {
        
        ElectionKeyPair electionKeys = null;
        
        for(ElectionKeyPair k : electionKeys) {
            if(k.getElectionID().equals(electionID)) {
                electionKeys = k;
                break;
            }
            
        }
        
        return electionKeys;
    }
   */ 
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eviv.trusteeapp;

import eviv.trusteeapp.ui.TrusteeAppGUI;

/**
 *
 * @author abrioso
 */
public class TrusteeApp {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
          java.awt.EventQueue.invokeLater(new Runnable() {

            @Override
            public void run() {
                new TrusteeAppGUI().setVisible(true);
            }
        });
    }
}

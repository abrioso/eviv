/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eviv.trusteeapp;

import eviv.bb.web.ElectionInfo;
import eviv.bb.web.ElectionPartialInfo;
import eviv.bb.web.Votes;
import eviv.commons.rmi.TrusteeRemote;
import eviv.commons.types.TrusteeElection;
import eviv.commons.util.Converters;
import gsd.inescid.markpledge3.MP3VoteAndReceipt;
import java.awt.Component;
import java.awt.FileDialog;
import java.awt.Frame;
import java.io.*;
import java.math.BigInteger;
import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.security.SignedObject;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

/**
 *
 * @author abrioso
 */
public class TrusteeAppUtil {

    public static void loadBC() {
        if (java.security.Security.getProvider("BC") == null) {
            java.security.Security.addProvider(new BouncyCastleProvider());
        }
    }

    static ArrayList<TrusteeElection> ImportElections(String trusteeID) {
        ArrayList<TrusteeElection> elections = null;

        File f = new File(trusteeID + ".ell");

        if (f.exists()) {

            FileInputStream fis;
            try {
                fis = new FileInputStream(f);


                ObjectInputStream ois;
                try {
                    ois = new ObjectInputStream(fis);

                    elections = (ArrayList<TrusteeElection>) ois.readObject();

                } catch (IOException ex) {
                    Logger.getLogger(TrusteeAppUtil.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(TrusteeAppUtil.class.getName()).log(Level.SEVERE, null, ex);
                }

                fis.close();
            } catch (FileNotFoundException ex) {
                Logger.getLogger(TrusteeAppUtil.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(TrusteeAppUtil.class.getName()).log(Level.SEVERE, null, ex);
            }

            return elections;
        } else {
            return null;
        }
    }

    static Boolean ExportElections(ArrayList<TrusteeElection> elections, String trusteeID) {
        
        File f = new File(trusteeID + ".ell");

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(f);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TrusteeAppUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
        ObjectOutputStream oos = null;
        try {
            oos = new ObjectOutputStream(fos);

            oos.writeObject(elections);

            fos.close();
        } catch (IOException ex) {
            Logger.getLogger(TrusteeAppUtil.class.getName()).log(Level.SEVERE, null, ex);
        }

        return true;
    }

    public static Boolean addElection(String electionID) {
       
        ElectionInfo electionInfo = getElectionInfo(electionID);
        
        boolean addElection = false;
        
        TrusteeRemote connect2TrusteeRMI = connect2TrusteeRMI();
        try {
            addElection = connect2TrusteeRMI.addElection(electionID, ConvertEInfo(electionInfo));
        } catch (RemoteException ex) {
            Logger.getLogger(TrusteeAppUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return addElection;
        
    }

   

    
    public String changeCertificate() {
        //Iniciar variaveis base
        TrusteeAppRMI trusteeAppRMI = null;
        TrusteeRemote trusteeAppRMIRemote = null;
        String certName = null;


        FileDialog fd = new FileDialog(new Frame(),
                "Open Cert file...", FileDialog.LOAD);
        fd.setVisible(true);
        String certFilePath = fd.getDirectory() + fd.getFile();

        fd = new FileDialog(new Frame(),
                "Open Key file...", FileDialog.LOAD);
        fd.setVisible(true);
        String keyFilePath = fd.getDirectory() + fd.getFile();

        //TODO: falta verificar se existe erro ao iniciar o cert


        System.out.println("Openning Cert file " + certFilePath
                + "\n Openning Key file " + keyFilePath);


        //TODO: fazer verificacao de que nao ainda nao foi adicionado
        loadBC();
        // java.security.Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());

        try {
            trusteeAppRMI = new TrusteeAppRMI(certFilePath, keyFilePath);
            trusteeAppRMIRemote = (TrusteeRemote) UnicastRemoteObject.exportObject(trusteeAppRMI, 0);
            Registry registry = LocateRegistry.createRegistry(TrusteeAppRMI.REGISTRY_PORT);
            //         Registry registry = LocateRegistry.getRegistry();
            registry.bind(TrusteeAppRMI.REGISTRY_NAME, trusteeAppRMIRemote);
        } catch (RemoteException ex) {
            Logger.getLogger(TrusteeAppUtil.class.getName()).log(Level.SEVERE, null, ex);
        } catch (AlreadyBoundException ex) {
            Logger.getLogger(TrusteeAppRMI.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            certName = trusteeAppRMIRemote.getCertificate().getSubjectDN().getName();
        } catch (RemoteException ex) {
            Logger.getLogger(TrusteeAppUtil.class.getName()).log(Level.SEVERE, null, ex);
        }

        return certName;

    }

    public static byte[] generateElectionKey(String electionID) {

        Registry trusteeRegistry = null;
        TrusteeRemote trusteeRemote = null;

        byte[] key = null;

        try {
            trusteeRegistry = LocateRegistry.getRegistry(TrusteeRemote.REGISTRY_PORT);

            trusteeRemote = (TrusteeRemote) trusteeRegistry.lookup(TrusteeRemote.REGISTRY_NAME);

            key = trusteeRemote.generateElectionKey(electionID);

        } catch (RemoteException ex) {
            Logger.getLogger(TrusteeAppUtil.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NotBoundException ex) {
            Logger.getLogger(TrusteeAppUtil.class.getName()).log(Level.SEVERE, null, ex);
        }

        return key;


    }

    @Deprecated
    public static ArrayList<ElectionKeyPair> ImportElectionsKeys(String trusteeID) {
        ArrayList<ElectionKeyPair> keys = null;

        File f = new File(trusteeID + ".elk");

        if (f.exists()) {

            FileInputStream fis;
            try {
                fis = new FileInputStream(f);


                ObjectInputStream ois;
                try {
                    ois = new ObjectInputStream(fis);

                    keys = (ArrayList<ElectionKeyPair>) ois.readObject();

                } catch (IOException ex) {
                    Logger.getLogger(TrusteeAppUtil.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(TrusteeAppUtil.class.getName()).log(Level.SEVERE, null, ex);
                }

                fis.close();
            } catch (FileNotFoundException ex) {
                Logger.getLogger(TrusteeAppUtil.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(TrusteeAppUtil.class.getName()).log(Level.SEVERE, null, ex);
            }

            return keys;
        } else {
            return null;
        }

    }

    @Deprecated
    public static boolean ExportElectionKeys(ArrayList<ElectionKeyPair> keys, String trusteeID) {

        File f = new File(trusteeID + ".elk");

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(f);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TrusteeAppUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
        ObjectOutputStream oos = null;
        try {
            oos = new ObjectOutputStream(fos);

            oos.writeObject(keys);

            fos.close();
        } catch (IOException ex) {
            Logger.getLogger(TrusteeAppUtil.class.getName()).log(Level.SEVERE, null, ex);
        }

        return true;
    }

    public static byte[] signElectionKey(String electionID) {

        Registry trusteeRegistry = null;
        TrusteeRemote trusteeRemote = null;

        byte[] signedKey = null;

        try {
            trusteeRegistry = LocateRegistry.getRegistry(TrusteeRemote.REGISTRY_PORT);

            trusteeRemote = (TrusteeRemote) trusteeRegistry.lookup(TrusteeRemote.REGISTRY_NAME);

            signedKey = trusteeRemote.signElectionKey(electionID);

        } catch (RemoteException ex) {
            Logger.getLogger(TrusteeAppUtil.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NotBoundException ex) {
            Logger.getLogger(TrusteeAppUtil.class.getName()).log(Level.SEVERE, null, ex);
        }

        return signedKey;
    }
    
        public static byte[] getElectionPubKey(String electionID) {

        Registry trusteeRegistry = null;
        TrusteeRemote trusteeRemote = null;

        byte[] kpub = null;

        try {
            trusteeRegistry = LocateRegistry.getRegistry(TrusteeRemote.REGISTRY_PORT);

            trusteeRemote = (TrusteeRemote) trusteeRegistry.lookup(TrusteeRemote.REGISTRY_NAME);

            kpub = trusteeRemote.findElectionPubKey(electionID);

        } catch (RemoteException ex) {
            Logger.getLogger(TrusteeAppUtil.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NotBoundException ex) {
            Logger.getLogger(TrusteeAppUtil.class.getName()).log(Level.SEVERE, null, ex);
        }

        return kpub;
    }

        
        
    private static TrusteeRemote connect2TrusteeRMI() {

        Registry trusteeRegistry = null;
        TrusteeRemote trusteeRemote = null;

        try {
            trusteeRegistry = LocateRegistry.getRegistry(TrusteeRemote.REGISTRY_PORT);

            trusteeRemote = (TrusteeRemote) trusteeRegistry.lookup(TrusteeRemote.REGISTRY_NAME);

        } catch (RemoteException ex) {
            Logger.getLogger(TrusteeAppUtil.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NotBoundException ex) {
            Logger.getLogger(TrusteeAppUtil.class.getName()).log(Level.SEVERE, null, ex);
        }

        return trusteeRemote;
    }
        
    public static byte[] aggregateVotes(String electionID, List<Votes> rawVotes) {
        byte[] signedAggVotes = null;

        byte[] byteVotes = null;
       
        
        //TODO: Convert List<Votes> to List<MP3VoteAndReceipt>
        
        MP3VoteAndReceipt[] votes = getVotes(rawVotes);
        
        
        
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ObjectOutput out = new ObjectOutputStream(bos);

            out.writeObject(votes);
            byteVotes = bos.toByteArray();

            out.close();
            bos.close();

        } catch (IOException ex) {
            Logger.getLogger(TrusteeAppUtil.class.getName()).log(Level.SEVERE, null, ex);
        }

        TrusteeRemote trusteeRemote = connect2TrusteeRMI();
        
        
        if (trusteeRemote != null && byteVotes != null) {
            try {
                boolean setElectionNumVotes = trusteeRemote.setElectionNumVotes(electionID, votes.length);
                signedAggVotes = trusteeRemote.aggregateAndSignVotes(electionID, byteVotes);
            } catch (RemoteException ex) {
                Logger.getLogger(TrusteeAppUtil.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        return signedAggVotes;
        
    }
    
    public static byte[] decryptAggregateVotes(String electionID, byte[] aggVotesBytes) {
       
        TrusteeRemote trusteeRemote = connect2TrusteeRMI();
        byte[] decryptedAggVotes = null;
        
        
        if (trusteeRemote != null && aggVotesBytes != null) {
            try {
                decryptedAggVotes = trusteeRemote.decryptAggregatedVotes(electionID, aggVotesBytes);
            } catch (RemoteException ex) {
                Logger.getLogger(TrusteeAppUtil.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        return decryptedAggVotes;
        
        
    }


    
      private static MP3VoteAndReceipt[] getVotes(List<Votes> rawVotes) {

        ArrayList<MP3VoteAndReceipt> votes_list = new ArrayList<MP3VoteAndReceipt>();
        
        for (Votes v : rawVotes) {
            
            MP3VoteAndReceipt vote = null; 
            
            try {
                ByteArrayInputStream bis = new ByteArrayInputStream(v.getVoteInfo());
                ObjectInput in = new ObjectInputStream(bis);
            
                SignedObject signedVote = (SignedObject) in.readObject();
            
                bis.close();
                in.close();
                
                vote = (MP3VoteAndReceipt) signedVote.getObject();
            
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(TrusteeAppRMI.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(TrusteeAppRMI.class.getName()).log(Level.SEVERE, null, ex);
            }

                        
                votes_list.add(vote);
            
        }

        MP3VoteAndReceipt[] votes = votes_list.toArray(new MP3VoteAndReceipt[0]);

        return votes;
        

    }
    
   
    
    public static void refreshPane(JPanel panel) {
        
   //     List<String> electionList = null;
   //     electionList = getElectionList();
        
        
        if (panel.isShowing()) {
            
            List<ElectionPartialInfo> elections = TrusteeAppUtil.getElectionList();

              
                if (!elections.isEmpty()) {

                                //TODO - Distinguish which Panel is selected

                for (Component c : panel.getComponents()) {
                    if (
                            (c != null)
                         //   && (c.getClass() == JComboBox.class)
                            && (c.getName() != null)
                            && (c.getName().equals("electionIDSelection"))) {
                        JComboBox cbox = (JComboBox) c;
                        cbox.removeAllItems();
                        for (ElectionPartialInfo eid : elections) {
                            cbox.addItem(eid.getElectionID() + " - " + eid.getElectionDesc());
                        }
                        
//                        cbox.setSelectedIndex(0);
                        
                    }
                }

            }

            
 /*           if(pane.getName().equals("electionPublicKeyGenPanel"))
            {
                pane.getComponents().
            }
            if (!electionList.isEmpty()) {
                electionIDComboBox.removeAllItems();

                for (String eid : elections) {
                    electionIDComboBox.addItem(eid);
                }
            }
        
        */
        
  //          JOptionPane.showMessageDialog(null, panel.getName() + " refreshed");
        }
    }
    
    
    
 
    public static java.util.List<eviv.bb.web.ElectionPartialInfo> getElectionList() {
        eviv.bb.web.BBTrustees_Service service = new eviv.bb.web.BBTrustees_Service();
        eviv.bb.web.BBTrustees port = service.getBBTrusteesPort();
        return port.getElectionList();
    }

    public static ElectionInfo getElectionInfo(java.lang.String electionID) {
        eviv.bb.web.BBTrustees_Service service = new eviv.bb.web.BBTrustees_Service();
        eviv.bb.web.BBTrustees port = service.getBBTrusteesPort();
        return port.getElectionInfo(electionID);
    }

    public static java.util.List<java.lang.Byte> setElectionKey(java.lang.String electionID, java.util.List<java.lang.Byte> electionKPub, java.util.List<java.lang.Byte> electionSignedKPub) {
        eviv.bb.web.BBTrustees_Service service = new eviv.bb.web.BBTrustees_Service();
        eviv.bb.web.BBTrustees port = service.getBBTrusteesPort();
        return port.setElectionKey(electionID, electionKPub, electionSignedKPub);
    }

    public static java.util.List<eviv.bb.web.Votes> getVotes(java.lang.String electionID) {
        eviv.bb.web.BBTrustees_Service service = new eviv.bb.web.BBTrustees_Service();
        eviv.bb.web.BBTrustees port = service.getBBTrusteesPort();
        return port.getVotes(electionID);
    }

    public static byte[] getAggregatedVotes(java.lang.String electionID) {
        eviv.bb.web.BBTrustees_Service service = new eviv.bb.web.BBTrustees_Service();
        eviv.bb.web.BBTrustees port = service.getBBTrusteesPort();
        return port.getAggregatedVotes(electionID);
    }

    public static Boolean setAggregatedVotes(java.lang.String electionID, byte[] electionAggregatedVotes) {
        eviv.bb.web.BBTrustees_Service service = new eviv.bb.web.BBTrustees_Service();
        eviv.bb.web.BBTrustees port = service.getBBTrusteesPort();
        return port.setAggregatedVotes(electionID, electionAggregatedVotes);
    }
    
     public static byte[] getDecryptedAggregatedVotes(java.lang.String electionID) {
        eviv.bb.web.BBTrustees_Service service = new eviv.bb.web.BBTrustees_Service();
        eviv.bb.web.BBTrustees port = service.getBBTrusteesPort();
        return port.getDecryptedAggregatedVotes(electionID);
    }

    public static Boolean setDecryptedAggregatedVotes(java.lang.String electionID, byte[] electionDecryptedAggregatedVotes) {
        eviv.bb.web.BBTrustees_Service service = new eviv.bb.web.BBTrustees_Service();
        eviv.bb.web.BBTrustees port = service.getBBTrusteesPort();
        return port.setDecryptedAggregatedVotes(electionID, electionDecryptedAggregatedVotes);
    }
    

    public static Boolean setResults(java.lang.String electionID, byte[] electionResults) {
        eviv.bb.web.BBTrustees_Service service = new eviv.bb.web.BBTrustees_Service();
        eviv.bb.web.BBTrustees port = service.getBBTrusteesPort();
        return port.setResults(electionID, electionResults);
    }

    
    
    private static eviv.commons.types.ElectionInfo ConvertEInfo(eviv.bb.web.ElectionInfo info) {
       
        eviv.commons.types.ElectionInfo electionInfo = new eviv.commons.types.ElectionInfo();
        
        electionInfo.setElectionID(info.getElectionID());
        electionInfo.setElectionDesc(info.getElectionDesc());
        electionInfo.setNumCandidates(info.getNumCandidates());
        electionInfo.setElectionCandidates(new ArrayList<String>(info.getElectionCandidates()));
   //     electionInfo.setElectionDates(info.getElectionDates().toArray(new Date[0]));
        electionInfo.setIsPhaseFinished(info.getIsPhaseFinished().toArray(new Boolean[4]));
        electionInfo.setElectionPubKey(info.getElectionPubKey().toArray(new Byte[0]));
        
        return electionInfo;
        
    }
    
    public static BigInteger generateElectionChallenge(String electionID) {
        
        BigInteger chal = null;
        byte[] chalByte = null;
        
        TrusteeRemote connect2TrusteeRMI = connect2TrusteeRMI();
        try {
            chalByte = connect2TrusteeRMI.generateChallenge(electionID);
            setElectionChallenge(electionID, chalByte);
        } catch (RemoteException ex) {
            Logger.getLogger(TrusteeAppUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        SignedObject so = (SignedObject) Converters.byteToObject(chalByte);
        chal = (BigInteger) Converters.signedObjectToObject(so);
        
        
        return chal;
        
    }
    
    
    
    
    
    
    public static int[] generateResults(String electionID) {
        
        int[] results = null;
        
        byte[] aggVotes = getAggregatedVotes(electionID);
        byte[] decryptedAggVotes = getDecryptedAggregatedVotes(electionID);
        
        
        TrusteeRemote connect2TrusteeRMI = connect2TrusteeRMI();
        
        byte[] generateAndSignResults = null;
        
        try {
            generateAndSignResults = connect2TrusteeRMI.generateAndSignResults(electionID, aggVotes, decryptedAggVotes);
            setResults(electionID, generateAndSignResults);
        } catch (RemoteException ex) {
            Logger.getLogger(TrusteeAppUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        try {
                ByteArrayInputStream bis = new ByteArrayInputStream(generateAndSignResults);
                ObjectInput in = new ObjectInputStream(bis);
            
                SignedObject signedVote = (SignedObject) in.readObject();
            
                bis.close();
                in.close();
                
                results = (int[]) signedVote.getObject();
            
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(TrusteeAppRMI.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(TrusteeAppRMI.class.getName()).log(Level.SEVERE, null, ex);
            }
        
        
                
        return results;
        
    }


    public static byte[] generateAndSignResults(String electionID) {
                
        byte[] aggVotes = getAggregatedVotes(electionID);
        byte[] decryptedAggVotes = getDecryptedAggregatedVotes(electionID);
        
        
        TrusteeRemote connect2TrusteeRMI = connect2TrusteeRMI();
        
        byte[] generateAndSignResults = null;
        
        try {
            generateAndSignResults = connect2TrusteeRMI.generateAndSignResults(electionID, aggVotes, decryptedAggVotes);
            setResults(electionID, generateAndSignResults);
        } catch (RemoteException ex) {
            Logger.getLogger(TrusteeAppUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
        
                
        return generateAndSignResults;
        
    }
    
    
    

    private static java.util.List<java.lang.Byte> setElectionVerificationData(java.lang.String electionID, java.util.List<java.lang.Byte> verificationData) {
        eviv.bb.web.BBTrustees_Service service = new eviv.bb.web.BBTrustees_Service();
        eviv.bb.web.BBTrustees port = service.getBBTrusteesPort();
        return port.setElectionVerificationData(electionID, verificationData);
    }

    private static byte[] setElectionChallenge(java.lang.String electionID, byte[] electionSignedChallenge) {
        eviv.bb.web.BBTrustees_Service service = new eviv.bb.web.BBTrustees_Service();
        eviv.bb.web.BBTrustees port = service.getBBTrusteesPort();
        return port.setElectionChallenge(electionID, electionSignedChallenge);
    }

    

}

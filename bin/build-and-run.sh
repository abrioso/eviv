#!/bin/sh

EVIV_BASE_DIR=/Users/abrioso/NetBeansProjects/EVIV
CURRENT_DIR=`pwd`


libprojects=(EVIV-MarkPlege3 EVIV-Commons)
deployprojects=(BulletinBoard BallotBox ElectionRegistrar)
runprojects=(VoterSecurityToken ECApp TrusteeApp VoterApp)

projects=( ${libprojects[@]} ${deployprojects[@]} ${runprojects[@]} )

if test $CURRENT_DIR != $EVIV_BASE_DIR
	then
	printf " You're in %s\n" $PWD
	printf " Changing dir to %s\n" $EVIV_BASE_DIR
	cd $EVIV_BASE_DIR
fi

echo "Number of SubProjects: ${#projects[*]}"
echo "SubProjects:"
for item in ${projects[*]}
do
    printf "   %s\n" $item
done

echo "Cleaning SubProjects..."
for item in ${projects[*]}
do
    printf "   %s (clean)\n" $item
	ant -f $item/build.xml clean
done

echo "Building SubProjects..."
for item in ${projects[*]}
do
    printf "   %s (build)\n" $item
	ant -f $item/build.xml default
done

echo "Deploy SubProjects..."
for item in ${deployprojects[*]}
do
    printf "   %s (deploy)\n" $item
	ant -f $item/build.xml run
	sleep 5
done

echo "Run SubProjects..."
for item in ${runprojects[*]}
do
    printf "   %s (run)\n" $item
	ant -f $item/build.xml run &
	sleep 5
done



#if($CURRENT_DIR in *EVIV)
#	then
#	
#	    printf " You're in %s\n" $CURRENT_DIR
#
#fi



#echo "Projects items:"
#for item in ${projects[*]}
#do
#    printf "   %s\n" $item
#done

#for filename in ./*
#do
#	ant -s $filename/build.xml clean
#  echo $filename
#done;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eviv.commons.rmi;

import eviv.commons.types.VoterElection;
import gsd.inescid.crypto.ElGamalPublicKey;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.security.KeyPair;

/**
 *
 * @author abrioso
 */
public interface VSTRemote extends Remote {

    //<editor-fold defaultstate="collapsed" desc="Registry Port">
    public static final int REGISTRY_PORT = 2001;
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Registry Name">
    public static final String REGISTRY_NAME = "VST";
    //</editor-fold>

   // public ElGamalKeyPair getKeyPair() throws RemoteException;

    public KeyPair getKeyPair() throws RemoteException;
    
    public String getVoterID() throws RemoteException;

    public Boolean initVST(String voterID) throws RemoteException;
    
    public Boolean addElection(String electionID, String[] candDesc, ElGamalPublicKey kpub) throws RemoteException;
    
    public VoterElection getElection(String electionID) throws RemoteException;
    
    public  byte[] generateVote(String electionID, String voteCode) throws RemoteException;
    
}

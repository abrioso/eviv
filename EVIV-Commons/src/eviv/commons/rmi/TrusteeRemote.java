/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eviv.commons.rmi;

import eviv.commons.types.ElectionInfo;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.security.cert.X509Certificate;

/**
 *
 * @author abrioso
 */
public interface TrusteeRemote extends Remote{
    
    //<editor-fold defaultstate="collapsed" desc="Registry Port">
    public static final int REGISTRY_PORT = 2003;
    //</editor-fold>
   
     //<editor-fold defaultstate="collapsed" desc="Registry Name">
    public static final String REGISTRY_NAME = "Trustee-HSM";
    //</editor-fold>
    
    public X509Certificate getCertificate() throws RemoteException;
    public boolean addElection(String electionID, ElectionInfo info) throws RemoteException;
    public boolean setElectionNumVotes(String electionID, int numVotes) throws RemoteException;
    public byte[] generateElectionKey(String electionID) throws RemoteException;
    public byte[] signElectionKey(String electionID) throws RemoteException;
    public byte[] findElectionPubKey(String ElectionID) throws RemoteException;
    public byte[] generateChallenge(String electionID) throws RemoteException;
    public byte[] aggregateAndSignVotes(String electionID, byte[] byteVotes) throws RemoteException;
    public byte[] decryptAggregatedVotes(String electionID, byte[] byteAggVotes) throws RemoteException;
    public byte[] generateAndSignResults(String electionID, byte[] raw_aggVotes, byte[] raw_decryptedAggVotes) throws RemoteException;
    

    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eviv.commons.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.security.PublicKey;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;

/**
 *
 * @author abrioso
 */
public interface HSMSimulRemote extends Remote{
    
    //<editor-fold defaultstate="collapsed" desc="Registry Port">
    public static final int REGISTRY_PORT = 2002;
    //</editor-fold>
   
     //<editor-fold defaultstate="collapsed" desc="Registry Name">
    public static final String REGISTRY_NAME = "EC-HSM";
    //</editor-fold>
    
    public X509Certificate getCertificate() throws RemoteException;
    public X509Certificate createCertificate(String voterID, PublicKey kpub) throws RemoteException;
    public X509Certificate signCertificate(Certificate crt) throws RemoteException;
    public byte[] signElection(byte[] election) throws RemoteException;
    
}

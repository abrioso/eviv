/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eviv.commons.types;

import gsd.inescid.crypto.ElGamalPrivateKey;
import gsd.inescid.markpledge3.MP3AggregatedElectionVotes;

/**
 *
 * @author abrioso
 */
public class TrusteeElection extends BaseElection {

    ElGamalPrivateKey kpriv = null;
    MP3AggregatedElectionVotes aggregatedVotes = null;
    
    public TrusteeElection() {
    }

    public MP3AggregatedElectionVotes getAggregatedVotes() {
        return aggregatedVotes;
    }

    public void setAggregatedVotes(MP3AggregatedElectionVotes aggregatedVotes) {
        this.aggregatedVotes = aggregatedVotes;
    }

    public ElGamalPrivateKey getKpriv() {
        return kpriv;
    }

    public void setKpriv(ElGamalPrivateKey kpriv) {
        this.kpriv = kpriv;
    }
    
    
    
    
    
}

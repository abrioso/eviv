/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eviv.commons.types;

import eviv.commons.util.Converters;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.io.Serializable;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Random;

/**
 *
 * @author abrioso
 */
public final class CodeCard implements Printable, Serializable {
    
    private final int CODECARD_STRING_SIZE = 5;

    private String electionID = null;
    private String[] candidatesDesc = null;
    private String[] candidatesCodes = null;
    private String verificationCode = null;
    private int numCandidates = 0;
    
    @Override
    public int print(Graphics grphcs, PageFormat pf, int i) throws PrinterException {
        
        int x = 100;
        int y = 100;
        
        if (i > 0) { /* We only have one page */
            return NO_SUCH_PAGE;
        }
        
        /* Translate X & Y values to be inside the imageable area */
        Graphics2D g2d = (Graphics2D) grphcs;
        g2d.translate( pf.getImageableX(), pf.getImageableY());
  //      g2d.translate(pf.getImageableX(), pf.getImageableY());
        
  //      g2d.setBackground(Color.yellow);
        
        // Draw the strings
        g2d.drawString("Election: " + electionID, x, y);
        y+=5;
        g2d.drawLine(x, y, x+150, y);
  //      grphcs.drawString("TESTE!!!!", 100, 100);
        for(int j = 0; j < numCandidates; j++) {
           y+=14;
            g2d.drawString(candidatesDesc[j], x, y); 
            g2d.drawString(candidatesCodes[j], x+100, y);             
        }
        
        y+=5;
        g2d.drawLine(x, y, x+150, y);
        
        y+=14;
        g2d.drawString("Verification: ", x, y);
        g2d.drawString(verificationCode, x+100, y);
        
        return PAGE_EXISTS;
        
        
    }

    public CodeCard(String electionID, int numCand, BigInteger pledge) {
        this.electionID = electionID;
        this.numCandidates = numCand;
        
        this.candidatesDesc = new String[numCand];
        this.candidatesCodes = new String[numCand];
        
        setCandidatesDesc();
        setCandidateCodes();
        setVerificationCode(pledge);
        
        
    }
    
     public CodeCard(String electionID, String[] candDesc, BigInteger pledge) {
        
         this(electionID, candDesc.length, pledge);
         
         
         setCandidatesDesc(candDesc);

    }
    
    private void setCandidateCodes() {
        
        Random r = new SecureRandom();
        
        for(int i =0; i < numCandidates; i++) {
         //   BigInteger codeValue = new BigInteger(2^CODECARD_STRING_SIZE, r); 
            BigInteger codeValue = new BigInteger((int) Math.pow(8, CODECARD_STRING_SIZE), r);
//            codeValue = new BigInteger(100, r);
            
            this.candidatesCodes[i] = Converters.base26toString(codeValue, CODECARD_STRING_SIZE);
        }
        
    }
    
    private void setVerificationCode(BigInteger pledge) {
        this.verificationCode = Converters.base26toString(pledge, CODECARD_STRING_SIZE);
    }
    
    // Talvez deva ser private -> foi posta para public pq dos testes
    public String[] getCandidatesCodes() {
        return candidatesCodes;
    }

    private String[] getCandidatesDesc() {
        return candidatesDesc;
    }

    private void setCandidatesDesc() {
        
        char c = 'A';
        
        for(int i = 0; i < numCandidates; i++) {
            this.candidatesDesc[i] = Character.toString((char) (c + i));
        }
    }
    
    public void setCandidatesDesc(String[] candidatesDesc) {
        this.candidatesDesc = candidatesDesc;
    }

    private String getElectionID() {
        return electionID;
    }

    private int getNumCandidates() {
        return numCandidates;
    }

    private String getVerificationCode() {
        return verificationCode;
    }    
    
    public int getCodeIndex(String code) {
        int index = -1;
        
        for (int i = 0; i < candidatesCodes.length; i++) {
            if (candidatesCodes[i].equals(code)) {
                index = i;
                break;
            }
        }

        
        return index;
    }
    
}

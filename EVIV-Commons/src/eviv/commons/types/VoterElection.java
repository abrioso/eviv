/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eviv.commons.types;

import eviv.commons.util.Converters;
import gsd.inescid.markpledge3.MP3VoteAndReceipt;
import gsd.inescid.markpledge3.MP3VoteFactory;
import java.math.BigInteger;

/**
 *
 * @author abrioso
 */
public class VoterElection extends BaseElection  {
    
    
    MP3VoteFactory ballot = null;
    MP3VoteAndReceipt vote = null;
//    String[] codeCard = null;
    CodeCard codeCard = null;

    public VoterElection() {
    }

    public MP3VoteFactory getBallot() {
        return ballot;
    }

    public void setBallot(MP3VoteFactory ballot) {
        this.ballot = ballot;
    }

/*    @Override
    public void setNumCandidates(int numCandidates) {
  //      this.codeCard = new String[numCandidates];
        this.numCandidates = numCandidates;
    }
*/
    public MP3VoteAndReceipt getVote() {
        return vote;
    }

    public void setVote(MP3VoteAndReceipt vote) {
        this.vote = vote;
    }


    public CodeCard getCodeCard() {
        return codeCard;
    }

    public void setCodeCard() {
        
        BigInteger pledge = getBallot().getPledgeValue();
        
      //  this.codeCard = new CodeCard(electionID, numCandidates, pledge);
        this.codeCard = new CodeCard(electionID, candDesc, pledge);
    }

    public String getVoteReceipt() {
        
        MP3VoteAndReceipt v = this.vote;
        
        if(v == null)
            return null;
        
        
        
        //Adapted RJoaquim code
        String s = "\nVote Receipt";
        for (int i = 0, r = v.FIRST_CANDIDATE_INDEX; i < v.CANDIDATE_VOTES.length; i++, r = (++r % v.CANDIDATE_VOTES.length)) {
            s += "\n" + candDesc[i] + " - " + Converters.base26toString(v.CANDIDATE_VOTES[r].VERIFICATION_VALUE);
        }

        s += "\nVote challenge\n" + Converters.base26toString(v.CHALLENGE);
        return s;

                    
    }
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eviv.commons.types;

import java.io.Serializable;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author abrioso
 */
public class DecryptionProof implements Serializable {
    
    BigInteger d;
    BigInteger e;
    BigInteger c;
    BigInteger r;

    /**
     * Chaum-Pederson Discrete Logs Equality
     * 
     * cypher = (a,b)
     * 
     * g,h are the public key values
     * 
     * modulus = q -> info from public key
     * 
     * s is the private key
     * 
     * We generate a random z to calculate the following:
     *  d = g^z
     *  e = a^z
     *  c = hash(g,h,a,b,d,e)
     *  r = z + c*s
     * 
     * We publish d,e,c,r
     * 
     * 
     */
    
    
    public DecryptionProof() {
    }

    public BigInteger getC() {
        return c;
    }

    public BigInteger getD() {
        return d;
    }

    public BigInteger getE() {
        return e;
    }

    public BigInteger getR() {
        return r;
    }

    
    
    
    
    public void calculateD(BigInteger g, BigInteger z, BigInteger modulus) {
        this.d = g.modPow(z, modulus);
    }
 
    public void calculateE(BigInteger a, BigInteger z, BigInteger modulus) {
        this.d = a.modPow(z, modulus);
    }
    
    public void calculateC(BigInteger g, BigInteger h, BigInteger a, BigInteger b, BigInteger d, BigInteger e) {
        
        String encryptionAlgorithm = "SHA-1";

        
        try {
            MessageDigest msgDgst = MessageDigest.getInstance(encryptionAlgorithm);
            
            
            msgDgst.reset();
            msgDgst.update(g.toByteArray());
            msgDgst.update(h.toByteArray());
            msgDgst.update(a.toByteArray());
            msgDgst.update(b.toByteArray());
            msgDgst.update(d.toByteArray());
            msgDgst.update(e.toByteArray());

            this.c = new BigInteger(msgDgst.digest());

            
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(DecryptionProof.class.getName()).log(Level.SEVERE, null, ex);
        }
     
    }
    
    public void calculateR(BigInteger z, BigInteger c, BigInteger s){
        this.r = z.add(c.multiply(s));
    }
}

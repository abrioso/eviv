/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eviv.commons.types;

/**
 *
 * @author abrioso
 */
public class ElectionPartialInfo {
    
    private String electionID;
    private String electionDesc;

    public ElectionPartialInfo(){
        
    }
    
    public ElectionPartialInfo(String electionID, String electionDesc) {
        this.electionID = electionID;
        this.electionDesc = electionDesc;
    }

    public String getElectionDesc() {
        return electionDesc;
    }

    public void setElectionDesc(String electionDesc) {
        this.electionDesc = electionDesc;
    }

    public String getElectionID() {
        return electionID;
    }

    public void setElectionID(String electionID) {
        this.electionID = electionID;
    }

    @Override
    public String toString() {
        return electionID + " - " + electionDesc;
    }
    
    
    
    public static String getElectionIDFromPInfo(String pInfo) {
        String[] splitInfo = pInfo.split(" - ");
        return splitInfo[0];
    }
    
}

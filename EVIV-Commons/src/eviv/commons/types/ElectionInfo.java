/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eviv.commons.types;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author abrioso
 */
public class ElectionInfo implements Serializable {

    public static final int ENROLL_DATE = 0;
    public static final int REGISTER_DATE = 1;
    public static final int CAST_DATE = 2;
    public static final int COUNT_DATE = 3;
    
    private String electionID;
    private String electionDesc;
    private Date[] electionDates;
    private ArrayList<String> electionCandidates = new ArrayList<String>();
    
    
    //Valor de teste - Apagar e actualizar no setter depois..
    private int numCandidates = 0;
    
    private Byte[] electionPubKey;
    
    private BigInteger electionChallenge;
    
    private Boolean[] isPhaseFinished = {Boolean.FALSE, Boolean.FALSE, Boolean.FALSE, Boolean.FALSE}; //new Boolean[4];

    public ElectionInfo() {
        this.electionDates = new Date[4];
 //       this.electionCandidates = new ArrayList<String>();

    }

    /**
     * Get the value of electionPubKey
     *
     * @return the value of electionPubKey
     */
    public Byte[] getElectionPubKey() {
        return electionPubKey;
    }

    /**
     * Set the value of electionPubKey
     *
     * @param electionPubKey new value of electionPubKey
     */
    public void setElectionPubKey(Byte[] electionPubKey) {
        this.electionPubKey = electionPubKey;
    }

    /**
     * Get the value of electionPubKey at specified index
     *
     * @param index
     * @return the value of electionPubKey at specified index
     */
    public Byte getElectionPubKey(int index) {
        return this.electionPubKey[index];
    }

    /**
     * Set the value of electionPubKey at specified index.
     *
     * @param index
     * @param newElectionPubKey new value of electionPubKey at specified index
     */
    public void setElectionPubKey(int index, Byte newElectionPubKey) {
        this.electionPubKey[index] = newElectionPubKey;
    }

    public ElectionInfo(String electionID) {
        this.electionID = electionID;
        this.electionDates = new Date[4];
        
    }

    public ArrayList<String> getElectionCandidates() {
        return electionCandidates;
    }

    public void setElectionCandidates(ArrayList<String> electionCandidates) {
        this.electionCandidates = electionCandidates;
    }

    public void addElectionCandidates(String candidate) {
        this.electionCandidates.add(candidate);
        this.numCandidates++;
    }

    public int getNumCandidates() {
        return numCandidates;
    }

    public void setNumCandidates(int numCandidates) {
        this.numCandidates = numCandidates;
    }
    
    
    
    
    

    /**
     * Get the value of electionDates
     *
     * @return the value of electionDates
     */
    public Date[] getElectionDates() {
        return electionDates;
    }

    /**
     * Set the value of electionDates
     *
     * @param electionDates new value of electionDates
     */
    public void setElectionDates(Date[] electionDates) {
        this.electionDates = electionDates;
    }

    /**
     * Get the value of electionDates at specified index
     *
     * @param index
     * @return the value of electionDates at specified index
     */
    public Date getElectionDates(int index) {
        return this.electionDates[index];
    }

    /**
     * Set the value of electionDates at specified index.
     *
     * @param index
     * @param newElectionDates new value of electionDates at specified index
     */
    public void setElectionDates(int index, Date newElectionDates) {
        this.electionDates[index] = newElectionDates;
    }

    /**
     * Get the value of electionDesc
     *
     * @return the value of electionDesc
     */
    public String getElectionDesc() {
        return electionDesc;
    }

    /**
     * Set the value of electionDesc
     *
     * @param electionDesc new value of electionDesc
     */
    public void setElectionDesc(String electionDesc) {
        this.electionDesc = electionDesc;
    }

    /**
     * Get the value of electionID
     *
     * @return the value of electionID
     */
    public String getElectionID() {
        return electionID;
    }

    /**
     * Set the value of electionID
     *
     * @param electionID new value of electionID
     */
    public void setElectionID(String electionID) {
        this.electionID = electionID;
    }

    public Boolean[] getIsPhaseFinished() {
        return isPhaseFinished;
    }

    public void setIsPhaseFinished(Boolean[] isPhaseFinished) {
        this.isPhaseFinished = isPhaseFinished;
    }

    public Boolean getIsPhaseFinished(int phase) {
        return isPhaseFinished[phase];
    }

    public void setIsPhaseFinished(Boolean isPhaseFinished, int phase) {
        this.isPhaseFinished[phase] = isPhaseFinished;
    }

    public BigInteger getElectionChallenge() {
        return electionChallenge;
    }

    public void setElectionChallenge(BigInteger electionChallenge) {
        this.electionChallenge = electionChallenge;
    }

    
    
    
    @Override
    public String toString() {
        return "ElectionInfo{" + "electionID=" + electionID + ", electionDesc=" + electionDesc + '}';
    }

    
    
    
}

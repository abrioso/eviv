/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eviv.commons.types;

import gsd.inescid.crypto.ElGamalPublicKey;
import gsd.inescid.markpledge3.MP3Parameters;
import java.io.Serializable;
import java.math.BigInteger;

/**
 *
 * @author abrioso
 */
public class BaseElection implements Serializable {
    
      
    String electionID = null;
    int numCandidates = 0;
    ElGamalPublicKey kpub = null;
    MP3Parameters electionParam = null;
    int numVotes = 0;
    BigInteger challenge = null;
    String[] candDesc = null;
    
    public BaseElection() {
    }

    public String getElectionID() {
        return electionID;
    }

    public void setElectionID(String electionID) {
        this.electionID = electionID;
    }

    public MP3Parameters getElectionParam() {
        return electionParam;
    }

    public void setElectionParam(MP3Parameters electionParam) {
        this.electionParam = electionParam;
    }

    public int getNumCandidates() {
        return numCandidates;
    }

    public void setNumCandidates(int numCandidates) {
        this.numCandidates = numCandidates;
        this.candDesc = new String[numCandidates];
        setCandDesc();
    }


    public ElGamalPublicKey getKpub() {
        return kpub;
    }

    public void setKpub(ElGamalPublicKey kpub) {
        this.kpub = kpub;
    }

    public int getNumVotes() {
        return numVotes;
    }

    public void setNumVotes(int numVotes) {
        this.numVotes = numVotes;
    }

    public BigInteger getChallenge() {
        return challenge;
    }

    public void setChallenge(BigInteger challenge) {
        this.challenge = challenge;
    }

    public String[] getCandDesc() {
        return candDesc;
    }

    public void setCandDesc(String[] candDesc) {
        this.candDesc = candDesc;
    }
    
    private void setCandDesc() {
        
        char c = 'A';
        
        for(int i = 0; i < numCandidates; i++) {
            this.candDesc[i] = Character.toString((char) (c + i));
        }
    }
        
    
}

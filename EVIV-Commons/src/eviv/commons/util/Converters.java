/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eviv.commons.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.math.BigInteger;
import java.security.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author abrioso
 */
public final class Converters {

    private static int DEFAULT_STRSIZE = 5;

    public static String base26toString(BigInteger bigInt) {

        return base26toString(bigInt, DEFAULT_STRSIZE);
        
    
    }

    public static String base26toString(BigInteger bigInt, int strSize) {

        //    BigInteger bigInt = new BigInteger(25, new SecureRandom());

        String str = bigInt.toString(26);

        char[] str_char = str.toCharArray();

        String str_final = new String();

        for (int i = 0; i < strSize; i++) {
            str_final += Character.toString((char) ('A' + Character.digit(str_char[i], 26)));
        }
        return str_final;

    }
    
    
    public static SignedObject objectToSignedObject(Serializable obj, PrivateKey kpriv) {
        SignedObject so = null;
        
        if(obj != null) {
        
            Signature sig;
            try {
                // sig = Signature.getInstance(kpriv.getAlgorithm());
                sig = Signature.getInstance("MD5WithRSA");
                so = new SignedObject(obj, kpriv, sig);
            } catch (InvalidKeyException ex) {
                Logger.getLogger(Converters.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SignatureException ex) {
                Logger.getLogger(Converters.class.getName()).log(Level.SEVERE, null, ex);
            } catch (NoSuchAlgorithmException ex) {
                Logger.getLogger(Converters.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(Converters.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        return so;
    }
    
    public static Object signedObjectToObject(SignedObject so) {
        Object obj = null;
        
        try {
            obj =  so.getObject();
        } catch (IOException ex) {
            Logger.getLogger(Converters.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Converters.class.getName()).log(Level.SEVERE, null, ex);
        }

        
        return obj;
    }
    
    public static byte[] objectToByte(Object obj) {
        byte[] objBytes = null;
        
        if(obj != null) {
            ObjectOutputStream outStream = null;
            ByteArrayOutputStream outByteStream = null;
            outByteStream = new ByteArrayOutputStream();
            try {
                outStream = new ObjectOutputStream(outByteStream);
                outStream.writeObject(obj);
                outStream.flush();
            } catch (IOException ex) {
                Logger.getLogger(Converters.class.getName()).log(Level.SEVERE, null, ex);
            }
      
            objBytes = outByteStream.toByteArray();

        }
        
        return objBytes;
    }
    
    public static Object byteToObject(byte[] objBytes) {
        Object obj = null;
        
        try {
                ByteArrayInputStream bis = new ByteArrayInputStream(objBytes);
                ObjectInput in = new ObjectInputStream(bis);
            
                obj = in.readObject();
            
                bis.close();
                in.close();
                
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(Converters.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(Converters.class.getName()).log(Level.SEVERE, null, ex);
            }
        
        return obj;
    }
 
    
    public static String byte2MD5(byte[] data) {
        
        String hash = null;
        
        MessageDigest md = null;
        
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Converters.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        md.update(data);
 
        byte hashByte[] = md.digest();
 
       
        //convert the byte to hex format
        StringBuilder hexString = new StringBuilder();
    	for (int i=0;i<hashByte.length;i++) {
    		String hex=Integer.toHexString(0xff & hashByte[i]);
   	     	if(hex.length()==1) hexString.append('0');
   	     	hexString.append(hex);
    	}
    	
        hash = hexString.toString();
        
 //       System.out.println("Digest(in hex format):: " + hexString.toString());
      
        
        return hash;
    }
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eviv.commons.util;

import eviv.commons.types.DecryptionProof;
import gsd.inescid.crypto.ElGamalEncryption;
import gsd.inescid.crypto.ElGamalKeyPair;
import gsd.inescid.crypto.ElGamalPublicKey;
import gsd.inescid.crypto.util.CryptoUtil;
import gsd.inescid.markpledge3.MP3AggregatedElectionVotes;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Random;

/**
 *
 * @author abrioso
 */
public class Crypto {
    
    
    public static DecryptionProof generateProof(ElGamalKeyPair keys, ElGamalEncryption encryption) {
        
        
        DecryptionProof proof = new DecryptionProof();
        
        // PublicKey is (p,q,g,h)
        ElGamalPublicKey kpub = keys.publicKey;
        
        BigInteger z = generateRandomNumber(kpub.q, new SecureRandom());
        
        proof.calculateD(kpub.g, z, kpub.q);
        proof.calculateE(encryption.X, z, kpub.q);
        proof.calculateC(kpub.g, kpub.h, encryption.X, encryption.Y, proof.getD(), proof.getE());
        proof.calculateR(z, proof.getC(), keys.privateKey.kpri);
        
        return proof;
    }
    
    public static Boolean verifyProof(ElGamalPublicKey kpub,
            ElGamalEncryption aggVote, BigInteger decAggVote,
            DecryptionProof decAggVoteProof) {
        
        
        BigInteger g = kpub.g;
        BigInteger h = kpub.h;
        BigInteger r = decAggVoteProof.getR();
        BigInteger d = decAggVoteProof.getD();
        BigInteger c = decAggVoteProof.getC();
        BigInteger e = decAggVoteProof.getE();
        BigInteger b = aggVote.Y;
        
        if((g.modPow(r, kpub.q) == d.multiply(h.modPow(c, kpub.q))) && 
                (h.modPow(r, kpub.q) == e.multiply(b.modPow(c, kpub.q)))) {
            return true;
        } else {
            return false;
        }
        
        
    }
    
    
    public static BigInteger generateRandomNumber(int bitLength, Random random) {
        return CryptoUtil.generateRandomNumber(bitLength, random);
    }
    
    public static BigInteger generateRandomNumber(BigInteger maxValue, Random random) {
        return CryptoUtil.generateRandomNumber(maxValue, random);
    }
    
    public static Boolean verifyAggregationDecryptionProof(ElGamalPublicKey kpub,
            MP3AggregatedElectionVotes aggVotes, BigInteger[] decAggVotes,
            DecryptionProof[] decAggVotesProof) {
                
        if((aggVotes.aggregatedVotes.length != decAggVotes.length) || (decAggVotes.length != decAggVotesProof.length))
            return false;
        
        
        
        for(int i=0; i<decAggVotes.length; i++) {
            Boolean b = verifyProof(kpub, aggVotes.aggregatedVotes[i], decAggVotes[i], decAggVotesProof[i]);
            if(b == false)
                return false;
        }
        
        return true;
            
    }
    
    public static DecryptionProof[] generateAggregationDecryptionProof(ElGamalKeyPair keys,
            MP3AggregatedElectionVotes aggVotes) {
        
        DecryptionProof[] proofList = new DecryptionProof[aggVotes.NUMBER_OF_AGGREGATED_VOTES];
        
        ElGamalEncryption[] aggVotesEnc = aggVotes.aggregatedVotes;
        
        
        for(int i=0; i<aggVotesEnc.length; i++) {
            DecryptionProof proof = generateProof(keys, aggVotes.aggregatedVotes[i]);
            proofList[i] = proof;
        }
        
        return proofList;
        
        
    }
    
    
}

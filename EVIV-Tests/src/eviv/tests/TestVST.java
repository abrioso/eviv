/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eviv.tests;

import eviv.VST.VoterSecurityToken;
import eviv.commons.types.CodeCard;
import eviv.tests.utils.EVIVTestResults;
import eviv.tests.utils.Pair;
import eviv.tests.utils.TestStorage;
import eviv.tests.utils.TestUtils;
import eviv.tests.ws.bb_er.ElectionInfo;
import gsd.inescid.crypto.ElGamalPublicKey;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang3.ArrayUtils;

/**
 *
 * Test the following functions: - initVST(voterID) - addElection(electionID,
 * candDesc, kpub) -- Needs VST + getElectionInfo (@BB_ER) -
 * generateVote(electionID, candCode) -- Needs VST registered + CodeCard
 *
 * @author abrioso
 */
public class TestVST implements Serializable {

    private int numVST = 0;
    private TestStorage stor = null;

    //   private String[] voterIDs = null;
    //   private VoterSecurityToken[] vstList = null;
    public TestVST(TestStorage storage) {
        this(storage, 1);
    }

    public TestVST(TestStorage storage, int numVST) {
        this.stor = storage;
        this.numVST = numVST;
        //       this.voterIDs = new String[numVST];
        //       TestUtils.initStrArray(voterIDs);
        //       this.vstList = new VoterSecurityToken[numVST];

        TestUtils.loadBC();

    }

    public int getNumVST() {
        return numVST;
    }

    private void addVST(String voterID, VoterSecurityToken vst) {
        stor.addVST(voterID, vst);
    }

    private void addResult(String testName, EVIVTestResults testResults) {
        stor.addResult(testName, testResults);
    }

    public void testInitVST() {
        double[] results = initVSTs(numVST);
        String testName = "TestVST: initVSTs";
        EVIVTestResults testResults = TestUtils.createResults(testName, results);


        addResult(testName, testResults);
//        printResults("initVSTs " + ")", results);
    }

 

    //
  

    private double[] initVSTs(int num) {

        String voterIDPrefix = "TV0";
        double[] time;
        time = new double[num];

        for (int i = 0; i < num; i++) {
            String voterID = voterIDPrefix + i;
            //           setVoterID(voterID, i);
            time[i] = initVST(voterID);
        }

        return time;
    }

    private double initVST(String voterID) {
        double time = 0;
        long startTime = 0;
        long stopTime = 0;

        VoterSecurityToken vst = createVST();

        //timein
        startTime = System.currentTimeMillis();
        Boolean initVST = vst.initVST(voterID);
        stopTime = System.currentTimeMillis();

        time = stopTime - startTime;

        addVST(voterID, vst);

//        setVST(vst, TestUtils.getPosStrArray(voterIDs, voterID));

        return time;
    }

    
       //
    public void testAddElection() {
        double[] results = addElections();        //FIXME: Alterar função

        String testName = "TestVST: addElection";
        EVIVTestResults testResults = TestUtils.createResults(testName, results);


        addResult(testName, testResults);

//        printResults("initVSTs " + ")", results);
    }
    
    private double[] addElections() {

        double[] time;
        
        
        Set<String> vstKeys = stor.getVSTtKeys();
        Set<String> election2RegisterKeys = stor.getElection2RegisterKeys();
        
        time = new double[vstKeys.size() * election2RegisterKeys.size()];
        
        int i = 0;
        for (String voterID : vstKeys) {
            
            VoterSecurityToken vst = stor.getVST(voterID);
            
            for (String electionID : election2RegisterKeys) {
                
                ElectionInfo election2Register = stor.getElection2Register(electionID);
                
                
                time[i] = addElection(vst, election2Register);
                i++;
            }
        }

        return time;
    }

    private double addElection(VoterSecurityToken vst, ElectionInfo electionInfo) {

        double time = 0;
        long startTime = 0;
        long stopTime = 0;

        String voterID = vst.getVoterID();
        
        String electionID = electionInfo.getElectionID();
        String[] electionCand = electionInfo.getElectionCandidates().toArray(new String[0]);
                
        byte[] kpub_bytes = ArrayUtils.toPrimitive(electionInfo.getElectionPubKey().toArray(new Byte[0]));
        
       
        ObjectInputStream inStream = null;
        ByteArrayInputStream inByteStream = null;

        ElGamalPublicKey kpub = null;
        
        inByteStream = new ByteArrayInputStream(kpub_bytes);
        try {
            inStream = new ObjectInputStream(inByteStream);
        } catch (IOException ex) {
            Logger.getLogger(TestVST.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            kpub = (ElGamalPublicKey) inStream.readObject();
        } catch (IOException ex) {
            Logger.getLogger(TestVST.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(TestVST.class.getName()).log(Level.SEVERE, null, ex);
        }

        
        
        //timein
        startTime = System.currentTimeMillis();
        try {
            vst.addElection(electionID, electionCand, kpub);
        } catch (RemoteException ex) {
            Logger.getLogger(TestVST.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        //timeout
        stopTime = System.currentTimeMillis();
        
        Pair<String, String> pairVoterElection = new Pair<String, String>(voterID, electionID);
        
        CodeCard codeCard = null;
        
        try {
            codeCard = vst.getElection(electionID).getCodeCard();
        } catch (RemoteException ex) {
            Logger.getLogger(TestVST.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        stor.addCodeCard(pairVoterElection, codeCard);
        
        time = stopTime - startTime;
        return time;
    }

    
    
    
    ////
      public void testGenerateVotes(int candNum) {
        double[] results = generateVotes(candNum);        //FIXME: Alterar função

        String testName = "TestVST: generateVotes";
        EVIVTestResults testResults = TestUtils.createResults(testName, results);


        addResult(testName, testResults);

//        printResults("initVSTs " + ")", results);
    }
    
        private double[] generateVotes(int candNum) {

        double[] time;
   
        Set<Pair<String, String>> codeCardKeys = stor.getCodeCardKeys();
        
        time = new double[codeCardKeys.size()];
        
        int i = 0;
        for (Pair<String, String> voterElectionPair : codeCardKeys) {
          
            time[i] = generateVote(voterElectionPair, candNum);
            i++;
    
        }

        return time;
    }

    private double generateVote(Pair<String, String> voterElectionPair, int candNum) {

        double time = 0;
        long startTime = 0;
        long stopTime = 0;

        String voterID = voterElectionPair.getFirst();
        String electionID = voterElectionPair.getSecond();

        VoterSecurityToken vst = stor.getVST(voterID);

        CodeCard codeCard = stor.getCodeCard(voterElectionPair);

        String codeVote = codeCard.getCandidatesCodes()[candNum];

        
        byte[] generatedVote = null;
        
        //timein
        startTime = System.currentTimeMillis();
        try {
            generatedVote = vst.generateVote(electionID, codeVote);
        } catch (RemoteException ex) {
            Logger.getLogger(TestVST.class.getName()).log(Level.SEVERE, null, ex);
        }
                
        
        //timeout
        stopTime = System.currentTimeMillis();

        
        stor.addVote2Sign(voterElectionPair, generatedVote);;
        
        time = stopTime - startTime;
        return time;
    }

    
    
    
 /*   private double generateVote(String voterID, String electionID, String candCode) {
        double time = 0;
        long startTime = 0;
        long stopTime = 0;
        VoterSecurityToken vst = createVST(voterID);


        //timein
        startTime = System.currentTimeMillis();

        try {
            //timein
            vst.generateVote(electionID, candCode);
        } catch (RemoteException ex) {
            Logger.getLogger(TestVST.class.getName()).log(Level.SEVERE, null, ex);
        }

        //timeout
        stopTime = System.currentTimeMillis();

        time = stopTime - startTime;

        return time;
    }

*/
    
    
    
    
    private VoterSecurityToken createVST() {
        VoterSecurityToken vst = new VoterSecurityToken();
        return vst;
    }

    private VoterSecurityToken createVST(String voterID) {
        VoterSecurityToken vst = new VoterSecurityToken(voterID + ".vst");
        return vst;
    }
}

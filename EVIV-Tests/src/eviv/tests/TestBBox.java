/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eviv.tests;

import eviv.tests.utils.EVIVTestResults;
import eviv.tests.utils.Pair;
import eviv.tests.utils.TestStorage;
import eviv.tests.utils.TestUtils;
import java.util.Set;

/**
 *
 * 
 * Test the following functions:
 *      - sendVote(electionID, voterID, vote)   -- Needs VST registered + vote
 * 
 * 
 * @author abrioso
 */
public class TestBBox {
    
     private TestStorage stor = null;

    public TestBBox(TestStorage storage) {
        this.stor = storage;

        TestUtils.loadBC();
    }

    
    
    public void testBBox_sendVote() {
        double[] results = bbox_sendVotes();
        String testName = "TestBBox: sendVotes";
        EVIVTestResults testResults = TestUtils.createResults(testName, results);

        stor.addResult(testName, testResults);
    }

    private double[] bbox_sendVotes() {

        double[] time;
  
        Set<Pair<String, String>> voteKeys = stor.getVote2SignKeys();
        
        time = new double[voteKeys.size()];

        

        int i = 0;
        for (Pair<String, String> voterElectionPair : voteKeys) {
              
            String voterID = voterElectionPair.getFirst();
            String electionID = voterElectionPair.getSecond();
            byte[] vote = stor.getVote2Sign(voterElectionPair);
            
            time[i] = bbox_sendVote(voterID, electionID, vote);
            i++;
        }

        return time;
    }

    private double bbox_sendVote(String voterID, String electionID, byte[] vote) {
        double time = 0;
        long startTime = 0;
        long stopTime = 0;


        //timein
        startTime = System.currentTimeMillis();
        sendVote(electionID, voterID, vote);
        stopTime = System.currentTimeMillis();

        time = stopTime - startTime;
        
        return time;
    }

    private static Boolean sendVote(java.lang.String electionID, java.lang.String voterID, byte[] vote) {
        eviv.tests.ws.bbox.BallotBox_Service service = new eviv.tests.ws.bbox.BallotBox_Service();
        eviv.tests.ws.bbox.BallotBox port = service.getBallotBoxPort();
        return port.sendVote(electionID, voterID, vote);
    }

    
    
    
}

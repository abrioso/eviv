/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eviv.tests;

import eviv.ecapp.ECAppUtil;
import eviv.tests.utils.EVIVTestResults;
import eviv.tests.utils.Pair;
import eviv.tests.utils.TestStorage;
import eviv.tests.utils.TestUtils;
import eviv.tests.ws.bb_ec.ElectionInfo;
import eviv.tests.ws.bb_ec.ElectionPartialInfo;
import eviv.tests.ws.bb_ec.Voters;
import eviv.tests.ws.bb_trustees.Votes;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import org.apache.commons.lang3.ArrayUtils;

/**
 *
 * @author abrioso
 */
public class TestBB {

    private TestStorage stor = null;

    public TestBB(TestStorage storage) {
        this.stor = storage;

        TestUtils.loadBC();
    }

    /*
     * testBB_EC_AddVoters
     */
    public void testBB_EC_AddVoters() {
        double[] results = bb_ecAddVoters(stor.getCertMap());
        String testName = "TestBB: BB_EC_AddVoters";
        EVIVTestResults testResults = TestUtils.createResults(testName, results);


        stor.addResult(testName, testResults);
//        printResults("initVSTs " + ")", results);
    }

    private double[] bb_ecAddVoters(HashMap<String, X509Certificate> certList) {

        double[] time;

        Set voterIDs = stor.getCerttKeys();

        time = new double[voterIDs.size()];

        int i = 0;
        for (Object vid : voterIDs) {

            X509Certificate cert = certList.get(vid);
            String certString = TestUtils.convertCert2String(cert);


            time[i] = bb_ecAddVoter((String) vid, certString);
            i++;
        }

        return time;
    }

    private double bb_ecAddVoter(String voterID, String cert) {
        double time = 0;
        long startTime = 0;
        long stopTime = 0;

        //  String certReply = null; 

        //timein
        startTime = System.currentTimeMillis();
        addVoter(voterID, cert);
        stopTime = System.currentTimeMillis();

        time = stopTime - startTime;

        //      stor.addCert(voterID, vstCert);

        return time;
    }

    /*
     * testBB_EC_AddElections
     */
    public void testBB_EC_AddElections(int num) {
        double[] results = bb_ecAddElections(num);
        String testName = "TestBB: BB_EC_AddElections";
        EVIVTestResults testResults = TestUtils.createResults(testName, results);


        stor.addResult(testName, testResults);
//        printResults("initVSTs " + ")", results);
    }

    private double[] bb_ecAddElections(int num) {

        double[] time;

        final String electionIDPrefix = "TE0";
        final String electionDescPrefix = "Description for ";
        time = new double[num];

        for (int i = 0; i < num; i++) {
            String electionID = electionIDPrefix + i;
            String electionDesc = electionDescPrefix + " " + electionID;
            time[i] = bb_ecAddElections(electionID, electionDesc);
        }

        return time;
    }

    private double bb_ecAddElections(String electionID, String electionDesc) {
        double time = 0;
        long startTime = 0;
        long stopTime = 0;

        //timein
        startTime = System.currentTimeMillis();
        addElection(electionID, electionDesc);
        stopTime = System.currentTimeMillis();

        time = stopTime - startTime;

        return time;
    }

    /*
     * testBB_getElectionLists
     */
    public void testBB_getElectionLists(int num) {
        double[] results = bb_getElectionLists(num);
        String testName = "TestBB: BB_getElectionLists";
        EVIVTestResults testResults = TestUtils.createResults(testName, results);

        stor.addResult(testName, testResults);
    }

    private double[] bb_getElectionLists(int num) {

        double[] time;

        time = new double[num];

        for (int j = 0; j < num; j++) {
            time[j] = bb_getElectionList();
        }

        return time;
    }

    private double bb_getElectionList() {
        double time = 0;
        long startTime = 0;
        long stopTime = 0;


        //timein
        startTime = System.currentTimeMillis();
        getElectionList();
        stopTime = System.currentTimeMillis();

        time = stopTime - startTime;

        return time;
    }

    /*
     * testBB_EC_UpdateElections
     */
    public void testBB_EC_UpdateElections() {
        double[] results = bb_ecUpdateElections();
        String testName = "TestBB: BB_EC_UpdateElectionAddElectionInfos";
        EVIVTestResults testResults = TestUtils.createResults(testName, results);


        stor.addResult(testName, testResults);
//        printResults("initVSTs " + ")", results);
    }

    private double[] bb_ecUpdateElections() {

        double[] time;


        List<ElectionPartialInfo> electionList = getElectionList();

        time = new double[electionList.size()];

        int i = 0;
        for (ElectionPartialInfo electionPartialInfo : electionList) {

            time[i] = bb_ecUpdateElection(electionPartialInfo.getElectionID(), electionPartialInfo.getElectionDesc());
            i++;

        }

        return time;
    }

    private double bb_ecUpdateElection(String electionID, String electionDesc) {
        double time = 0;
        long startTime = 0;
        long stopTime = 0;

        final int NUM_CAND = 4;



        eviv.tests.ws.bb_ec.ElectionInfo info = new eviv.tests.ws.bb_ec.ElectionInfo();

        info.setElectionID(electionID);
        info.setElectionDesc(electionDesc + " UPD");

        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy '@' hh:mm");

        String eDatePhase1 = dateFormatter.format(System.currentTimeMillis());
        String eDatePhase2 = dateFormatter.format(System.currentTimeMillis());
        String eDatePhase3 = dateFormatter.format(System.currentTimeMillis());
        String eDatePhase4 = dateFormatter.format(System.currentTimeMillis());


        info.getElectionDates().clear();

        info.getElectionDates().add(eviv.commons.types.ElectionInfo.ENROLL_DATE, ECAppUtil.String2XMLGregorianCalendar(eDatePhase1));
        info.getElectionDates().add(eviv.commons.types.ElectionInfo.REGISTER_DATE, ECAppUtil.String2XMLGregorianCalendar(eDatePhase2));
        info.getElectionDates().add(eviv.commons.types.ElectionInfo.CAST_DATE, ECAppUtil.String2XMLGregorianCalendar(eDatePhase3));
        info.getElectionDates().add(eviv.commons.types.ElectionInfo.COUNT_DATE, ECAppUtil.String2XMLGregorianCalendar(eDatePhase4));


        for (int i = 0; i < NUM_CAND; i++) {
            char candID = (char) ('A' + i);
            String candDesc = "Cand " + candID + " @ " + electionID;
            info.getElectionCandidates().add(candDesc);
        }




        //timein
        startTime = System.currentTimeMillis();
        updateElection(electionID, info);
        stopTime = System.currentTimeMillis();

        time = stopTime - startTime;

        return time;
    }

    /*
     * testBB_getVotersLists
     */
    public void testBB_getVotersLists(int num) {
        double[] results = bb_getVotersLists(num);
        String testName = "TestBB: BB_getVotersLists";
        EVIVTestResults testResults = TestUtils.createResults(testName, results);

        stor.addResult(testName, testResults);
    }

    private double[] bb_getVotersLists(int num) {

        double[] time;

        time = new double[num];

        for (int j = 0; j < num; j++) {
            time[j] = bb_getVotersList();
        }

        return time;
    }

    private double bb_getVotersList() {
        double time = 0;
        long startTime = 0;
        long stopTime = 0;


        //timein
        startTime = System.currentTimeMillis();
        getVotersList();
        stopTime = System.currentTimeMillis();

        time = stopTime - startTime;

        return time;
    }

    /*
     * testBB_getElectionInfo
     */
    public void testBB_getElectionInfos() {
        double[] results = bb_getElectionInfos();
        String testName = "TestBB: BB_getElectionInfos";
        EVIVTestResults testResults = TestUtils.createResults(testName, results);

        stor.addResult(testName, testResults);
    }

    private double[] bb_getElectionInfos() {

        double[] time;


        List<ElectionPartialInfo> electionList = getElectionList();

        time = new double[electionList.size()];


        int i = 0;
        for (ElectionPartialInfo einfo : electionList) {

            time[i] = bb_getElectionInfo(einfo.getElectionID());
            i++;
        }

        return time;
    }

    private double bb_getElectionInfo(String electionID) {
        double time = 0;
        long startTime = 0;
        long stopTime = 0;


        //timein
        startTime = System.currentTimeMillis();
        getElectionInfo(electionID);
        stopTime = System.currentTimeMillis();

        time = stopTime - startTime;

        return time;
    }

    /*
     * testBB_getElectionInfo
     */
    public void testBB_getVoterInfos() {
        double[] results = bb_getVoterInfos();
        String testName = "TestBB: BB_getVoterInfos";
        EVIVTestResults testResults = TestUtils.createResults(testName, results);

        stor.addResult(testName, testResults);
    }

    private double[] bb_getVoterInfos() {

        double[] time;
        //       List<Voters> votersList = getVotersList();


        Set<String> vstKeys = stor.getVSTtKeys();

        time = new double[vstKeys.size()];


        int i = 0;
        for (String vinfo : vstKeys) {

            time[i] = bb_getVoterInfo(vinfo);
            i++;
        }

        return time;
    }

    private double bb_getVoterInfo(String voterID) {
        double time = 0;
        long startTime = 0;
        long stopTime = 0;


        //timein
        startTime = System.currentTimeMillis();
        getVoterInfo(voterID);
        stopTime = System.currentTimeMillis();

        time = stopTime - startTime;

        return time;
    }

    /*
     * testBB_Trustee_setElectionKeys
     */
    public void testBB_Trustee_setElectionKeys() {
        double[] results = bb_trusteeSetElectionKeys();
        String testName = "TestBB: BB_Trustee_SetElectionKeys";
        EVIVTestResults testResults = TestUtils.createResults(testName, results);


        stor.addResult(testName, testResults);
//        printResults("initVSTs " + ")", results);
    }

    private double[] bb_trusteeSetElectionKeys() {

        double[] time;

        Set<String> electionKPubKeys = stor.getElectionKPubKeys();

        time = new double[electionKPubKeys.size()];

        int i = 0;
        for (String eid : electionKPubKeys) {

            byte[] kpub = stor.getElectionKPub(eid);
            byte[] signedKpub = stor.getElectionSigKPub(eid);
            
            time[i] = bb_trusteeSetElectionKey(eid, kpub, signedKpub);
            i++;

        }

        return time;
    }

    private double bb_trusteeSetElectionKey(String electionID, byte[] kpub_bytes, byte[] kpub_signed_bytes) {
        double time = 0;
        long startTime = 0;
        long stopTime = 0;


        List<Byte> kpub = Arrays.asList(ArrayUtils.toObject(kpub_bytes));
        List<Byte> signedKey = Arrays.asList(ArrayUtils.toObject(kpub_signed_bytes));

        //timein
        startTime = System.currentTimeMillis();
        setElectionKey(electionID, kpub, signedKey); 
        stopTime = System.currentTimeMillis();

        time = stopTime - startTime;

        return time;
    }

    
    /*
     * testBB_EC_UpdateElections
     */
    public void testBB_Trustee_setElectionChallenge() {
        double[] results = bb_trusteeSetElectionChallenges();
        String testName = "TestBB: BB_Trustee_SetElectionChallenges";
        EVIVTestResults testResults = TestUtils.createResults(testName, results);


        stor.addResult(testName, testResults);
//        printResults("initVSTs " + ")", results);
    }

    private double[] bb_trusteeSetElectionChallenges() {

        double[] time;
        
        Set<String> electionChallengeKeys = stor.getElectionChallengeKeys();

        time = new double[electionChallengeKeys.size()];

        int i = 0;
        for (String eid : electionChallengeKeys) {

            byte[] chal = stor.getElectionChallenge(eid);
            
            time[i] = bb_trusteeSetElectionChallenge(eid, chal);
            i++;

        }

        return time;
    }

    private double bb_trusteeSetElectionChallenge(String electionID, byte[] signedChal) {
        double time = 0;
        long startTime = 0;
        long stopTime = 0;

        //timein
        startTime = System.currentTimeMillis();
        setElectionChallenge(electionID, signedChal);
        stopTime = System.currentTimeMillis();

        time = stopTime - startTime;

        return time;
    }

    
    
        /*
     * testBB_ECgetElection2Signs
     */
    public void testBB_EC_getElection2Signs() {
        double[] results = bb_getElection2Signs();
        String testName = "TestBB: BB_EC_getElection2Signs";
        EVIVTestResults testResults = TestUtils.createResults(testName, results);

        stor.addResult(testName, testResults);
    }

    private double[] bb_getElection2Signs() {

        double[] time;


        List<ElectionPartialInfo> electionList = getElectionList();

        time = new double[electionList.size()];


        int i = 0;
        for (ElectionPartialInfo einfo : electionList) {

            time[i] = bb_getElection2Sign(einfo.getElectionID());
            i++;
        }

        return time;
    }

    private double bb_getElection2Sign(String electionID) {
        double time = 0;
        long startTime = 0;
        long stopTime = 0;


        //timein
        startTime = System.currentTimeMillis();
        byte[] election2Sign = getElection2Sign(electionID);
        stopTime = System.currentTimeMillis();
        
        stor.addElection2SignKPub(electionID, election2Sign);

        time = stopTime - startTime;

        return time;
    }

    
    
    
    
     /*
     * testBB_EC_UpdateElections
     */
    public void testBB_EC_signElection() {
        double[] results = bb_ecSignElections();
        String testName = "TestBB: BB_EC_SignElection";
        EVIVTestResults testResults = TestUtils.createResults(testName, results);


        stor.addResult(testName, testResults);
//        printResults("initVSTs " + ")", results);
    }

    private double[] bb_ecSignElections() {

        double[] time;
        
        Set<String> signedElectionKeys = stor.getSignedElectionKeys();

        time = new double[signedElectionKeys.size()];

        int i = 0;
        for (String eid : signedElectionKeys) {

            byte[] sinfo = stor.getSignedElection(eid);
            
            time[i] = bb_ecSignElection(eid, sinfo);
            i++;

        }

        return time;
    }

    private double bb_ecSignElection(String electionID, byte[] signedInfo) {
        double time = 0;
        long startTime = 0;
        long stopTime = 0;

        //timein
        startTime = System.currentTimeMillis();
        signElection(electionID, signedInfo);
        stopTime = System.currentTimeMillis();

        time = stopTime - startTime;

        return time;
    }

    
    
    /*
     * testBB_ER_registerVoters
     */
     public void testBB_ER_registerVoters() {
        double[] results = bb_er_registerVoters();
        String testName = "TestBB: BB_ER_registerVoters";
        EVIVTestResults testResults = TestUtils.createResults(testName, results);

        stor.addResult(testName, testResults);
    }

    private double[] bb_er_registerVoters() {

        double[] time;
        
        Set<String> election2RegisterKeys = stor.getElection2RegisterKeys();
        Set<String> vstKeys = stor.getVSTtKeys();
        
        time = new double[election2RegisterKeys.size() * vstKeys.size()];


        int i = 0;
        for (String electionID : election2RegisterKeys) {
            for (String voterID : vstKeys) {
                time[i] = bb_er_registerVoter(electionID, voterID);
                i++;
            }
        }

        return time;
    }

    private double bb_er_registerVoter(String electionID, String voterID) {
        double time = 0;
        long startTime = 0;
        long stopTime = 0;


        //timein
        startTime = System.currentTimeMillis();
        registerVoter(electionID, voterID);
        stopTime = System.currentTimeMillis();

        time = stopTime - startTime;
        
        return time;
    }
    
    
    
       
    public void testBB_BBox_deliverVote() {
        double[] results = bb_bbox_deliverVotes();
        String testName = "TestBB: BBox_deliverVote";
        EVIVTestResults testResults = TestUtils.createResults(testName, results);

        stor.addResult(testName, testResults);
    }

    private double[] bb_bbox_deliverVotes() {

        double[] time;
  
        Set<Pair<String, String>> voteKeys = stor.getVote2SignKeys();
        
        time = new double[voteKeys.size()];

        

        int i = 0;
        for (Pair<String, String> voterElectionPair : voteKeys) {
              
            String voterID = voterElectionPair.getFirst();
            String electionID = voterElectionPair.getSecond();
            byte[] vote = stor.getVote2Sign(voterElectionPair);
            
            time[i] = bb_bbox_deliverVote(voterID, electionID, vote);
            i++;
        }

        return time;
    }

    private double bb_bbox_deliverVote(String voterID, String electionID, byte[] vote) {
        double time = 0;
        long startTime = 0;
        long stopTime = 0;


        //timein
        startTime = System.currentTimeMillis();
        deliverVote(electionID, voterID, vote);
        stopTime = System.currentTimeMillis();

        time = stopTime - startTime;
        
        return time;
    }

    
    
    
    /*
     * testBB_Trustee_getVotes
     */
    public void testBB_Trustee_getVotes() {
        double[] results = bb_trusteeGetVotes();
        String testName = "TestBB: BB_Trustee_getVotes";
        EVIVTestResults testResults = TestUtils.createResults(testName, results);

        stor.addResult(testName, testResults);
    }

    private double[] bb_trusteeGetVotes() {

        double[] time;
        //       List<Voters> votersList = getVotersList();
        
        Set<String> election2RegisterKeys = stor.getElection2RegisterKeys();
        
        time = new double[election2RegisterKeys.size()];


        int i = 0;
        for (String electionID : election2RegisterKeys) {

            time[i] = bb_trusteeGetVote(electionID);
            i++;
        }

        return time;
    }

    private double bb_trusteeGetVote(String electionID) {
        double time = 0;
        long startTime = 0;
        long stopTime = 0;

        List<Votes> votes = null;
        
        //timein
        startTime = System.currentTimeMillis();
        votes = getVotes(electionID);
        stopTime = System.currentTimeMillis();

        time = stopTime - startTime;
        
        stor.addVotes2Aggregate(electionID, votes);

        return time;
    }

    
    
    
    /*
     * testBB_Trustee_setElectionKeys
     */
    public void testBB_Trustee_setAggregatedVotes() {
        double[] results = bb_trusteeSetAggregatedVotes();
        String testName = "TestBB: BB_Trustee_SetAggregatedVotes";
        EVIVTestResults testResults = TestUtils.createResults(testName, results);


        stor.addResult(testName, testResults);
//        printResults("initVSTs " + ")", results);
    }

    private double[] bb_trusteeSetAggregatedVotes() {

        double[] time;
        
        Set<String> aggregatedVoteKeys = stor.getAggregatedVoteKeys();
        
        time = new double[aggregatedVoteKeys.size()];

        int i = 0;
        for (String eid : aggregatedVoteKeys) {
            
            byte[] aggregatedVote = stor.getAggregatedVote(eid);
            
            time[i] = bb_trusteeSetAggregatedVotes(eid, aggregatedVote);
            i++;

        }

        return time;
    }

    private double bb_trusteeSetAggregatedVotes(String electionID, byte[] aggVotes) {
        double time = 0;
        long startTime = 0;
        long stopTime = 0;

        //timein
        startTime = System.currentTimeMillis();
        setAggregatedVotes(electionID, aggVotes);
        stopTime = System.currentTimeMillis();

        time = stopTime - startTime;

        return time;
    }

    
    
    
        /*
     * testBB_Trustee_getAggregatedVotes
     */
    public void testBB_Trustee_getAggregatedVotes() {
        double[] results = bb_trusteeGetAggregatedVotes();
        String testName = "TestBB: BB_Trustee_GetAggregatedVotes";
        EVIVTestResults testResults = TestUtils.createResults(testName, results);

        stor.addResult(testName, testResults);
    }

    private double[] bb_trusteeGetAggregatedVotes() {

        double[] time;
        //       List<Voters> votersList = getVotersList();
        
        Set<String> election2RegisterKeys = stor.getElection2RegisterKeys();
        
        time = new double[election2RegisterKeys.size()];


        int i = 0;
        for (String electionID : election2RegisterKeys) {

            time[i] = bb_trusteeGetAggregatedVotes(electionID);
            i++;
        }

        return time;
    }

    private double bb_trusteeGetAggregatedVotes(String electionID) {
        double time = 0;
        long startTime = 0;
        long stopTime = 0;

        byte[] aggregatedVotes = null;
        
        //timein
        startTime = System.currentTimeMillis();
        aggregatedVotes = getAggregatedVotes(electionID);
        stopTime = System.currentTimeMillis();

        time = stopTime - startTime;

        //stor.addAggregatedVote(electionID, aggregatedVotes);

        
        return time;
    }

    
        /*
     * testBB_Trustee_setDecryptedAggregatedVotes
     */
    public void testBB_Trustee_setDecryptedAggregatedVotes() {
        double[] results = bb_trusteeSetDecryptedAggregatedVotes();
        String testName = "TestBB: BB_Trustee_SetDecryptedAggregatedVotes";
        EVIVTestResults testResults = TestUtils.createResults(testName, results);


        stor.addResult(testName, testResults);
//        printResults("initVSTs " + ")", results);
    }

    private double[] bb_trusteeSetDecryptedAggregatedVotes() {

        double[] time;
        
        Set<String> decryptedAggregatedVoteKeys = stor.getDecryptedAggregatedVoteKeys();
        
        time = new double[decryptedAggregatedVoteKeys.size()];

        int i = 0;
        for (String eid : decryptedAggregatedVoteKeys) {
            
            byte[] decryptedAggregatedVote = stor.getDecryptedAggregatedVote(eid);
            
            time[i] = bb_trusteeSetDecryptedAggregatedVotes(eid, decryptedAggregatedVote);
            i++;

        }

        return time;
    }

    private double bb_trusteeSetDecryptedAggregatedVotes(String electionID, byte[] decAggVotes) {
        double time = 0;
        long startTime = 0;
        long stopTime = 0;

        //timein
        startTime = System.currentTimeMillis();
        setDecryptedAggregatedVotes(electionID, decAggVotes);
        stopTime = System.currentTimeMillis();

        time = stopTime - startTime;

        return time;
    }

    
    
    
    /*
     * testBB_Trustee_getVotes
     */
    public void testBB_Trustee_getDecryptedAggregatedVotes() {
        double[] results = bb_trusteeGetDecryptedAggregatedVotes();
        String testName = "TestBB: BB_TrusteeGetDecryptedAggregatedVotes";
        EVIVTestResults testResults = TestUtils.createResults(testName, results);

        stor.addResult(testName, testResults);
    }

    private double[] bb_trusteeGetDecryptedAggregatedVotes() {

        double[] time;
        //       List<Voters> votersList = getVotersList();
        
        Set<String> election2RegisterKeys = stor.getElection2RegisterKeys();
        
        time = new double[election2RegisterKeys.size()];


        int i = 0;
        for (String electionID : election2RegisterKeys) {

            time[i] = bb_trusteeGetDecryptedAggregatedVote(electionID);
            i++;
        }

        return time;
    }

    private double bb_trusteeGetDecryptedAggregatedVote(String electionID) {
        double time = 0;
        long startTime = 0;
        long stopTime = 0;

        byte[] decryptedAggregatedVotes = null;
        
        //timein
        startTime = System.currentTimeMillis();
        decryptedAggregatedVotes = getDecryptedAggregatedVotes(electionID);
        stopTime = System.currentTimeMillis();

        time = stopTime - startTime;

        //stor.addDecryptedAggregatedVote(electionID, decryptedAggregatedVotes);
        
        
        return time;
    }
    
    
    
    
            /*
     * testBB_Trustee_setDecryptedAggregatedVotes
     */
    public void testBB_Trustee_setElectionResult() {
        double[] results = bb_trusteeSetElectionResults();
        String testName = "TestBB: BB_Trustee_SetElectionResult";
        EVIVTestResults testResults = TestUtils.createResults(testName, results);


        stor.addResult(testName, testResults);
//        printResults("initVSTs " + ")", results);
    }

    private double[] bb_trusteeSetElectionResults() {

        double[] time;
        
        Set<String> electionResultsKeys = stor.getElectionResultsKeys();
        
        time = new double[electionResultsKeys.size()];

        int i = 0;
        for (String eid : electionResultsKeys) {
            
            byte[] electionResults = stor.getElectionResults(eid);
            
            time[i] = bb_trusteeSetElectionResult(eid, electionResults);
            i++;

        }

        return time;
    }

    private double bb_trusteeSetElectionResult(String electionID, byte[] results) {
        double time = 0;
        long startTime = 0;
        long stopTime = 0;

        //timein
        startTime = System.currentTimeMillis();
        setResults(electionID, results);
        stopTime = System.currentTimeMillis();

        time = stopTime - startTime;

        return time;
    }

    
    
    
    /*
     * Funções de acesso aos WebServices
     */
    private static String addVoter(java.lang.String voterID, java.lang.String voterCertificate) {
        eviv.ecapp.bbclient.BBEC_Service service = new eviv.ecapp.bbclient.BBEC_Service();
        eviv.ecapp.bbclient.BBEC port = service.getBBECPort();
        return port.addVoter(voterID, voterCertificate);
    }

    private static java.util.List<eviv.tests.ws.bb_ec.ElectionPartialInfo> getElectionList() {
        eviv.tests.ws.bb_ec.BBEC_Service service = new eviv.tests.ws.bb_ec.BBEC_Service();
        eviv.tests.ws.bb_ec.BBEC port = service.getBBECPort();
        return port.getElectionList();
    }

    private static java.util.List<java.lang.Object> getVotersList() {
        eviv.tests.ws.bb_ec.BBEC_Service service = new eviv.tests.ws.bb_ec.BBEC_Service();
        eviv.tests.ws.bb_ec.BBEC port = service.getBBECPort();
        return port.getVotersList();
    }

    private static ElectionInfo getElectionInfo(java.lang.String electionID) {
        eviv.tests.ws.bb_ec.BBEC_Service service = new eviv.tests.ws.bb_ec.BBEC_Service();
        eviv.tests.ws.bb_ec.BBEC port = service.getBBECPort();
        return port.getElectionInfo(electionID);
    }

    private static Voters getVoterInfo(java.lang.String voterID) {
        eviv.tests.ws.bb_ec.BBEC_Service service = new eviv.tests.ws.bb_ec.BBEC_Service();
        eviv.tests.ws.bb_ec.BBEC port = service.getBBECPort();
        return port.getVoterInfo(voterID);
    }

    private static Boolean addElection(java.lang.String electionID, java.lang.String electionDesc) {
        eviv.tests.ws.bb_ec.BBEC_Service service = new eviv.tests.ws.bb_ec.BBEC_Service();
        eviv.tests.ws.bb_ec.BBEC port = service.getBBECPort();
        return port.addElection(electionID, electionDesc);
    }

    private static Boolean updateElection(java.lang.String electionID, eviv.tests.ws.bb_ec.ElectionInfo electionInfo) {
        eviv.tests.ws.bb_ec.BBEC_Service service = new eviv.tests.ws.bb_ec.BBEC_Service();
        eviv.tests.ws.bb_ec.BBEC port = service.getBBECPort();
        return port.updateElection(electionID, electionInfo);
    }

    private static java.util.List<java.lang.Byte> setElectionKey(java.lang.String electionID, java.util.List<java.lang.Byte> electionKPub, java.util.List<java.lang.Byte> electionSignedKPub) {
        eviv.tests.ws.bb_trustees.BBTrustees_Service service = new eviv.tests.ws.bb_trustees.BBTrustees_Service();
        eviv.tests.ws.bb_trustees.BBTrustees port = service.getBBTrusteesPort();
        return port.setElectionKey(electionID, electionKPub, electionSignedKPub);
    }

    private static byte[] setElectionChallenge(java.lang.String electionID, byte[] electionSignedChallenge) {
        eviv.tests.ws.bb_trustees.BBTrustees_Service service = new eviv.tests.ws.bb_trustees.BBTrustees_Service();
        eviv.tests.ws.bb_trustees.BBTrustees port = service.getBBTrusteesPort();
        return port.setElectionChallenge(electionID, electionSignedChallenge);
    }

    private static byte[] getElection2Sign(java.lang.String electionID) {
        eviv.tests.ws.bb_ec.BBEC_Service service = new eviv.tests.ws.bb_ec.BBEC_Service();
        eviv.tests.ws.bb_ec.BBEC port = service.getBBECPort();
        return port.getElection2Sign(electionID);
    }

    private static Boolean signElection(java.lang.String electionID, byte[] signedElection) {
        eviv.ecapp.bbclient.BBEC_Service service = new eviv.ecapp.bbclient.BBEC_Service();
        eviv.ecapp.bbclient.BBEC port = service.getBBECPort();
        return port.signElection(electionID, signedElection);
    }

    private static Boolean registerVoter(java.lang.String electionID, java.lang.String voterID) {
        eviv.tests.ws.bb_er.BBER_Service service = new eviv.tests.ws.bb_er.BBER_Service();
        eviv.tests.ws.bb_er.BBER port = service.getBBERPort();
        return port.registerVoter(electionID, voterID);
    }

    private static Boolean deliverVote(java.lang.String electionID, java.lang.String voterID, byte[] vote) {
        eviv.tests.ws.bb_bbox.BBBBox_Service service = new eviv.tests.ws.bb_bbox.BBBBox_Service();
        eviv.tests.ws.bb_bbox.BBBBox port = service.getBBBBoxPort();
        return port.deliverVote(electionID, voterID, vote);
    }

    private static java.util.List<eviv.tests.ws.bb_trustees.Votes> getVotes(java.lang.String electionID) {
        eviv.tests.ws.bb_trustees.BBTrustees_Service service = new eviv.tests.ws.bb_trustees.BBTrustees_Service();
        eviv.tests.ws.bb_trustees.BBTrustees port = service.getBBTrusteesPort();
        return port.getVotes(electionID);
    }

    private static Boolean setAggregatedVotes(java.lang.String electionID, byte[] electionAggregatedVotes) {
        eviv.tests.ws.bb_trustees.BBTrustees_Service service = new eviv.tests.ws.bb_trustees.BBTrustees_Service();
        eviv.tests.ws.bb_trustees.BBTrustees port = service.getBBTrusteesPort();
        return port.setAggregatedVotes(electionID, electionAggregatedVotes);
    }

    private static byte[] getAggregatedVotes(java.lang.String electionID) {
        eviv.tests.ws.bb_trustees.BBTrustees_Service service = new eviv.tests.ws.bb_trustees.BBTrustees_Service();
        eviv.tests.ws.bb_trustees.BBTrustees port = service.getBBTrusteesPort();
        return port.getAggregatedVotes(electionID);
    }

    private static Boolean setDecryptedAggregatedVotes(java.lang.String electionID, byte[] electionDecryptedAggregatedVotes) {
        eviv.tests.ws.bb_trustees.BBTrustees_Service service = new eviv.tests.ws.bb_trustees.BBTrustees_Service();
        eviv.tests.ws.bb_trustees.BBTrustees port = service.getBBTrusteesPort();
        return port.setDecryptedAggregatedVotes(electionID, electionDecryptedAggregatedVotes);
    }
    
    
    private static byte[] getDecryptedAggregatedVotes(java.lang.String electionID) {
        eviv.tests.ws.bb_trustees.BBTrustees_Service service = new eviv.tests.ws.bb_trustees.BBTrustees_Service();
        eviv.tests.ws.bb_trustees.BBTrustees port = service.getBBTrusteesPort();
        return port.getDecryptedAggregatedVotes(electionID);
    }

    private static Boolean setResults(java.lang.String electionID, byte[] electionResults) {
        eviv.tests.ws.bb_trustees.BBTrustees_Service service = new eviv.tests.ws.bb_trustees.BBTrustees_Service();
        eviv.tests.ws.bb_trustees.BBTrustees port = service.getBBTrusteesPort();
        return port.setResults(electionID, electionResults);
    }



    
    
    
}

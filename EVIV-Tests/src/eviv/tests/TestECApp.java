/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eviv.tests;

import eviv.VST.VoterSecurityToken;
import eviv.ecapp.ECAppRMI;
import eviv.ecapp.ECAppUtil;
import eviv.tests.utils.EVIVTestResults;
import eviv.tests.utils.TestStorage;
import eviv.tests.utils.TestUtils;
import java.rmi.RemoteException;
import java.security.PublicKey;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author abrioso
 */
public class TestECApp {
    
    private static final String EC_CERT_LOC = "certs/ec";

    private ECAppRMI ecApp = null;
    
    private TestStorage stor = null;

    
    public TestECApp(TestStorage storage) {
        this.stor = storage;
        this.ecApp = createECApp();
        
        TestUtils.loadBC();
    }
    
    private ECAppRMI createECApp() {
        
        ECAppRMI ecAppRMI = new ECAppRMI(EC_CERT_LOC + ".crt", EC_CERT_LOC + ".key");
                
        String certName = null; 
        try {
            certName = ecAppRMI.getCertificate().getSubjectDN().getName();
        } catch (RemoteException ex) {
            Logger.getLogger(TestECApp.class.getName()).log(Level.SEVERE, null, ex);
        }
                
        
        System.out.println("ECApp with certName = " + certName + " opened");
        
        return ecAppRMI;
        
    }
    
    public void testCreateCertificates() {
        double[] results = createCertificates(stor.getVstMap());
        String testName = "TestECApp: createCertificates";
        EVIVTestResults testResults = TestUtils.createResults(testName, results);
        
        
        stor.addResult(testName, testResults);
//        printResults("initVSTs " + ")", results);
    } 

    
    private double[] createCertificates(HashMap<String, VoterSecurityToken> vstList) {
        
        double[] time;
        
        Set voterIDs = stor.getVSTtKeys();
        
        time = new double[voterIDs.size()]; 

        int i = 0;
        for (Object vid : voterIDs) {
            
            VoterSecurityToken vst = vstList.get(vid);
            
            time[i] = createCertificate((String) vid, vst.getKeyPair().getPublic());
            i++;
        }
       
        return time;
    }
    
    private double createCertificate(String voterID, PublicKey kpub){
        double time = 0;
        long startTime = 0;
        long stopTime = 0;
        
        X509Certificate vstCert = null; 
                
        //timein
        startTime = System.currentTimeMillis();
        try {
            vstCert = ecApp.createCertificate(voterID, kpub);
        } catch (RemoteException ex) {
            Logger.getLogger(TestECApp.class.getName()).log(Level.SEVERE, null, ex);
        }
        stopTime = System.currentTimeMillis();
        
        time = stopTime - startTime;
        
        stor.addCert(voterID, vstCert);
        
        return time;
    }
    
      public void testSignElection() {
        double[] results = signElections();
        String testName = "TestECApp: signElection";
        EVIVTestResults testResults = TestUtils.createResults(testName, results);
        
        
        stor.addResult(testName, testResults);
//        printResults("initVSTs " + ")", results);
    } 

    
    private double[] signElections() {
        
        double[] time;
        
        Set<String> electionIDs = stor.getElection2SignKPubKeys();
        
        time = new double[electionIDs.size()]; 

        int i = 0;
        for (String eid : electionIDs) {
            byte[] election2SignKPub = stor.getElection2SignKPub(eid);
            
            time[i] = signElection(eid, election2SignKPub);
            i++;
        }
       
        return time;
    }
    
    private double signElection(String electionID, byte[] electionInfo){
        double time = 0;
        long startTime = 0;
        long stopTime = 0;
                  
        byte[] signElection = null;
        //timein
        startTime = System.currentTimeMillis();
        try {
            signElection = ecApp.signElection(electionInfo);
        } catch (RemoteException ex) {
            Logger.getLogger(TestECApp.class.getName()).log(Level.SEVERE, null, ex);
        }
            
        stopTime = System.currentTimeMillis();
        
        time = stopTime - startTime;
        
        stor.addSignedElection(electionID, signElection);
        
        return time;
    }
    
    
}

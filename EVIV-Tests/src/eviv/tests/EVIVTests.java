/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eviv.tests;

import eviv.tests.utils.TestStorage;

/**
 *
 * @author abrioso
 */
public class EVIVTests {

    private static final TestStorage data = new TestStorage();

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here


        TestVST tvst = new TestVST(data, 100);

        TestECApp tecapp = new TestECApp(data);

        TestTrusteeApp ttrusteeapp = new TestTrusteeApp(data);

        TestBB tbb = new TestBB(data);
        
        TestER ter = new TestER(data);
        
        TestBBox tbbox = new TestBBox(data);

        //Fase 0 -- Init VST + AddVoter
        
        tvst.testInitVST();

        tecapp.testCreateCertificates();

        tbb.testBB_EC_AddVoters();

        tbb.testBB_getVotersLists(10);

        tbb.testBB_getVoterInfos();

        //Fase 1 -- AddElection + genKeys + genChalleng + signInfo
        
        tbb.testBB_EC_AddElections(10);

        tbb.testBB_getElectionLists(10);

        tbb.testBB_EC_UpdateElections();

        tbb.testBB_getElectionInfos();

        ttrusteeapp.testGenerateElectionKeys();
        
        ttrusteeapp.testSignElectionKeys();
        
        tbb.testBB_Trustee_setElectionKeys();
        
        tbb.testBB_EC_getElection2Signs();
        
        tecapp.testSignElection();
        
        tbb.testBB_EC_signElection();
        
        ttrusteeapp.testGenerateElectionChallenge();

        tbb.testBB_Trustee_setElectionChallenge();
        
        
        // Fase 2 -- Registo
        
        ter.testER_getElectionLists(10);
        
        ter.testER_getElectionInfos();
   
        tvst.testAddElection();
        
            // Escolher um dos dois, pois o ER envia para o BB
        ter.testER_registerVoters();     
        // tbb.testBB_ER_registerVoters();
        
        // Fase 3 -- Votação
        
        tvst.testGenerateVotes(1);
        
            // Escolher um dos dois, pois o ER envia para o BB    
        tbbox.testBBox_sendVote();
        //tbb.testBB_BBox_deliverVote();
        
        
        // Fase 4 -- Contagem e Resultados
        
        tbb.testBB_Trustee_getVotes();
        
        ttrusteeapp.testAggregateVotes();

        tbb.testBB_Trustee_setAggregatedVotes();
        
        tbb.testBB_Trustee_getAggregatedVotes();
        
        ttrusteeapp.testDecryptAggregateVotes();
        
        tbb.testBB_Trustee_setDecryptedAggregatedVotes();
        
        tbb.testBB_Trustee_getDecryptedAggregatedVotes();
        
        ttrusteeapp.testGenerateResults();
        
        tbb.testBB_Trustee_setElectionResult();
        
        // Print Results
        TestStorage.printResults();
        
        TestStorage.printLatexResults();


    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eviv.tests;

import eviv.tests.utils.EVIVTestResults;
import eviv.tests.utils.TestStorage;
import eviv.tests.utils.TestUtils;
import eviv.tests.ws.bb_er.ElectionInfo;
import eviv.tests.ws.bb_er.ElectionPartialInfo;
import java.util.List;
import java.util.Set;

/**
 *
 * Test the following functions: - registerVoter(electionID, voterID) -- Needs
 * VST??
 *
 *
 *
 * @author abrioso
 */
public class TestER {

    private TestStorage stor = null;

    public TestER(TestStorage storage) {
        this.stor = storage;

        TestUtils.loadBC();
    }

    public void testER_getElectionLists(int num) {
        double[] results = er_getElectionLists(num);
        String testName = "TestER: ER_getElectionLists";
        EVIVTestResults testResults = TestUtils.createResults(testName, results);

        stor.addResult(testName, testResults);
    }

    private double[] er_getElectionLists(int num) {

        double[] time;

        time = new double[num];

        for (int j = 0; j < num; j++) {
            time[j] = er_getElectionList();
        }

        return time;
    }

    private double er_getElectionList() {
        double time = 0;
        long startTime = 0;
        long stopTime = 0;


        //timein
        startTime = System.currentTimeMillis();
        getElectionList();
        stopTime = System.currentTimeMillis();

        time = stopTime - startTime;

        return time;
    }

    public void testER_getElectionInfos() {
        double[] results = er_getElectionInfos();
        String testName = "TestER: ER_getElectionInfos";
        EVIVTestResults testResults = TestUtils.createResults(testName, results);

        stor.addResult(testName, testResults);
    }

    private double[] er_getElectionInfos() {

        double[] time;
        List<ElectionPartialInfo> electionList = getElectionList();

        time = new double[electionList.size()];


        int i = 0;
        for (ElectionPartialInfo einfo : electionList) {

            time[i] = er_getElectionInfo(einfo.getElectionID());
            i++;
        }

        return time;
    }

    private double er_getElectionInfo(String electionID) {
        double time = 0;
        long startTime = 0;
        long stopTime = 0;


        //timein
        startTime = System.currentTimeMillis();
        ElectionInfo electionInfo = getElectionInfo(electionID);
        stopTime = System.currentTimeMillis();

        time = stopTime - startTime;

        stor.addElection2Register(electionID, electionInfo);
        
        return time;
    }

    
    
    
     public void testER_registerVoters() {
        double[] results = er_registerVoters();
        String testName = "TestER: ER_registerVoters";
        EVIVTestResults testResults = TestUtils.createResults(testName, results);

        stor.addResult(testName, testResults);
    }

    private double[] er_registerVoters() {

        double[] time;
        
        Set<String> election2RegisterKeys = stor.getElection2RegisterKeys();
        Set<String> vstKeys = stor.getVSTtKeys();
        
        time = new double[election2RegisterKeys.size() * vstKeys.size()];


        int i = 0;
        for (String electionID : election2RegisterKeys) {
            for (String voterID : vstKeys) {
                time[i] = er_registerVoter(electionID, voterID);
                i++;
            }
        }

        return time;
    }

    private double er_registerVoter(String electionID, String voterID) {
        double time = 0;
        long startTime = 0;
        long stopTime = 0;


        //timein
        startTime = System.currentTimeMillis();
        registerVoter(electionID, voterID);
        stopTime = System.currentTimeMillis();

        time = stopTime - startTime;
        
        return time;
    }

    
    
    
    private static java.util.List<eviv.tests.ws.bb_er.ElectionPartialInfo> getElectionList() {
        eviv.tests.ws.bb_er.BBER_Service service = new eviv.tests.ws.bb_er.BBER_Service();
        eviv.tests.ws.bb_er.BBER port = service.getBBERPort();
        return port.getElectionList();
    }

    private static ElectionInfo getElectionInfo(java.lang.String electionID) {
        eviv.tests.ws.bb_er.BBER_Service service = new eviv.tests.ws.bb_er.BBER_Service();
        eviv.tests.ws.bb_er.BBER port = service.getBBERPort();
        return port.getElectionInfo(electionID);
    }

    private static Boolean registerVoter(java.lang.String electionID, java.lang.String voterID) {
        eviv.tests.ws.er.ElectionRegistration_Service service = new eviv.tests.ws.er.ElectionRegistration_Service();
        eviv.tests.ws.er.ElectionRegistration port = service.getElectionRegistrationPort();
        return port.registerVoter(electionID, voterID);
    }
    
    
    
}

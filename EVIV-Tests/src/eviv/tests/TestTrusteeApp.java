/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eviv.tests;

import eviv.commons.types.TrusteeElection;
import eviv.ecapp.ECAppUtil;
import eviv.tests.utils.EVIVTestResults;
import eviv.tests.utils.TestStorage;
import eviv.tests.utils.TestUtils;
import eviv.tests.ws.bb_trustees.ElectionInfo;
import eviv.tests.ws.bb_trustees.ElectionPartialInfo;
import eviv.tests.ws.bb_trustees.Votes;
import eviv.trusteeapp.TrusteeAppRMI;
import eviv.trusteeapp.TrusteeAppUtil;
import gsd.inescid.markpledge3.MP3VoteAndReceipt;
import java.io.*;
import java.rmi.RemoteException;
import java.security.SignedObject;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author abrioso
 */
public class TestTrusteeApp {

    private static final String TRUSTEE_CERT_LOC = "certs/trustee001";
    private TrusteeAppRMI trusteeApp = null;
    private TestStorage stor = null;

    public TestTrusteeApp(TestStorage storage) {
        this.stor = storage;
        this.trusteeApp = createTrusteeApp();

        TestUtils.loadBC();
    }

    private TrusteeAppRMI createTrusteeApp() {

        TrusteeAppRMI trusteeAppRMI = new TrusteeAppRMI(TRUSTEE_CERT_LOC + ".crt", TRUSTEE_CERT_LOC + ".key");

        String certName = null;
        try {
            certName = trusteeAppRMI.getCertificate().getSubjectDN().getName();
        } catch (RemoteException ex) {
            Logger.getLogger(TestTrusteeApp.class.getName()).log(Level.SEVERE, null, ex);
        }


        System.out.println("TrusteeApp with certName = " + certName + " opened");

        return trusteeAppRMI;

    }

    /*
     * testGenerateElectionKeys
     */
    public void testGenerateElectionKeys() {
        double[] results = generateElectionKeys();
        String testName = "TestTrusteeApp: GenerateElectionKeys";
        EVIVTestResults testResults = TestUtils.createResults(testName, results);

        stor.addResult(testName, testResults);
    }

    private double[] generateElectionKeys() {

        double[] time;

        List<eviv.bb.web.ElectionPartialInfo> electionList = getElectionList();

        time = new double[electionList.size()];

        int i = 0;
        for (eviv.bb.web.ElectionPartialInfo electionPartialInfo : electionList) {

            time[i] = generateElectionKey(electionPartialInfo.getElectionID());
            i++;

        }

        return time;
    }

    private double generateElectionKey(String electionID) {
        double time = 0;
        long startTime = 0;
        long stopTime = 0;

        ElectionInfo electionInfo = getElectionInfo(electionID);

        //      electionInfo.

        try {
            trusteeApp.addElection(electionID, TestUtils.convertElectionInfo(electionInfo));
        } catch (RemoteException ex) {
            Logger.getLogger(TestTrusteeApp.class.getName()).log(Level.SEVERE, null, ex);
        }


        byte[] generateElectionKey = null;

        //timein
        startTime = System.currentTimeMillis();
        try {
            generateElectionKey = trusteeApp.generateElectionKey(electionID);
        } catch (RemoteException ex) {
            Logger.getLogger(TestTrusteeApp.class.getName()).log(Level.SEVERE, null, ex);
        }
        stopTime = System.currentTimeMillis();



        time = stopTime - startTime;

        stor.addElectionKPub(electionID, generateElectionKey);
        
        TrusteeElection election = trusteeApp.getElection(electionID);
        stor.addTrusteeElection(electionID, election);
        

        return time;
    }

    
   
    /*
     * testSignElectionKeys
     */
    public void testSignElectionKeys() {
        double[] results = signElectionKeys();
        String testName = "TestTrusteeApp: SignElectionKeys";
        EVIVTestResults testResults = TestUtils.createResults(testName, results);

        stor.addResult(testName, testResults);
    }

    private double[] signElectionKeys() {

        double[] time;
       
        Set<String> electionKPubKeys = stor.getElectionKPubKeys();
        
        time = new double[electionKPubKeys.size()];

        int i = 0;
        for (String eid : electionKPubKeys) {
            
            byte[] electionKPub = stor.getElectionKPub(eid);
            
            time[i] = signElectionKey(eid, electionKPub);
            i++;

        }

        return time;
    }

    private double signElectionKey(String electionID, byte[] kpub) {
        double time = 0;
        long startTime = 0;
        long stopTime = 0;

        //timein
        startTime = System.currentTimeMillis();
       
        //       trusteeApp.signElectionKey(electionID, kpub);
        
        byte[] signElectionKey = null;
        try {
            signElectionKey = trusteeApp.signElectionKey(electionID);
        } catch (RemoteException ex) {
            Logger.getLogger(TestTrusteeApp.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        stopTime = System.currentTimeMillis();



        time = stopTime - startTime;

        stor.addElectionSigKPub(electionID, signElectionKey);

        return time;
    }

    
 
    
    
    
    
    /*
     * testGenerateElectionChallenge
     */
    public void testGenerateElectionChallenge() {
        double[] results = generateElectionChallenges();
        String testName = "TestTrusteeApp: GenerateElectionChallenge";
        EVIVTestResults testResults = TestUtils.createResults(testName, results);

        stor.addResult(testName, testResults);
    }

    private double[] generateElectionChallenges() {

        double[] time;

        List<eviv.bb.web.ElectionPartialInfo> electionList = getElectionList();

        time = new double[electionList.size()];

        int i = 0;
        for (eviv.bb.web.ElectionPartialInfo electionPartialInfo : electionList) {

            time[i] = generateElectionChallenge(electionPartialInfo.getElectionID());
            i++;

        }

        return time;
    }

    private double generateElectionChallenge(String electionID) {
        double time = 0;
        long startTime = 0;
        long stopTime = 0;

        byte[] generateElectionKey = null;

        //timein
        startTime = System.currentTimeMillis();
        
        byte[] generateChallenge = trusteeApp.generateChallenge(electionID);
        
        stopTime = System.currentTimeMillis();

        time = stopTime - startTime;

        stor.addElectionChallenge(electionID, generateChallenge);

        return time;
    }

    
    
    
    /*
     * testAggregateVotes
     */
    public void testAggregateVotes() {
        double[] results = aggregateVotes();
        String testName = "TestTrusteeApp: AggregateVotes";
        EVIVTestResults testResults = TestUtils.createResults(testName, results);

        stor.addResult(testName, testResults);
    }

    private double[] aggregateVotes() {

        double[] time;
       
        Set<String> votes2AggregateKeys = stor.getVotes2AggregateKeys();
        
        time = new double[votes2AggregateKeys.size()];

        int i = 0;
        for (String eid : votes2AggregateKeys) {
            
            List<Votes> votes2Aggregate = stor.getVotes2Aggregate(eid);
            
            time[i] = aggregateVote(eid, votes2Aggregate);
            i++;

        }

        return time;
    }

    private double aggregateVote(String electionID, List<Votes> rawVotes) {
        double time = 0;
        long startTime = 0;
        long stopTime = 0;

        //            byte[] aggVotesBytes = TrusteeAppUtil.aggregateVotes(electionID, votes);

        
        //timein
        startTime = System.currentTimeMillis();
       
        
        ArrayList<MP3VoteAndReceipt> votes_list = new ArrayList<MP3VoteAndReceipt>();
        
        for (Votes v : rawVotes) {
            
            MP3VoteAndReceipt vote = null; 
            
            try {
                ByteArrayInputStream bis = new ByteArrayInputStream(v.getVoteInfo());
                ObjectInput in = new ObjectInputStream(bis);
            
                SignedObject signedVote = (SignedObject) in.readObject();
            
                bis.close();
                in.close();
                
                vote = (MP3VoteAndReceipt) signedVote.getObject();
            
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(TestTrusteeApp.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(TestTrusteeApp.class.getName()).log(Level.SEVERE, null, ex);
            }

                        
                votes_list.add(vote);
            
        }

        MP3VoteAndReceipt[] votes = votes_list.toArray(new MP3VoteAndReceipt[0]);

        byte[] signedAggVotes = null;

        byte[] byteVotes = null;
        
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ObjectOutput out = new ObjectOutputStream(bos);

            out.writeObject(votes);
            byteVotes = bos.toByteArray();

            out.close();
            bos.close();

        } catch (IOException ex) {
            Logger.getLogger(TestTrusteeApp.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        try {
            trusteeApp.setElectionNumVotes(electionID, votes.length);
            signedAggVotes = trusteeApp.aggregateAndSignVotes(electionID, byteVotes);
        } catch (RemoteException ex) {
            Logger.getLogger(TestTrusteeApp.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        //timeout
        stopTime = System.currentTimeMillis();



        time = stopTime - startTime;

        stor.addAggregatedVote(electionID, signedAggVotes);

        return time;
    }

    
    /*
     * testDecryptAggregateVotes
     */
    public void testDecryptAggregateVotes() {
        double[] results = decryptAggregateVotes();
        String testName = "TestTrusteeApp: DecryptAggregateVotes";
        EVIVTestResults testResults = TestUtils.createResults(testName, results);

        stor.addResult(testName, testResults);
    }

    private double[] decryptAggregateVotes() {

        double[] time;
        
        Set<String> aggregatedVoteKeys = stor.getAggregatedVoteKeys();
                
        time = new double[aggregatedVoteKeys.size()];

        int i = 0;
        for (String eid : aggregatedVoteKeys) {
            
            byte[] aggregatedVote = stor.getAggregatedVote(eid);
            
            time[i] = decryptAggregateVote(eid, aggregatedVote);
            i++;

        }

        return time;
    }

    private double decryptAggregateVote(String electionID, byte[] aggVotes) {
        double time = 0;
        long startTime = 0;
        long stopTime = 0;

        //            byte[] aggVotesBytes = TrusteeAppUtil.aggregateVotes(electionID, votes);

        byte[] decryptAggregatedVotes = null;
        
        //timein
        startTime = System.currentTimeMillis();
        
        decryptAggregatedVotes = trusteeApp.decryptAggregatedVotes(electionID, aggVotes);
        
        //timeout
        stopTime = System.currentTimeMillis();



        time = stopTime - startTime;

        stor.addDecryptedAggregatedVote(electionID, decryptAggregatedVotes);

        return time;
    }
 
    
    
    /*
     * testGenerateResults
     */
    public void testGenerateResults() {
        double[] results = generateResults();
        String testName = "TestTrusteeApp: GenerateResults";
        EVIVTestResults testResults = TestUtils.createResults(testName, results);

        stor.addResult(testName, testResults);
    }

    private double[] generateResults() {

        double[] time;
        
        Set<String> decryptedAggregatedVoteKeys = stor.getDecryptedAggregatedVoteKeys();
        
        time = new double[decryptedAggregatedVoteKeys.size()];

        int i = 0;
        for (String eid : decryptedAggregatedVoteKeys) {
            
            byte[] aggregatedVote = stor.getAggregatedVote(eid);
            byte[] decryptedAggregatedVote = stor.getDecryptedAggregatedVote(eid);
            
            time[i] = generateResult(eid, aggregatedVote, decryptedAggregatedVote);
            i++;

        }

        return time;
    }

    private double generateResult(String electionID, byte[] aggVotes, byte[] decAggVotes) {
        double time = 0;
        long startTime = 0;
        long stopTime = 0;

        //            byte[] aggVotesBytes = TrusteeAppUtil.aggregateVotes(electionID, votes);

        byte[] generateAndSignResults = null;
        
        //timein
        startTime = System.currentTimeMillis();
        
        generateAndSignResults = trusteeApp.generateAndSignResults(electionID, aggVotes, decAggVotes);
        
        //timeout
        stopTime = System.currentTimeMillis();



        time = stopTime - startTime;

        stor.addElectionResults(electionID, generateAndSignResults);

        return time;
    }
 
    
    
    
    /*
     * Funções de acesso ao WS BB_Trustee
     */
    private static java.util.List<eviv.bb.web.ElectionPartialInfo> getElectionList() {
        eviv.bb.web.BBTrustees_Service service = new eviv.bb.web.BBTrustees_Service();
        eviv.bb.web.BBTrustees port = service.getBBTrusteesPort();
        return port.getElectionList();
    }

    private static ElectionInfo getElectionInfo(java.lang.String electionID) {
        eviv.tests.ws.bb_trustees.BBTrustees_Service service = new eviv.tests.ws.bb_trustees.BBTrustees_Service();
        eviv.tests.ws.bb_trustees.BBTrustees port = service.getBBTrusteesPort();
        return port.getElectionInfo(electionID);
    }
}

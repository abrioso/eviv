/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eviv.tests.utils;

import eviv.commons.types.ElectionInfo;
import eviv.ecapp.ECAppUtil;
import java.io.IOException;
import java.io.StringWriter;
import java.math.BigInteger;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openssl.PEMWriter;

/**
 *
 * @author abrioso
 */
public class TestUtils {
   
    public static void loadBC() {
        if (java.security.Security.getProvider("BC") == null) {
            java.security.Security.addProvider(new BouncyCastleProvider());
        }
    }
    
    public static double getMax(double[] results) {
        
        double max = 0;
        
        for (int i = 0; i < results.length; i++) {
            if (results[i] > max) {
                max = results[i];
            }
        }
        
        return max;
        
    }
    
    public static double getMin(double[] results) {

        double min = 10000000;
        
        for (int i = 0; i < results.length; i++) {
            if (results[i] < min) {
                min = results[i];
            }
        }
        
        return min;
    }
    
    public static double getSum(double[] results) {
        double sum = 0;
        
        for (int i = 0; i < results.length; i++) {
            sum+= results[i];
        }
        
        return sum;
    }
    
    public static double getAverage(double[] results) {
        double sum = getSum(results);
        double average = sum / results.length;
        return average;
    }
    
    
    public static int getPosStrArray(String[] strArray, String str) {
        int pos = -1;
        for (int i = 0; i < strArray.length; i++) {
            if (strArray[i].equals(str)) {
                pos = i;
            }
        }
        return pos;
    }
           
    public static String[] initStrArray(String[] strArray) {
        for (int i = 0; i < strArray.length; i++) {
            strArray[i] = "";
        }
        return strArray;
    }
    
    
    public static EVIVTestResults createResults(String testName, double[] results) {
        
        int num = results.length;
        double min = TestUtils.getMin(results);
        double max = TestUtils.getMax(results);
        double avr = TestUtils.getAverage(results);
        double sum = TestUtils.getSum(results);
        
        
        
        System.out.println("Results for " + testName);
        System.out.println("    Min = " + min);
        System.out.println("    Avr = " + avr);
        System.out.println("    Max = " + max);
        System.out.println("    Sum = " + sum);
        
        EVIVTestResults testResults = new EVIVTestResults(num, min, avr, max, sum);

        return testResults;
        
    }
    
    
    public static String convertCert2String(X509Certificate cert) {

        String certString = null;

        if (cert != null) {
            StringWriter strWriter = new StringWriter();


            PEMWriter certWriter = new PEMWriter(strWriter);
            try {
                certWriter.writeObject(cert);
                certWriter.close();

                //     BufferedWriter certBuf = new BufferedWriter
            } catch (IOException ex) {
                Logger.getLogger(ECAppUtil.class.getName()).log(Level.SEVERE, null, ex);
            }


            certString = strWriter.toString();

        }
        return certString;

    }

    
    public static eviv.commons.types.ElectionInfo convertElectionInfo(eviv.tests.ws.bb_trustees.ElectionInfo electionInfo) {
        
        eviv.commons.types.ElectionInfo convElectionInfo =  new ElectionInfo(electionInfo.getElectionID());
        
        convElectionInfo.setElectionID(electionInfo.getElectionID());
        convElectionInfo.setElectionDesc(electionInfo.getElectionDesc());
        convElectionInfo.setNumCandidates(electionInfo.getNumCandidates());

        
        convElectionInfo.setElectionCandidates((ArrayList) electionInfo.getElectionCandidates());
        convElectionInfo.setElectionChallenge(electionInfo.getElectionChallenge());

//        convElectionInfo.setElectionPubKey(electionInfo.getElectionPubKey());
        
//        convElectionInfo.setElectionDates(electionInfo.getElectionDates().to);
        
        return convElectionInfo;
        
        
    }
    
    
}

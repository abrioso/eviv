/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eviv.tests.utils;

import eviv.VST.VoterSecurityToken;
import eviv.commons.types.CodeCard;
import eviv.commons.types.TrusteeElection;
import eviv.tests.ws.bb_trustees.Votes;
import java.io.Serializable;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

/**
 *
 * @author abrioso
 */
public class TestStorage implements Serializable {

    // private VoterSecurityToken[] vstList = null;
    private static final HashMap<String, VoterSecurityToken> vstMap = new HashMap<String, VoterSecurityToken>();
    private static final HashMap<String, X509Certificate> certsMap = new HashMap<String, X509Certificate>();
    private static final HashMap<String, TrusteeElection> trusteeElectionMap = new HashMap<String, TrusteeElection>();
    private static final HashMap<String, byte[]> electionKPubMap = new HashMap<String, byte[]>();
    private static final HashMap<String, byte[]> electionSigKPubMap = new HashMap<String, byte[]>();
    private static final HashMap<String, byte[]> election2SignMap = new HashMap<String, byte[]>();
    private static final HashMap<String, byte[]> signedElectionMap = new HashMap<String, byte[]>();
    private static final HashMap<String, byte[]> electionChallengeMap = new HashMap<String, byte[]>();
    
    private static final HashMap<String, eviv.tests.ws.bb_er.ElectionInfo> election2RegisterMap = new HashMap<String, eviv.tests.ws.bb_er.ElectionInfo>();

    private static final HashMap<Pair<String, String>, CodeCard> codeCardMap = new HashMap<Pair<String, String>, CodeCard>();

    private static final HashMap<Pair<String, String>, byte[]> votes2SendMap = new HashMap<Pair<String, String>, byte[]>();
    
    private static final HashMap<String, List<Votes>> votes2AggMap = new HashMap<String, List<Votes>>();
    private static final HashMap<String, byte[]> votesAgg2DecAggMap = new HashMap<String, byte[]>();
    private static final HashMap<String, byte[]> votesDecAgg2ResultsMap = new HashMap<String, byte[]>();

    private static final HashMap<String, byte[]> electionResultsMap = new HashMap<String, byte[]>();


    
  //  private static final HashMap<String, byte[]> election2VoteMap = new HashMap<String, byte[]>();
    
  //  private static final HashMap<String, byte[]> votes2SendMap = new HashMap<String, byte[]>();

   // private static final HashMap<String, byte[]> votes = new HashMap<String, byte[]>();
    
    
    
    private static final HashMap<String, EVIVTestResults> resultsMap = new HashMap<String, EVIVTestResults>();

    public TestStorage() {
    }

    public static void printResults() {

        System.out.println();
        System.out.println("------------------");

        System.out.println("EVIV-Tests Results:");

        Set testNames = resultsMap.keySet();

        for (Object test : testNames) {
            System.out.println();
            System.out.println("    " + (String) test + " :");
            EVIVTestResults results = resultsMap.get((String) test);
            System.out.println("        num = " + results.getNum());
            System.out.println("        min = " + results.getMin());
            System.out.println("        avr = " + results.getAvr());
            System.out.println("        max = " + results.getMax());
            System.out.println("        sum = " + results.getSum());
        }

        System.out.println("------------------");
        System.out.println();

    }

        public static void printLatexResults() {

        System.out.println();
        System.out.println("------------------");

        System.out.println("EVIV-Tests Results:");

        Set testNames = resultsMap.keySet();

        for (Object test : testNames) {
            System.out.println();
            System.out.print("    " + (String) test + " :");
            EVIVTestResults results = resultsMap.get((String) test);
            System.out.print("    $" + results.getMin() + "$ &");
            System.out.print("    $" + results.getAvr() + "$ &");
            System.out.print("    $" + results.getMax() + "$ &");
            System.out.print("    $" + results.getSum() + "$ ");
            System.out.print("    $_{(" + results.getNum() + ")}");
            System.out.println();

        }

        System.out.println("------------------");
        System.out.println();

    }

    
    /*
     * VSTs Map
     */
    public HashMap<String, VoterSecurityToken> getVstMap() {
        return vstMap;
    }

    public void addVST(String voterID, VoterSecurityToken vst) {
        vstMap.put(voterID, vst);
    }

    public void remVST(String voterID) {
        vstMap.remove(voterID);
    }

    public void updateVST(String voterID, VoterSecurityToken vst) {
        vstMap.remove(voterID);
        vstMap.put(voterID, vst);
    }

    public void clearVSTMap() {
        vstMap.clear();
    }

    public VoterSecurityToken getVST(String voterID) {
        return vstMap.get(voterID);
    }

    public Set<String> getVSTtKeys() {
        return vstMap.keySet();
    }

    /*
     * Certs Map
     */
    public HashMap<String, X509Certificate> getCertMap() {
        return certsMap;
    }

    public void addCert(String voterID, X509Certificate cert) {
        certsMap.put(voterID, cert);
    }

    public void remCert(String voterID) {
        certsMap.remove(voterID);
    }

    public void updateCert(String voterID, X509Certificate cert) {
        certsMap.remove(voterID);
        certsMap.put(voterID, cert);
    }

    public void clearCertMap() {
        certsMap.clear();
    }

    public X509Certificate getCert(String voterID) {
        return certsMap.get(voterID);
    }

    public Set<String> getCerttKeys() {
        return certsMap.keySet();
    }

    /*
     * TrusteeElections Map
     */
    public HashMap<String, TrusteeElection> getTrusteeElectionMap() {
        return trusteeElectionMap;
    }

    public void addTrusteeElection(String electionID, TrusteeElection trusteeElection) {
        trusteeElectionMap.put(electionID, trusteeElection);
    }

    public void remTrusteeElection(String electionID) {
        trusteeElectionMap.remove(electionID);
    }

    public void updateTrusteeElection(String electionID, TrusteeElection trusteeElection) {
        trusteeElectionMap.remove(electionID);
        trusteeElectionMap.put(electionID, trusteeElection);
    }

    public void clearTrusteeElectionMap() {
        trusteeElectionMap.clear();
    }

    public TrusteeElection getTrusteeElection(String electionID) {
        return trusteeElectionMap.get(electionID);
    }

    public Set<String> getTrusteeElectionKeys() {
        return trusteeElectionMap.keySet();
    }

    /*
     * elections kpub Map
     */
    public HashMap<String, byte[]> getElectionKPubMap() {
        return electionKPubMap;
    }

    public void addElectionKPub(String electionID, byte[] kpub) {
        electionKPubMap.put(electionID, kpub);
    }

    public void remElectionKPub(String electionID) {
        electionKPubMap.remove(electionID);
    }

    public void updateElectionKPub(String electionID, byte[] kpub) {
        electionKPubMap.remove(electionID);
        electionKPubMap.put(electionID, kpub);
    }

    public void clearElectionKPubMap() {
        electionKPubMap.clear();
    }

    public byte[] getElectionKPub(String electionID) {
        return electionKPubMap.get(electionID);
    }

    public Set<String> getElectionKPubKeys() {
        return electionKPubMap.keySet();
    }

    /*
     * elections kpub Map
     */
    public HashMap<String, byte[]> getElectionSigKPubMap() {
        return electionSigKPubMap;
    }

    public void addElectionSigKPub(String electionID, byte[] kpub) {
        electionSigKPubMap.put(electionID, kpub);
    }

    public void remElectionSigKPub(String electionID) {
        electionSigKPubMap.remove(electionID);
    }

    public void updateElectionSigKPub(String electionID, byte[] kpub) {
        electionSigKPubMap.remove(electionID);
        electionSigKPubMap.put(electionID, kpub);
    }

    public void clearElectionSigKPubMap() {
        electionSigKPubMap.clear();
    }

    public byte[] getElectionSigKPub(String electionID) {
        return electionSigKPubMap.get(electionID);
    }

    public Set<String> getElectionSigKPubKeys() {
        return electionSigKPubMap.keySet();
    }

    /*
     * elections kpub Map
     */
    public HashMap<String, byte[]> getElection2SignMap() {
        return election2SignMap;
    }

    public void addElection2SignKPub(String electionID, byte[] election_byte) {
        election2SignMap.put(electionID, election_byte);
    }

    public void remElection2SignKPub(String electionID) {
        election2SignMap.remove(electionID);
    }

    public void updateElection2SignKPub(String electionID, byte[] election_byte) {
        election2SignMap.remove(electionID);
        election2SignMap.put(electionID, election_byte);
    }

    public void clearElection2SignKPubMap() {
        election2SignMap.clear();
    }

    public byte[] getElection2SignKPub(String electionID) {
        return election2SignMap.get(electionID);
    }

    public Set<String> getElection2SignKPubKeys() {
        return election2SignMap.keySet();
    }

    /*
     * elections kpub Map
     */
    public HashMap<String, byte[]> getSignedElectionMap() {
        return signedElectionMap;
    }

    public void addSignedElection(String electionID, byte[] election_byte) {
        signedElectionMap.put(electionID, election_byte);
    }

    public void remSignedElection(String electionID) {
        signedElectionMap.remove(electionID);
    }

    public void updateSignedElection(String electionID, byte[] election_byte) {
        signedElectionMap.remove(electionID);
        signedElectionMap.put(electionID, election_byte);
    }

    public void clearSignedElectionMap() {
        signedElectionMap.clear();
    }

    public byte[] getSignedElection(String electionID) {
        return signedElectionMap.get(electionID);
    }

    public Set<String> getSignedElectionKeys() {
        return signedElectionMap.keySet();
    }

    /*
     * elections kpub Map
     */
    public HashMap<String, byte[]> getElectionChallengeMap() {
        return electionKPubMap;
    }

    public void addElectionChallenge(String electionID, byte[] chal) {
        electionChallengeMap.put(electionID, chal);
    }

    public void remElectionChallenge(String electionID) {
        electionChallengeMap.remove(electionID);
    }

    public void updateElectionChallenge(String electionID, byte[] chal) {
        electionChallengeMap.remove(electionID);
        electionChallengeMap.put(electionID, chal);
    }

    public void clearElectionChallengeMap() {
        electionChallengeMap.clear();
    }

    public byte[] getElectionChallenge(String electionID) {
        return electionChallengeMap.get(electionID);
    }

    public Set<String> getElectionChallengeKeys() {
        return electionChallengeMap.keySet();
    }

    
    
    /*
     * elections Election2Register Map
     */
    public HashMap<String, eviv.tests.ws.bb_er.ElectionInfo> getElection2RegisterMap() {
        return election2RegisterMap;
    }

    public void addElection2Register(String electionID, eviv.tests.ws.bb_er.ElectionInfo einfo) {
        election2RegisterMap.put(electionID, einfo);
    }

    public void remElection2Register(String electionID) {
        election2RegisterMap.remove(electionID);
    }

    public void updateElection2Register(String electionID, eviv.tests.ws.bb_er.ElectionInfo einfo) {
        election2RegisterMap.remove(electionID);
        election2RegisterMap.put(electionID, einfo);
    }

    public void clearElection2RegisterMap() {
        election2RegisterMap.clear();
    }

    public eviv.tests.ws.bb_er.ElectionInfo getElection2Register(String electionID) {
        return election2RegisterMap.get(electionID);
    }

    public Set<String> getElection2RegisterKeys() {
        return election2RegisterMap.keySet();
    }
    
    /*
     * CodeCard Map
     */
    public HashMap<Pair<String, String>, CodeCard> getCodeCardMap() {
        return codeCardMap;
    }

    public void addCodeCard(Pair<String, String> pair, CodeCard cc) {
        codeCardMap.put(pair, cc);
    }

    public void remCodeCard(Pair<String, String> pair) {
        codeCardMap.remove(pair);
    }

    public void updateCodeCard(Pair<String, String> pair, CodeCard cc) {
        codeCardMap.remove(pair);
        codeCardMap.put(pair, cc);
    }

    public void clearCodeCard() {
        codeCardMap.clear();
    }

    public CodeCard getCodeCard(Pair<String, String> pair) {
        return codeCardMap.get(pair);
    }

    public Set<Pair<String, String>> getCodeCardKeys() {
        return codeCardMap.keySet();
    }
    
    
    /*
     * VoteMap
     */
    public HashMap<Pair<String, String>, byte[]> getVote2SignMap() {
        return votes2SendMap;
    }

    public void addVote2Sign(Pair<String, String> voterElectionPair, byte[] vote) {
        votes2SendMap.put(voterElectionPair, vote);
    }

    public void remVote2Sign(Pair<String, String> voterElectionPair) {
        votes2SendMap.remove(voterElectionPair);
    }

    public void updateVote2Sign(Pair<String, String> voterElectionPair, byte[] vote) {
        votes2SendMap.remove(voterElectionPair);
        votes2SendMap.put(voterElectionPair, vote);
    }

    public void clearVote2SignMap() {
        votes2SendMap.clear();
    }

    public byte[] getVote2Sign(Pair<String, String> voterElectionPair) {
        return votes2SendMap.get(voterElectionPair);
    }

    public Set<Pair<String, String>> getVote2SignKeys() {
        return votes2SendMap.keySet();
    }
    
    
    /*
     * VoteMap
     */
    public HashMap<String, List<Votes>> getVotes2Aggregate() {
        return votes2AggMap;
    }

    public void addVotes2Aggregate(String electionID, List<Votes> votes) {
        votes2AggMap.put(electionID, votes);
    }

    public void remVotes2Aggregate(String electionID) {
        votes2AggMap.remove(electionID);
    }

    public void updateVotes2Aggregate(String electionID, List<Votes> votes) {
        votes2AggMap.remove(electionID);
        votes2AggMap.put(electionID, votes);
    }

    public void clearVotes2AggregateMap() {
        votes2AggMap.clear();
    }

    public List<Votes> getVotes2Aggregate(String electionID) {
        return votes2AggMap.get(electionID);
    }

    public Set<String> getVotes2AggregateKeys() {
        return votes2AggMap.keySet();
    }
    
    /*
     * VoteMap
     */
    public HashMap<String, byte[]> getAggregatedVoteMap() {
        return votesAgg2DecAggMap;
    }

    public void addAggregatedVote(String electionID, byte[] vote) {
        votesAgg2DecAggMap.put(electionID, vote);
    }

    public void remAggregatedVote(String electionID) {
        votesAgg2DecAggMap.remove(electionID);
    }

    public void updateAggregatedVote(String electionID, byte[] vote) {
        votesAgg2DecAggMap.remove(electionID);
        votesAgg2DecAggMap.put(electionID, vote);
    }

    public void clearAggregatedVoteMap() {
        votesAgg2DecAggMap.clear();
    }

    public byte[] getAggregatedVote(String electionID) {
        return votesAgg2DecAggMap.get(electionID);
    }

    public Set<String> getAggregatedVoteKeys() {
        return votesAgg2DecAggMap.keySet();
    }
    
    /*
     * VoteMap
     */
    public HashMap<String, byte[]> getDecryptedAggregatedVoteMap() {
        return votesDecAgg2ResultsMap;
    }

    public void addDecryptedAggregatedVote(String electionID, byte[] vote) {
        votesDecAgg2ResultsMap.put(electionID, vote);
    }

    public void remDecryptedAggregatedVote(String electionID) {
        votesDecAgg2ResultsMap.remove(electionID);
    }

    public void updateDecryptedAggregatedVote(String electionID, byte[] vote) {
        votesDecAgg2ResultsMap.remove(electionID);
        votesDecAgg2ResultsMap.put(electionID, vote);
    }

    public void clearDecryptedAggregatedVoteMap() {
        votesDecAgg2ResultsMap.clear();
    }

    public byte[] getDecryptedAggregatedVote(String electionID) {
        return votesDecAgg2ResultsMap.get(electionID);
    }

    public Set<String> getDecryptedAggregatedVoteKeys() {
        return votesDecAgg2ResultsMap.keySet();
    }
    
    
    
    /*
     * VoteMap
     */
    public HashMap<String, byte[]> getElectionResultsMap() {
        return electionResultsMap;
    }

    public void addElectionResults(String electionID, byte[] result) {
        electionResultsMap.put(electionID, result);
    }

    public void remElectionResults(String electionID) {
        electionResultsMap.remove(electionID);
    }

    public void updateElectionResults(String electionID, byte[] result) {
        electionResultsMap.remove(electionID);
        electionResultsMap.put(electionID, result);
    }

    public void clearElectionResultsMap() {
        electionResultsMap.clear();
    }

    public byte[] getElectionResults(String electionID) {
        return electionResultsMap.get(electionID);
    }

    public Set<String> getElectionResultsKeys() {
        return electionResultsMap.keySet();
    }
    
    
    
    /*
     * Results Map
     */
    public HashMap<String, EVIVTestResults> getResultsMap() {
        return resultsMap;
    }

    public void addResult(String resultName, EVIVTestResults result) {
        resultsMap.put(resultName, result);
    }

    public void remResult(String resultName) {
        resultsMap.remove(resultName);
    }

    public void clearResultsMap() {
        resultsMap.clear();
    }

    public EVIVTestResults getResult(String resultName) {
        return resultsMap.get(resultName);
    }

    public Set<String> getResultKeys() {
        return resultsMap.keySet();
    }
}

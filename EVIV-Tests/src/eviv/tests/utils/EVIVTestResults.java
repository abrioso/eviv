/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eviv.tests.utils;

import java.io.Serializable;

/**
 *
 * @author abrioso
 */
public class EVIVTestResults implements Serializable{
    
    private int num = 0;
    private double min = 10000000;
    private double avr = 0;
    private double max = 0;
    private double sum = 0;

    public EVIVTestResults() {
    }

    public EVIVTestResults(int num, double min, double avr, double max, double sum) {
        this.num = num;
        this.min = min;
        this.avr = avr;
        this.max = max;
        this.sum = sum;
    }

    
    
    /**
     * Get the value of num
     *
     * @return the value of num
     */
    public int getNum() {
        return num;
    }

    /**
     * Set the value of num
     *
     * @param num new value of num
     */
    public void setNum(int num) {
        this.num = num;
    }
    

    /**
     * Get the value of min
     *
     * @return the value of min
     */
    public double getMin() {
        return min;
    }

    /**
     * Set the value of min
     *
     * @param min new value of min
     */
    public void setMin(double min) {
        this.min = min;
    }


    /**
     * Get the value of avr
     *
     * @return the value of avr
     */
    public double getAvr() {
        return avr;
    }

    /**
     * Set the value of avr
     *
     * @param avr new value of avr
     */
    public void setAvr(double avr) {
        this.avr = avr;
    }

    /**
     * Get the value of max
     *
     * @return the value of max
     */
    public double getMax() {
        return max;
    }

    /**
     * Set the value of max
     *
     * @param max new value of max
     */
    public void setMax(double max) {
        this.max = max;
    }

    /**
     * Get the value of sum
     *
     * @return the value of sum
     */
    public double getSum() {
        return sum;
    }

    /**
     * Set the value of sum
     *
     * @param sum new value of sum
     */
    public void setSum(double sum) {
        this.sum = sum;
    }

    
}

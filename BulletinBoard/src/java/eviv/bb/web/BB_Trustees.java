/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eviv.bb.web;


import eviv.bb.beans.BBUtil;
import eviv.bb.entities.Elections;
import eviv.bb.entities.Votes;
import eviv.commons.types.ElectionInfo;
import eviv.commons.types.ElectionPartialInfo;
import eviv.commons.util.Converters;
import java.math.BigInteger;
import java.security.SignedObject;
import java.util.ArrayList;
import java.util.Collection;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.xml.bind.annotation.XmlSeeAlso;
import org.apache.commons.lang3.ArrayUtils;

/**
 *
 * @author abrioso
 */
@WebService(serviceName = "BB_Trustees")
@XmlSeeAlso({ElectionPartialInfo.class,ElectionInfo.class})
@Stateless()
public class BB_Trustees {

    /** This is a sample web service operation */
    @WebMethod(operationName = "hello")
    public String hello(@WebParam(name = "name") String txt) {
        return "Hello " + txt + " !";
    }
    
     /**
     * Web service operation
     * @return 
     */
    
    @WebMethod(operationName = "getElectionList")
    
    public Collection<ElectionPartialInfo> getElectionList() {
        //TODO write your implementation code here:
        
        //Gets an Entity Manager & Transaction
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("BulletinBoardPU");
        EntityManager em = emf.createEntityManager();
        
  //      Collection<Elections> electionList = (Collection<Elections>) em.find(Elections.class, null);
        Collection<Elections> electionList = em.createNamedQuery("Elections.findAll").getResultList();
                //(Collection<Elections>) em.find(Elections.class, null);
        
        Collection<ElectionPartialInfo> electionPInfoList = new ArrayList<ElectionPartialInfo>();
        
        for(Elections e : electionList)
            electionPInfoList.add(new ElectionPartialInfo(e.getElectionId(), e.getElectionDesc()));
        
        
        //Close the Entity Manager & Factory
        em.close();
        emf.close();
        
        return electionPInfoList;
    }

      /**
     * Web service operation
     * @param electionID
     * @return  
     */
    @WebMethod(operationName = "getElectionInfo")
    public ElectionInfo getElectionInfo(@WebParam(name = "electionID") String electionID) {
        //TODO write your implementation code here:
        
        //Gets an Entity Manager & Transaction
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("BulletinBoardPU");
        EntityManager em = emf.createEntityManager();
        
        Elections election = (Elections) em.find(Elections.class, electionID);
   
        
        
        //Close the Entity Manager & Factory
        em.close();
        emf.close();
        
        ElectionInfo info = null;
             
        if(election != null) {
            info = BBUtil.ConvertElections(election);
        }
        
        return info;
        
        
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "setElectionKey")
    public java.lang.Byte[] setElectionKey(@WebParam(name = "electionID") java.lang.String electionID, @WebParam(name = "electionKPub") java.lang.Byte[] electionKPub, @WebParam(name = "electionSignedKPub") java.lang.Byte[] electionSignedKPub) {
         
        //Gets an Entity Manager & Transaction
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("BulletinBoardPU");
        EntityManager em = emf.createEntityManager();
        
        Elections election = (Elections) em.find(Elections.class, electionID);
        
        election.setElectionPubKey(ArrayUtils.toPrimitive(electionKPub));
        election.setElectionSignedPubKey(ArrayUtils.toPrimitive(electionSignedKPub));
        
        
        //Persist the Election

        EntityTransaction trans = em.getTransaction();       
        trans.begin();
        em.persist(election);
        trans.commit();
        
        //Close the Entity Manager & Factory
        em.close();
        emf.close();
       
        return electionSignedKPub;
        
    }
    
     /**
     * Web service operation
     */
    @WebMethod(operationName = "setElectionChallenge")
    public byte[] setElectionChallenge(@WebParam(name = "electionID") java.lang.String electionID, @WebParam(name = "electionSignedChallenge") byte[] electionSignedChallenge) {
         
        //Gets an Entity Manager & Transaction
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("BulletinBoardPU");
        EntityManager em = emf.createEntityManager();
        
        Elections election = (Elections) em.find(Elections.class, electionID);
        
        election.setElectionSignedChallenge(electionSignedChallenge);
        
        // Election Phase 2
        election.setElectionPhase(2);
        
        SignedObject so = (SignedObject) Converters.byteToObject(electionSignedChallenge);
        
        BigInteger chal = (BigInteger) Converters.signedObjectToObject(so);
        
        byte[] chalBytes = Converters.objectToByte(chal);
        
        election.setElectionChallenge(chalBytes);
        
        
        //Persist the Election

        EntityTransaction trans = em.getTransaction();       
        trans.begin();
        em.persist(election);
        trans.commit();
        
        //Close the Entity Manager & Factory
        em.close();
        emf.close();
       
        return chalBytes;
        
    }
    
    
     /**
     * Web service operation
     */
    @WebMethod(operationName = "setElectionVerificationData")
    public java.lang.Byte[] setElectionVerificationData(@WebParam(name = "electionID") java.lang.String electionID, @WebParam(name = "verificationData") java.lang.Byte[] verificationData) {
         
        //Gets an Entity Manager & Transaction
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("BulletinBoardPU");
        EntityManager em = emf.createEntityManager();
        
        Elections election = (Elections) em.find(Elections.class, electionID);
        
        election.setElectionAggregationVerification(ArrayUtils.toPrimitive(verificationData));
   //     election.setElectionChallenge(ArrayUtils.toPrimitive(electionChallenge));
   //     election.setElectionSignedChallenge(ArrayUtils.toPrimitive(electionSignedChallenge));
        
        
        //Persist the Election

        EntityTransaction trans = em.getTransaction();       
        trans.begin();
        em.persist(election);
        trans.commit();
        
        //Close the Entity Manager & Factory
        em.close();
        emf.close();
       
        return verificationData;
        
    }
    

    /**
     * Web service operation
     */
    @WebMethod(operationName = "getVotes")
    public java.util.Collection<eviv.bb.entities.Votes> getVotes(@WebParam(name = "electionID") String electionID) {
        
        //Gets an Entity Manager & Transaction
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("BulletinBoardPU");
        EntityManager em = emf.createEntityManager();
        
        Collection<Votes> votes = em.createNamedQuery("Votes.findByElectionselectionid").setParameter("electionselectionid", electionID).getResultList();
        
       // Elections election = (Elections) em.find(Elections.class, electionID);
   
        
        
        //Close the Entity Manager & Factory
        em.close();
        emf.close();
        
             
        //ElectionInfo info = BBUtil.ConvertElections(election);
        
        return votes;
        
        
        
    }

    
    
    /**
     * Web service operation
     */
    @WebMethod(operationName = "setAggregatedVotes")
    public Boolean setAggregatedVotes(@WebParam(name = "electionID") String electionID, @WebParam(name = "electionAggregatedVotes") byte[] electionAggregatedVotes) {
        
        //Gets an Entity Manager & Transaction
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("BulletinBoardPU");
        EntityManager em = emf.createEntityManager();
         
        Elections election = em.find(Elections.class, electionID);

        election.setElectionAgregatedVotes(electionAggregatedVotes);
        
        // Election Phase 4
        election.setElectionPhase(4);
        
        //Persist the Election
        EntityTransaction trans = em.getTransaction();
        trans.begin();
        em.merge(election);
        trans.commit();

        //Close the Entity Manager & Factory
        em.close();
        emf.close();
        
        return true;
        
    }
    
    
    /**
     * Web service operation
     */
    @WebMethod(operationName = "getAggregatedVotes")
    public byte[] getAggregatedVotes(@WebParam(name = "electionID") String electionID) {
        
        byte[] aggVotesBytes = null;
        
        //Gets an Entity Manager & Transaction
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("BulletinBoardPU");
        EntityManager em = emf.createEntityManager();
        
        Elections election = em.find(Elections.class, electionID);

        //Close the Entity Manager & Factory
        em.close();
        emf.close();
        

        aggVotesBytes = election.getElectionAgregatedVotes();
        
        //ElectionInfo info = BBUtil.ConvertElections(election);
        
        return aggVotesBytes;
        
    }
    
    
    
       /**
     * Web service operation
     */
    @WebMethod(operationName = "setDecryptedAggregatedVotes")
    public Boolean setDecryptedAggregatedVotes(@WebParam(name = "electionID") String electionID, @WebParam(name = "electionDecryptedAggregatedVotes") byte[] electionDecryptedAggregatedVotes) {
        
        //Gets an Entity Manager & Transaction
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("BulletinBoardPU");
        EntityManager em = emf.createEntityManager();
        
        Elections election = em.find(Elections.class, electionID);

        election.setElectionDecryptedAggregatedVotes(electionDecryptedAggregatedVotes);
        
        
        //Persist the Election
        EntityTransaction trans = em.getTransaction();
        trans.begin();
        em.merge(election);
        trans.commit();

        //Close the Entity Manager & Factory
        em.close();
        emf.close();
        
        return true;
        
    }
    
    
    /**
     * Web service operation
     */
    @WebMethod(operationName = "getDecryptedAggregatedVotes")
    public byte[] getDecryptedAggregatedVotes(@WebParam(name = "electionID") String electionID) {
        
        byte[] decryptedAggVotesBytes = null;
        
        //Gets an Entity Manager & Transaction
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("BulletinBoardPU");
        EntityManager em = emf.createEntityManager();
        
        Elections election = em.find(Elections.class, electionID);

        //Close the Entity Manager & Factory
        em.close();
        emf.close();
        

        decryptedAggVotesBytes = election.getElectionDecryptedAggregatedVotes();
        
        //ElectionInfo info = BBUtil.ConvertElections(election);
        
        return decryptedAggVotesBytes;
        
    }
    
   
    
    /**
     * Web service operation
     */
    @WebMethod(operationName = "setResults")
    public Boolean setResults(@WebParam(name = "electionID") String electionID, @WebParam(name = "electionResults") byte[] electionResults) {
        
         
        //Gets an Entity Manager & Transaction
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("BulletinBoardPU");
        EntityManager em = emf.createEntityManager();
        
        Elections election = em.find(Elections.class, electionID);

        election.setElectionResults(electionResults);
        
        
        //Persist the Election
        EntityTransaction trans = em.getTransaction();
        trans.begin();
        em.merge(election);
        trans.commit();

        //Close the Entity Manager & Factory
        em.close();
        emf.close();
        
        return true;
        
        
    }
    
    
    
    
    
}

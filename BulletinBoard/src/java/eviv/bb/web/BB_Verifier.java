/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eviv.bb.web;

import eviv.bb.beans.BBUtil;
import eviv.bb.entities.Elections;
import eviv.bb.entities.Votes;
import eviv.commons.types.ElectionInfo;
import eviv.commons.types.ElectionPartialInfo;
import java.util.ArrayList;
import java.util.Collection;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author abrioso
 */
@WebService(serviceName = "BB_Verifier")
@Stateless()
public class BB_Verifier {

    /**
     * This is a sample web service operation
     */
    @WebMethod(operationName = "hello")
    public String hello(@WebParam(name = "name") String txt) {
        return "Hello " + txt + " !";
    }
    
    
    /**
     * Web service operation
     * @return 
     */
    
    @WebMethod(operationName = "getElectionList")
    
    public Collection<ElectionPartialInfo> getElectionList() {
        //TODO write your implementation code here:
        
        //Gets an Entity Manager & Transaction
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("BulletinBoardPU");
        EntityManager em = emf.createEntityManager();
        
  //      Collection<Elections> electionList = (Collection<Elections>) em.find(Elections.class, null);
        Collection<Elections> electionList = em.createNamedQuery("Elections.findAll").getResultList();
                //(Collection<Elections>) em.find(Elections.class, null);
        
        Collection<ElectionPartialInfo> electionPInfoList = new ArrayList<ElectionPartialInfo>();
        
        for(Elections e : electionList)
            electionPInfoList.add(new ElectionPartialInfo(e.getElectionId(), e.getElectionDesc()));
        
        
        //Close the Entity Manager & Factory
        em.close();
        emf.close();
        
        return electionPInfoList;
    }

      /**
     * Web service operation
     * @param electionID
     * @return  
     */
    @WebMethod(operationName = "getElectionInfo")
    public ElectionInfo getElectionInfo(@WebParam(name = "electionID") String electionID) {
        //TODO write your implementation code here:
        
        //Gets an Entity Manager & Transaction
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("BulletinBoardPU");
        EntityManager em = emf.createEntityManager();
        
        Elections election = (Elections) em.find(Elections.class, electionID);
   
        
        
        //Close the Entity Manager & Factory
        em.close();
        emf.close();
        
        ElectionInfo info = null;
             
        if(election != null) {
            info = BBUtil.ConvertElections(election);
        }
        
        return info;
        
        
    }

     /**
     * Web service operation
     */
    @WebMethod(operationName = "getVotes")
    public java.util.Collection<eviv.bb.entities.Votes> getVotes(@WebParam(name = "electionID") String electionID) {
        
        //Gets an Entity Manager & Transaction
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("BulletinBoardPU");
        EntityManager em = emf.createEntityManager();
        
        Collection<Votes> votes = em.createNamedQuery("Votes.findByElectionselectionid").setParameter("electionselectionid", electionID).getResultList();
        
       // Elections election = (Elections) em.find(Elections.class, electionID);
   
        
        
        //Close the Entity Manager & Factory
        em.close();
        emf.close();
        
             
        //ElectionInfo info = BBUtil.ConvertElections(election);
        
        return votes;
        
        
        
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "getAggregatedVotes")
    public byte[] getAggregatedVotes(@WebParam(name = "electionID") String electionID) {
        
        byte[] aggVotesBytes = null;
        
        //Gets an Entity Manager & Transaction
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("BulletinBoardPU");
        EntityManager em = emf.createEntityManager();
        
        Elections election = em.find(Elections.class, electionID);

        //Close the Entity Manager & Factory
        em.close();
        emf.close();
        

        aggVotesBytes = election.getElectionAgregatedVotes();
        
        //ElectionInfo info = BBUtil.ConvertElections(election);
        
        return aggVotesBytes;
        
    }
    
      /**
     * Web service operation
     */
    @WebMethod(operationName = "getDecryptedAggregatedVotes")
    public byte[] getDecryptedAggregatedVotes(@WebParam(name = "electionID") String electionID) {
        
        byte[] decryptedAggVotesBytes = null;
        
        //Gets an Entity Manager & Transaction
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("BulletinBoardPU");
        EntityManager em = emf.createEntityManager();
        
        Elections election = em.find(Elections.class, electionID);

        //Close the Entity Manager & Factory
        em.close();
        emf.close();
        

        decryptedAggVotesBytes = election.getElectionDecryptedAggregatedVotes();
        
        //ElectionInfo info = BBUtil.ConvertElections(election);
        
        return decryptedAggVotesBytes;
        
    }
    
   
        /**
     * Web service operation
     */
    @WebMethod(operationName = "getResults")
    public byte[] getResults(@WebParam(name = "electionID") String electionID) {
        
        byte[] results = null;
        
        //Gets an Entity Manager & Transaction
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("BulletinBoardPU");
        EntityManager em = emf.createEntityManager();
        
        Elections election = em.find(Elections.class, electionID);

        //Close the Entity Manager & Factory
        em.close();
        emf.close();
        

        results = election.getElectionResults();
        
        //ElectionInfo info = BBUtil.ConvertElections(election);
        
        return results;
        
    }
    
  
    
}

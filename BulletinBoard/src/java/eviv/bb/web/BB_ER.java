/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eviv.bb.web;

import eviv.bb.beans.BBUtil;
import eviv.bb.entities.Elections;
import eviv.bb.entities.Voters;
import eviv.commons.types.ElectionInfo;
import eviv.commons.types.ElectionPartialInfo;
import java.util.ArrayList;
import java.util.Collection;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.xml.bind.annotation.XmlSeeAlso;

/**
 *
 * @author abrioso
 */
@WebService(serviceName = "BB_ER")
@XmlSeeAlso({ElectionPartialInfo.class,ElectionInfo.class})
@Stateless()
public class BB_ER {

    /** This is a sample web service operation */
    @WebMethod(operationName = "hello")
    public String hello(@WebParam(name = "name") String txt) {
        return "Hello " + txt + " !";
    }
    
        /**
     * Web service operation
     * @return 
     */
     @WebMethod(operationName = "getElectionList")
    public Collection<ElectionPartialInfo> getElectionList() {
        //TODO write your implementation code here:
        
        //Gets an Entity Manager & Transaction
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("BulletinBoardPU");
        EntityManager em = emf.createEntityManager();
        
  //      Collection<Elections> electionList = (Collection<Elections>) em.find(Elections.class, null);
        Collection<Elections> electionList = em.createNamedQuery("Elections.findAll").getResultList();
                //(Collection<Elections>) em.find(Elections.class, null);
        
 /*       Collection<String> electionNamesList = new ArrayList<String>();
        
        for(Elections e : electionList)
            electionNamesList.add(e.getElectionId());
   */     
       Collection<ElectionPartialInfo> electionPInfoList = new ArrayList<ElectionPartialInfo>();
        
        for(Elections e : electionList)
            electionPInfoList.add(new ElectionPartialInfo(e.getElectionId(), e.getElectionDesc()));
         
        
        
        
        //Close the Entity Manager & Factory
        em.close();
        emf.close();
        
        return electionPInfoList;
    }

      /**
     * Web service operation
     * @param electionID
     * @return  
     */
    @WebMethod(operationName = "getElectionInfo")
    public ElectionInfo getElectionInfo(@WebParam(name = "electionID") String electionID) {
        //TODO write your implementation code here:
        
        //Gets an Entity Manager & Transaction
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("BulletinBoardPU");
        EntityManager em = emf.createEntityManager();
        
        Elections election = (Elections) em.find(Elections.class, electionID);
   
        
        
        //Close the Entity Manager & Factory
        em.close();
        emf.close();
        
             
        ElectionInfo info = BBUtil.ConvertElections(election);
        
        return info;
        
        
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "registerVoter")
    public Boolean registerVoter(@WebParam(name = "electionID") String electionID, @WebParam(name = "voterID") String voterID) {
        
        Boolean voterRegistered = false;
        
        
        //Gets an Entity Manager & Transaction
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("BulletinBoardPU");
        EntityManager em = emf.createEntityManager();
        
        
        
        
        Elections election = (Elections) em.find(Elections.class, electionID);
        Voters voter = (Voters) em.find(Voters.class, voterID);
        
        voterRegistered = election.getVotersList().add(voter);
        
        //Persist the Election

        EntityTransaction trans = em.getTransaction();       
        trans.begin();
        em.persist(election);
        em.persist(voter);
        trans.commit();
        
        //Close the Entity Manager & Factory
        em.close();
        emf.close();
       
        return voterRegistered;
        
        
    }

    
    
    
}

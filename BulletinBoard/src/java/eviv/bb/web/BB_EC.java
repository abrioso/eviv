/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eviv.bb.web;

import eviv.bb.beans.BBUtil;
import eviv.bb.entities.Candidates;
import eviv.bb.entities.Elections;
import eviv.bb.entities.Voters;
import eviv.commons.types.ElectionInfo;
import eviv.commons.types.ElectionPartialInfo;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.xml.bind.annotation.XmlSeeAlso;

/**
 *
 * @author abrioso
 */
@WebService(serviceName = "BB_EC")
@XmlSeeAlso({ElectionPartialInfo.class,ElectionInfo.class})
@Stateless()
public class BB_EC {

    /** This is a sample web service operation */
    @WebMethod(operationName = "hello")
    public String hello(@WebParam(name = "name") String txt) {
        return "Hello " + txt + " !";
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "addVoter")
    public String addVoter(@WebParam(name = "voterID")
    String voterID,  @WebParam(name = "voterCertificate")
    String voterCertificate) {
        //TODO write your implementation code here:
        
        //Create a new Voter
        Voters voter = new Voters(voterID);
        voter.setVoterCertificate(voterCertificate);
        
        
        //Gets an Entity Manager & Transaction
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("BulletinBoardPU");
        EntityManager em = emf.createEntityManager();
        
        //Persist the Voter
        EntityTransaction trans = em.getTransaction();
        trans.begin();
        em.persist(voter);
        trans.commit();
        
        //Close the Entity Manager & Factory
        em.close();
        emf.close();
        
        
        return voterCertificate;
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "addElection")
    public Boolean addElection(@WebParam(name = "electionID") String electionID, @WebParam(name = "electionDesc") String electionDesc) {
        //TODO write your implementation code here:
        
        Elections election = new Elections(electionID);
        election.setElectionDesc(electionDesc);
        
        // Election Phase 1
        election.setElectionPhase(1);
        
         //Gets an Entity Manager & Transaction
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("BulletinBoardPU");
        EntityManager em = emf.createEntityManager();
        
        //Persist the Voters
        EntityTransaction trans = em.getTransaction();
        trans.begin();
        em.persist(election);
        trans.commit();
        
        //Close the Entity Manager & Factory
        em.close();
        emf.close();
        
        
        return true;
    }

    
    
    /**
     * Web service operation
     */
    @WebMethod(operationName = "addCandidate")
    public Boolean addCandidate(@WebParam(name = "electionID") String electionID, @WebParam(name = "candidateID") int candidateID, @WebParam(name = "candidateInfo") String candidateInfo) {
        //TODO write your implementation code here:
        
        
   //    Candidates candidate = new Candidates(candidateID, electionID);
        Candidates candidate = new Candidates();
        
        
        
         //Gets an Entity Manager & Transaction
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("BulletinBoardPU");
        EntityManager em = emf.createEntityManager();
        
        
        
        
        Elections election = (Elections) em.find(Elections.class, electionID);
        
  //      candidate.setCandidatesPK(new CandidatesPK(candidateID, election.getElectionId()));
        candidate.setElections(election);
  //      election.getCandidatesCollection().add(candidate);
        
   
        //Persist the Voters
        EntityTransaction trans = em.getTransaction();
        trans.begin();
   //     em.persist(election);
        em.persist(candidate);
   
        trans.commit();
        
        //Close the Entity Manager & Factory
        em.close();
        emf.close();
        
        
        
        return true;
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "getElectionList")
    public Collection<ElectionPartialInfo> getElectionList() {
        //TODO write your implementation code here:
        
        //Gets an Entity Manager & Transaction
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("BulletinBoardPU");
        EntityManager em = emf.createEntityManager();
        
  //      Collection<Elections> electionList = (Collection<Elections>) em.find(Elections.class, null);
        Collection<Elections> electionList = em.createNamedQuery("Elections.findAll").getResultList();
                //(Collection<Elections>) em.find(Elections.class, null);
        
 /*       Collection<String> electionNamesList = new ArrayList<String>();
        
        for(Elections e : electionList)
            electionNamesList.add(e.getElectionId());
   */     
       Collection<ElectionPartialInfo> electionPInfoList = new ArrayList<ElectionPartialInfo>();
        
        for(Elections e : electionList)
            electionPInfoList.add(new ElectionPartialInfo(e.getElectionId(), e.getElectionDesc()));
         
        
        
        
        //Close the Entity Manager & Factory
        em.close();
        emf.close();
        
        return electionPInfoList;
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "getCandidatesList")
    public Collection getCandidatesList(@WebParam(name = "electionID") String electionID) {
        //TODO write your implementation code here:
        
        
        //Gets an Entity Manager & Transaction
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("BulletinBoardPU");
        EntityManager em = emf.createEntityManager();
        
        Elections election = (Elections) em.find(Elections.class, electionID);
        
        Collection<Candidates> candidateList = election.getCandidatesList();
        
        //Close the Entity Manager & Factory
        em.close();
        emf.close();
        
        return candidateList;
        
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "getElectionInfo")
    public ElectionInfo getElectionInfo(@WebParam(name = "electionID") String electionID) {
        //TODO write your implementation code here:
        
        //Gets an Entity Manager & Transaction
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("BulletinBoardPU");
        EntityManager em = emf.createEntityManager();
        
        Elections election = (Elections) em.find(Elections.class, electionID);
   
        
        
        //Close the Entity Manager & Factory
        em.close();
        emf.close();
        
             
        ElectionInfo info = BBUtil.ConvertElections(election);
        
        return info;
        
        
    }
    
    @WebMethod(operationName = "getElection2Sign")
    public byte[] getElection2Sign(@WebParam(name = "electionID") String electionID) {
        //TODO write your implementation code here:
        
        //Gets an Entity Manager & Transaction
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("BulletinBoardPU");
        EntityManager em = emf.createEntityManager();
        
        Elections election = (Elections) em.find(Elections.class, electionID);
           
        ObjectOutputStream outStream = null;
        ByteArrayOutputStream outByteStream = null;
        outByteStream = new ByteArrayOutputStream();

        try {
            outStream = new ObjectOutputStream(outByteStream);
            outStream.writeObject(election);
            outStream.flush();
        } catch (IOException ex) {
            Logger.getLogger(BB_EC.class.getName()).log(Level.SEVERE, null, ex);
        }

        byte[] electionBytes = outByteStream.toByteArray();
        
        
        //Close the Entity Manager & Factory
        em.close();
        emf.close();
        
        return electionBytes;
        
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "getVotersList")
    public Collection getVotersList() {
        //TODO write your implementation code here:
        
         //Gets an Entity Manager & Transaction
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("BulletinBoardPU");
        EntityManager em = emf.createEntityManager();
        
        Collection<Voters> voterList = em.createNamedQuery("Voters.findAll").getResultList();;
        
        //Close the Entity Manager & Factory
        em.close();
        emf.close();
        
        return voterList;
        
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "getVoterInfo")
    public Voters getVoterInfo(@WebParam(name = "voterID") String voterID) {
        //TODO write your implementation code here:
        
        //Gets an Entity Manager & Transaction
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("BulletinBoardPU");
        EntityManager em = emf.createEntityManager();
        
        Voters voter = (Voters) em.find(Voters.class, voterID);
        
      
        
        //Close the Entity Manager & Factory
        em.close();
        emf.close();
        
        return voter;
        
        
        
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "updateVoter")
    public Boolean updateVoter(@WebParam(name = "voterID") String voterID, @WebParam(name = "voterCertificate") String voterCertificate) {
        //TODO write your implementation code here:
         
        //Gets an Entity Manager & Transaction
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("BulletinBoardPU");
        EntityManager em = emf.createEntityManager();
        
        Voters voter = (Voters) em.find(Voters.class, voterID);
        
        voter.setVoterCertificate(voterCertificate);
      
        //Persist the Voters
        EntityTransaction trans = em.getTransaction();
        trans.begin();
        em.persist(voter);
        trans.commit();
        
        //Close the Entity Manager & Factory
        em.close();
        emf.close();
       
        return true;
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "updateElection")
    public Boolean updateElection(@WebParam(name = "electionID") String electionID, @WebParam(name = "electionInfo") ElectionInfo electionInfo) {
        //TODO write your implementation code here:
        
         
        //Gets an Entity Manager & Transaction
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("BulletinBoardPU");
        EntityManager em = emf.createEntityManager();
        
        Elections election = (Elections) em.find(Elections.class, electionID);
        
     //   Collection<Date> electionDates = 
        
        
        
        
        
        election.setElectionDesc(electionInfo.getElectionDesc());
    
     
        election.setVoterEnrollInitDate(electionInfo.getElectionDates(ElectionInfo.ENROLL_DATE));
        election.setVoterRegisterInitDate(electionInfo.getElectionDates(ElectionInfo.REGISTER_DATE));
        election.setVoteCastInitDate(electionInfo.getElectionDates(ElectionInfo.CAST_DATE));
        election.setVoteCountInitDate(electionInfo.getElectionDates(ElectionInfo.COUNT_DATE));
        
        // Updates the candidates
  //      election.getCandidatesCollection().clear();
        
        EntityTransaction trans = em.getTransaction();
        trans.begin();
        
        
        for(Candidates c : election.getCandidatesList()) {
            em.remove(c);
     //       em.persist(c);
        }
        
        trans.commit();
        
        election.getCandidatesList().clear();
        
        //int i = 1;
   //     for(String candID : electionInfo.getElectionCandidates()) {
   //         election.getCandidatesCollection().add(new Candidates);
   //     }
        
        
        for(int i = 1; i <= electionInfo.getElectionCandidates().size(); i++ ) {
          Candidates cand = new Candidates(i, electionID);
          cand.setCandidateInfo(electionInfo.getElectionCandidates().get(i-1));
          election.getCandidatesList().add(cand);  
        }
        
        
        election.setCandidatesCount(election.getCandidatesList().size());
        
        //Persist the Election
  //      EntityTransaction trans = em.getTransaction();
        trans = em.getTransaction();
        trans.begin();
        em.persist(election);
        trans.commit();
        
        
        
        //TODO: candidates!!
      //  election.getCandidatesCollection().clear();
        
      //   for(String cand : electionInfo.getElectionCandidates())
      //          election.getCandidatesCollection().add();
        
        
     
      
        //Persist the Election
   /*     EntityTransaction trans = em.getTransaction();
        trans.begin();
        em.persist(election);
        trans.commit();
     */   
        //Close the Entity Manager & Factory
        em.close();
        emf.close();
       
        return true;
        
    }

    
    @WebMethod(operationName = "signElection")
    public Boolean signElection(@WebParam(name = "electionID") String electionID, @WebParam(name = "signedElection") byte[] signedElection) {
         
        //Gets an Entity Manager & Transaction
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("BulletinBoardPU");
        EntityManager em = emf.createEntityManager();
        
        Elections election = (Elections) em.find(Elections.class, electionID);
        
   //     election.setElectionSignedInfo(ArrayUtils.toPrimitive(signedElection)); 
       election.setElectionSignedInfo(signedElection);
        
        //Persist the Election
        EntityTransaction trans = em.getTransaction();
        trans.begin();
        em.persist(election);
        trans.commit();
        
        //Close the Entity Manager & Factory
        em.close();
        emf.close();
       
        return true;
        
    }
    
    
    /**
     * Web service operation
     */
    @WebMethod(operationName = "remVoter")
    public Boolean remVoter(@WebParam(name = "voterID") String voterID) {
        //TODO write your implementation code here:
        
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("BulletinBoardPU");
        EntityManager em = emf.createEntityManager();
        
        
        //Find Voter
        Voters voter = (Voters) em.find(Voters.class, voterID);
        
        
        //Remove Voter (is this enough?)
 //       em.remove(voter);
        
        EntityTransaction trans = em.getTransaction();
        trans.begin();
        em.remove(voter);
  //      em.persist(voter);
        trans.commit();
        
        
        //Close the Entity Manager & Factory
        em.close();
        emf.close();
        
        
        return true;
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "remElection")
    public Boolean remElection(@WebParam(name = "electionID") String electionID) {
        //TODO write your implementation code here:
        
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("BulletinBoardPU");
        EntityManager em = emf.createEntityManager();
        
        
        //Find Voter
        Elections election = (Elections) em.find(Elections.class, electionID);
        
        
        
        
        //Remove Election (is this enough?)
        election.getCandidatesList().clear();
        
        EntityTransaction trans = em.getTransaction();
        trans.begin();
        em.remove(election);
        trans.commit();
        
        //Close the Entity Manager & Factory
        em.close();
        emf.close();
        
        
        return true;
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "remCandidate")
    public Boolean remCandidate(@WebParam(name = "electionID") String electionID, @WebParam(name = "candidateID") String candidateID) {
        //TODO write your implementation code here:
        
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("BulletinBoardPU");
        EntityManager em = emf.createEntityManager();
        
        
        //Find Election & Candidate
        Elections election = (Elections) em.find(Elections.class, electionID);
        
        Candidates candidate = (Candidates) em.find(Candidates.class, candidateID);
        
        election.getCandidatesList().remove(candidate);
        
         //Persist the Election
        EntityTransaction trans = em.getTransaction();
        trans.begin();
        em.persist(candidate);
        em.persist(election);
        trans.commit();

        
        
        //Close the Entity Manager & Factory
        em.close();
        emf.close();
        
        
        return true;
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "updateCandidate")
    public Boolean updateCandidate(@WebParam(name = "electionID") String electionID, @WebParam(name = "candidateID") String candidateID, @WebParam(name = "candidateInfo") String candidateInfo) {
        //TODO write your implementation code here:
        
           
        //Gets an Entity Manager & Transaction
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("BulletinBoardPU");
        EntityManager em = emf.createEntityManager();
        
        Candidates candidate = (Candidates) em.find(Candidates.class, candidateID);
        
   
        candidate.setCandidateInfo(candidateInfo);
        
   
    
     //TODO: Alterar datas   
     //   election.setVoteCastInitDate(electionDates.);
        
     
      
        //Persist the Election
        EntityTransaction trans = em.getTransaction();
        trans.begin();
        em.persist(candidate);
        trans.commit();
        
        //Close the Entity Manager & Factory
        em.close();
        emf.close();
       
        return true;
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "signElectionKey")
    public Boolean signElectionKey(@WebParam(name = "electionID") String electionID, @WebParam(name = "signedKey") String signedKey) {
        //TODO write your implementation code here:
        
        //Gets an Entity Manager & Transaction
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("BulletinBoardPU");
        EntityManager em = emf.createEntityManager();
        
        Elections election = (Elections) em.find(Elections.class, electionID);
        
   
        election.setElectionSignedPubKey(signedKey.getBytes());
                
   
    
     //TODO: Alterar datas   
     //   election.setVoteCastInitDate(electionDates.);
        
     
      
        //Persist the Election
        EntityTransaction trans = em.getTransaction();
        trans.begin();
        em.persist(election);
        trans.commit();
        
        //Close the Entity Manager & Factory
        em.close();
        emf.close();
        
        
        return true;
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "signElectionInfo")
    public Boolean signElectionInfo(@WebParam(name = "electionID") String electionID, @WebParam(name = "signedInfo") byte[] signedInfo) {
        //TODO write your implementation code here:
          
        //Gets an Entity Manager & Transaction
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("BulletinBoardPU");
        EntityManager em = emf.createEntityManager();
        
        Elections election = (Elections) em.find(Elections.class, electionID);
        
   
        election.setElectionSignedInfo(signedInfo);
                
   
    
     //TODO: Alterar datas   
     //   election.setVoteCastInitDate(electionDates.);
        
     
      
        //Persist the Election
        EntityTransaction trans = em.getTransaction();
        trans.begin();
        em.persist(election);
        trans.commit();
        
        //Close the Entity Manager & Factory
        em.close();
        emf.close();
        
        
        return true;
    }

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eviv.bb.beans;

import javax.ejb.Stateless;
import javax.ejb.LocalBean;

/**
 *
 * @author abrioso
 */
@Stateless
@LocalBean
public class BulletinBoardBean {

    public String sayHello(String name) {
        return "Hello " + name;
    }

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    
}

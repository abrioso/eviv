/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eviv.bb.beans;

import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author abrioso
 */
@ManagedBean
@SessionScoped
public class ElectionVotersList {
    
    private String ElectionID;
    private String ElectionDescription;
    private List<String> ElectionVoters;

    public String getElectionID() {
        return ElectionID;
    }

    public void setElectionID(String ElectionID) {
        this.ElectionID = ElectionID;
    }

    public List<String> getElectionVoters() {
        return ElectionVoters;
    }

    public void setElectionVoters(List<String> ElectionVoters) {
        this.ElectionVoters = ElectionVoters;
    }

    public String getElectionDescription() {
        return ElectionDescription;
    }

    public void setElectionDescription(String ElectionDescription) {
        this.ElectionDescription = ElectionDescription;
    }
    
    
    public String getSayWelcome(){
	   //check if null?
	   if("".equals(ElectionID) || ElectionID ==null){
		return "";
	   }else{
		return "Ajax message : Welcome " + ElectionID;
	   }
	}
    
    
}

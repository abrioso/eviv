/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eviv.bb.beans.jsf;

import eviv.bb.entities.Elections;
import eviv.bb.entities.Votes;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author abrioso
 */
@ManagedBean
@RequestScoped
public class ElectionJSFManagedBean implements Serializable {

 //   private Elections election = new Elections(); 
    private List<Elections> electionList = new ArrayList<Elections>();
    
    private Elections selectedElection = null;
    
    /** Creates a new instance of ElectionJSFManagedBean */
    public ElectionJSFManagedBean() {
        
        //Gets an Entity Manager & Transaction
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("BulletinBoardPU");
        EntityManager em = emf.createEntityManager();
        
        this.electionList = em.createNamedQuery("Elections.findAll").getResultList();
 
        //Close the Entity Manager & Factory
        em.close();
        emf.close();
         
    }

    public List<Elections> getElectionList() {
        return electionList;
    }
    
    public Elections getElection(String electionID) {
        
        //Gets an Entity Manager & Transaction
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("BulletinBoardPU");
        EntityManager em = emf.createEntityManager();
        
        Elections election = (Elections) em.find(Elections.class, electionID);
   
        
        
        //Close the Entity Manager & Factory
        em.close();
        emf.close();
        
        
        return election;
    }
    
    public Elections getSelectedElection() {
        return selectedElection;
    }
    
    public String setSelectElection(String electionID){  
       // String name = (String) event.getComponent().getAttributes().get("name");  
     //   event.
     //   String name = electionID;
        this.selectedElection= getElection(electionID);  
        return "/showElectionInfo";
    }  
    
    
     public List<Votes> getElectionVotes(String electionID) {
        
         
    //     return selectedElection.
         
        //Gets an Entity Manager & Transaction
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("BulletinBoardPU");
        EntityManager em = emf.createEntityManager();
        
        List<Votes> electionVotes = em.createNamedQuery("Votes.findAll").getResultList();
 
        //Close the Entity Manager & Factory
        em.close();
        emf.close();
        
        return electionVotes;
         
    }
    
    
}

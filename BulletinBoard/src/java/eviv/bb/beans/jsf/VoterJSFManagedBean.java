/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eviv.bb.beans.jsf;

import eviv.bb.entities.Voters;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author abrioso
 */
@ManagedBean
@RequestScoped
public class VoterJSFManagedBean {

    private List<Voters> voterList = new ArrayList<Voters>();
    
    /** Creates a new instance of VoterJSFManagedBean */
    public VoterJSFManagedBean() {
        //Gets an Entity Manager & Transaction
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("BulletinBoardPU");
        EntityManager em = emf.createEntityManager();
        
        this.voterList = em.createNamedQuery("Voters.findAll").getResultList();
 
        //Close the Entity Manager & Factory
        em.close();
        emf.close();
    }

    public List<Voters> getVoterList() {
        return voterList;
    }
    
    
    
}

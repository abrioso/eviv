/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eviv.bb.beans;

import eviv.bb.entities.Candidates;
import eviv.bb.entities.Elections;
import eviv.commons.types.ElectionInfo;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import org.apache.commons.lang3.ArrayUtils;

/**
 *
 * @author abrioso
 */

@Stateless
@LocalBean
public class BBUtil {
    
    /**
     * Converts Elections (eviv.bb.entities) to ElectionInfo (eviv.types)
     * @param election (from DB entities
     * @return
     */
    public static ElectionInfo ConvertElections(Elections election) {
        ElectionInfo info = new ElectionInfo(election.getElectionId());
        
        info.setElectionDesc(election.getElectionDesc());
        
        info.setElectionDates(ElectionInfo.ENROLL_DATE, election.getVoterEnrollInitDate());
        info.setElectionDates(ElectionInfo.REGISTER_DATE, election.getVoterRegisterInitDate());
        info.setElectionDates(ElectionInfo.CAST_DATE, election.getVoteCastInitDate());
        info.setElectionDates(ElectionInfo.COUNT_DATE, election.getVoteCountInitDate());
        
        Boolean[] bmask = new Boolean[4];
        
        Arrays.fill(bmask, Boolean.FALSE);
        
//        info.setElectionCandidates(election.getCandidatesCollection());
  
        for(Candidates c : election.getCandidatesList()) 
            info.addElectionCandidates(c.getCandidateInfo());
       
        info.setElectionPubKey(ArrayUtils.toObject(election.getElectionPubKey()));
    
        try {
            byte[] chalBytes = election.getElectionChallenge();
            if(chalBytes != null) {
            ByteArrayInputStream bis = new ByteArrayInputStream(chalBytes);
            ObjectInput in = new ObjectInputStream(bis);
            info.setElectionChallenge((BigInteger) in.readObject());
            bis.close();
            in.close();
            } else {
                info.setElectionChallenge(BigInteger.ZERO);
            }

        } catch (ClassNotFoundException ex) {
            Logger.getLogger(BBUtil.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(BBUtil.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (election.getElectionSignedInfo() != null) {
            info.setIsPhaseFinished(Boolean.TRUE, ElectionInfo.ENROLL_DATE);
        }


        return info;

    }
    
    
}

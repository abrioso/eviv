/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eviv.bb.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author abrioso
 */
@Entity
@Table(name = "Ballots")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Ballots.findAll", query = "SELECT b FROM Ballots b"),
    @NamedQuery(name = "Ballots.findByBallotId", query = "SELECT b FROM Ballots b WHERE b.ballotId = :ballotId"),
    @NamedQuery(name = "Ballots.findByUrl", query = "SELECT b FROM Ballots b WHERE b.url = :url")})
public class Ballots implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "ballot_id", nullable = false, length = 10)
    private String ballotId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 60)
    @Column(name = "url", nullable = false, length = 60)
    private String url;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ballotsballotid")
    private List<Votes> votesList;

    public Ballots() {
    }

    public Ballots(String ballotId) {
        this.ballotId = ballotId;
    }

    public Ballots(String ballotId, String url) {
        this.ballotId = ballotId;
        this.url = url;
    }

    public String getBallotId() {
        return ballotId;
    }

    public void setBallotId(String ballotId) {
        this.ballotId = ballotId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @XmlTransient
    public List<Votes> getVotesList() {
        return votesList;
    }

    public void setVotesList(List<Votes> votesList) {
        this.votesList = votesList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ballotId != null ? ballotId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Ballots)) {
            return false;
        }
        Ballots other = (Ballots) object;
        if ((this.ballotId == null && other.ballotId != null) || (this.ballotId != null && !this.ballotId.equals(other.ballotId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "eviv.bb.entities.Ballots[ ballotId=" + ballotId + " ]";
    }
    
}

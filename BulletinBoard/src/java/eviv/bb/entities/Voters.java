/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eviv.bb.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author abrioso
 */
@Entity
@Table(name = "Voters")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Voters.findAll", query = "SELECT v FROM Voters v"),
    @NamedQuery(name = "Voters.findByVoterId", query = "SELECT v FROM Voters v WHERE v.voterId = :voterId"),
    @NamedQuery(name = "Voters.findByVoterCertificate", query = "SELECT v FROM Voters v WHERE v.voterCertificate = :voterCertificate")})
public class Voters implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "voter_id", nullable = false, length = 10)
    private String voterId;
    @Size(max = 1024)
    @Column(name = "voter_certificate", length = 1024)
    private String voterCertificate;
    @Lob
    @Column(name = "voter_certificate_bin")
    private byte[] voterCertificateBin;
    @ManyToMany(mappedBy = "votersList")
    private List<Elections> electionsList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "voters")
    private List<Votes> votesList;

    public Voters() {
    }

    public Voters(String voterId) {
        this.voterId = voterId;
    }

    public String getVoterId() {
        return voterId;
    }

    public void setVoterId(String voterId) {
        this.voterId = voterId;
    }

    public String getVoterCertificate() {
        return voterCertificate;
    }

    public void setVoterCertificate(String voterCertificate) {
        this.voterCertificate = voterCertificate;
    }

    public byte[] getVoterCertificateBin() {
        return voterCertificateBin;
    }

    public void setVoterCertificateBin(byte[] voterCertificateBin) {
        this.voterCertificateBin = voterCertificateBin;
    }

    @XmlTransient
    public List<Elections> getElectionsList() {
        return electionsList;
    }

    public void setElectionsList(List<Elections> electionsList) {
        this.electionsList = electionsList;
    }

    @XmlTransient
    public List<Votes> getVotesList() {
        return votesList;
    }

    public void setVotesList(List<Votes> votesList) {
        this.votesList = votesList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (voterId != null ? voterId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Voters)) {
            return false;
        }
        Voters other = (Voters) object;
        if ((this.voterId == null && other.voterId != null) || (this.voterId != null && !this.voterId.equals(other.voterId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "eviv.bb.entities.Voters[ voterId=" + voterId + " ]";
    }
    
}

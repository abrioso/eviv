/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eviv.bb.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author abrioso
 */
@Entity
@Table(name = "Votes")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Votes.findAll", query = "SELECT v FROM Votes v"),
    @NamedQuery(name = "Votes.findByElectionselectionid", query = "SELECT v FROM Votes v WHERE v.votesPK.electionselectionid = :electionselectionid"),
    @NamedQuery(name = "Votes.findByVotersvoterid", query = "SELECT v FROM Votes v WHERE v.votesPK.votersvoterid = :votersvoterid"),
    @NamedQuery(name = "Votes.findByVoteBallot", query = "SELECT v FROM Votes v WHERE v.voteBallot = :voteBallot"),
    @NamedQuery(name = "Votes.findByVoteToken", query = "SELECT v FROM Votes v WHERE v.voteToken = :voteToken"),
    @NamedQuery(name = "Votes.findByVoteReceiptToken", query = "SELECT v FROM Votes v WHERE v.voteReceiptToken = :voteReceiptToken"),
    @NamedQuery(name = "Votes.findByVoteTime", query = "SELECT v FROM Votes v WHERE v.voteTime = :voteTime")})
public class Votes implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected VotesPK votesPK;
    @Size(max = 45)
    @Column(name = "vote_ballot", length = 45)
    private String voteBallot;
    @Lob
    @Column(name = "vote_info")
    private byte[] voteInfo;
    @Size(max = 10)
    @Column(name = "vote_token", length = 10)
    private String voteToken;
    @Size(max = 50)
    @Column(name = "vote_receipt_token", length = 50)
    private String voteReceiptToken;
    @Column(name = "vote_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date voteTime;
    @JoinColumn(name = "Voters_voter_id", referencedColumnName = "voter_id", nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Voters voters;
    @JoinColumn(name = "Elections_election_id", referencedColumnName = "election_id", nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Elections elections;
    @JoinColumn(name = "Ballots_ballot_id", referencedColumnName = "ballot_id", nullable = false)
    @ManyToOne(optional = false)
    private Ballots ballotsballotid;

    public Votes() {
    }

    public Votes(VotesPK votesPK) {
        this.votesPK = votesPK;
    }

    public Votes(String electionselectionid, String votersvoterid) {
        this.votesPK = new VotesPK(electionselectionid, votersvoterid);
    }

    public VotesPK getVotesPK() {
        return votesPK;
    }

    public void setVotesPK(VotesPK votesPK) {
        this.votesPK = votesPK;
    }

    public String getVoteBallot() {
        return voteBallot;
    }

    public void setVoteBallot(String voteBallot) {
        this.voteBallot = voteBallot;
    }

    public byte[] getVoteInfo() {
        return voteInfo;
    }

    public void setVoteInfo(byte[] voteInfo) {
        this.voteInfo = voteInfo;
    }

    public String getVoteToken() {
        return voteToken;
    }

    public void setVoteToken(String voteToken) {
        this.voteToken = voteToken;
    }

    public String getVoteReceiptToken() {
        return voteReceiptToken;
    }

    public void setVoteReceiptToken(String voteReceiptToken) {
        this.voteReceiptToken = voteReceiptToken;
    }

    public Date getVoteTime() {
        return voteTime;
    }

    public void setVoteTime(Date voteTime) {
        this.voteTime = voteTime;
    }

    public Voters getVoters() {
        return voters;
    }

    public void setVoters(Voters voters) {
        this.voters = voters;
    }

    public Elections getElections() {
        return elections;
    }

    public void setElections(Elections elections) {
        this.elections = elections;
    }

    public Ballots getBallotsballotid() {
        return ballotsballotid;
    }

    public void setBallotsballotid(Ballots ballotsballotid) {
        this.ballotsballotid = ballotsballotid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (votesPK != null ? votesPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Votes)) {
            return false;
        }
        Votes other = (Votes) object;
        if ((this.votesPK == null && other.votesPK != null) || (this.votesPK != null && !this.votesPK.equals(other.votesPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "eviv.bb.entities.Votes[ votesPK=" + votesPK + " ]";
    }
    
}

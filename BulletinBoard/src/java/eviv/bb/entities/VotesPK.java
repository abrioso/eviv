/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eviv.bb.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author abrioso
 */
@Embeddable
public class VotesPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "Elections_election_id", nullable = false, length = 8)
    private String electionselectionid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "Voters_voter_id", nullable = false, length = 10)
    private String votersvoterid;

    public VotesPK() {
    }

    public VotesPK(String electionselectionid, String votersvoterid) {
        this.electionselectionid = electionselectionid;
        this.votersvoterid = votersvoterid;
    }

    public String getElectionselectionid() {
        return electionselectionid;
    }

    public void setElectionselectionid(String electionselectionid) {
        this.electionselectionid = electionselectionid;
    }

    public String getVotersvoterid() {
        return votersvoterid;
    }

    public void setVotersvoterid(String votersvoterid) {
        this.votersvoterid = votersvoterid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (electionselectionid != null ? electionselectionid.hashCode() : 0);
        hash += (votersvoterid != null ? votersvoterid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VotesPK)) {
            return false;
        }
        VotesPK other = (VotesPK) object;
        if ((this.electionselectionid == null && other.electionselectionid != null) || (this.electionselectionid != null && !this.electionselectionid.equals(other.electionselectionid))) {
            return false;
        }
        if ((this.votersvoterid == null && other.votersvoterid != null) || (this.votersvoterid != null && !this.votersvoterid.equals(other.votersvoterid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "eviv.bb.entities.VotesPK[ electionselectionid=" + electionselectionid + ", votersvoterid=" + votersvoterid + " ]";
    }
    
}

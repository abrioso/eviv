/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eviv.bb.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author abrioso
 */
@Entity
@Table(name = "Elections")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Elections.findAll", query = "SELECT e FROM Elections e"),
    @NamedQuery(name = "Elections.findByElectionId", query = "SELECT e FROM Elections e WHERE e.electionId = :electionId"),
    @NamedQuery(name = "Elections.findByElectionDesc", query = "SELECT e FROM Elections e WHERE e.electionDesc = :electionDesc"),
    @NamedQuery(name = "Elections.findByVoterEnrollInitDate", query = "SELECT e FROM Elections e WHERE e.voterEnrollInitDate = :voterEnrollInitDate"),
    @NamedQuery(name = "Elections.findByVoterRegisterInitDate", query = "SELECT e FROM Elections e WHERE e.voterRegisterInitDate = :voterRegisterInitDate"),
    @NamedQuery(name = "Elections.findByVoteCastInitDate", query = "SELECT e FROM Elections e WHERE e.voteCastInitDate = :voteCastInitDate"),
    @NamedQuery(name = "Elections.findByVoteCountInitDate", query = "SELECT e FROM Elections e WHERE e.voteCountInitDate = :voteCountInitDate"),
    @NamedQuery(name = "Elections.findByElectionPhase", query = "SELECT e FROM Elections e WHERE e.electionPhase = :electionPhase"),
    @NamedQuery(name = "Elections.findByCandidatesCount", query = "SELECT e FROM Elections e WHERE e.candidatesCount = :candidatesCount"),
    @NamedQuery(name = "Elections.findByVotesCount", query = "SELECT e FROM Elections e WHERE e.votesCount = :votesCount")})
public class Elections implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "election_id", nullable = false, length = 8)
    private String electionId;
    @Size(max = 45)
    @Column(name = "election_desc", length = 45)
    private String electionDesc;
    @Column(name = "voter_enroll_init_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date voterEnrollInitDate;
    @Column(name = "voter_register_init_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date voterRegisterInitDate;
    @Column(name = "vote_cast_init_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date voteCastInitDate;
    @Column(name = "vote_count_init_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date voteCountInitDate;
    @Column(name = "election_phase")
    private Integer electionPhase;
    @Column(name = "candidates_count")
    private Integer candidatesCount;
    @Lob
    @Column(name = "election_pub_key")
    private byte[] electionPubKey;
    @Lob
    @Column(name = "election_signed_pub_key")
    private byte[] electionSignedPubKey;
    @Lob
    @Column(name = "election_signed_info")
    private byte[] electionSignedInfo;
    @Lob
    @Column(name = "election_challenge")
    private byte[] electionChallenge;
    @Lob
    @Column(name = "election_signed_challenge")
    private byte[] electionSignedChallenge;
    @Lob
    @Column(name = "election_results")
    private byte[] electionResults;
    @Lob
    @Column(name = "election_agregated_votes")
    private byte[] electionAgregatedVotes;
    @Lob
    @Column(name = "election_aggregation_verification")
    private byte[] electionAggregationVerification;
    @Lob
    @Column(name = "election_decrypted_aggregated_votes")
    private byte[] electionDecryptedAggregatedVotes;
    @Column(name = "votes_count")
    private Integer votesCount;
    @JoinTable(name = "Elections_has_Voters", joinColumns = {
        @JoinColumn(name = "Elections_election_id", referencedColumnName = "election_id", nullable = false)}, inverseJoinColumns = {
        @JoinColumn(name = "Voters_voter_id", referencedColumnName = "voter_id", nullable = false)})
    @ManyToMany
    private List<Voters> votersList;
    @JoinTable(name = "Elections_has_Trustees", joinColumns = {
        @JoinColumn(name = "Elections_election_id", referencedColumnName = "election_id", nullable = false)}, inverseJoinColumns = {
        @JoinColumn(name = "Trustees_idTrustees", referencedColumnName = "trustee_id", nullable = false)})
    @ManyToMany
    private List<Trustees> trusteesList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "elections")
    private List<Votes> votesList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "elections")
    private List<Candidates> candidatesList;

    public Elections() {
    }

    public Elections(String electionId) {
        this.electionId = electionId;
    }

    public String getElectionId() {
        return electionId;
    }

    public void setElectionId(String electionId) {
        this.electionId = electionId;
    }

    public String getElectionDesc() {
        return electionDesc;
    }

    public void setElectionDesc(String electionDesc) {
        this.electionDesc = electionDesc;
    }

    public Date getVoterEnrollInitDate() {
        return voterEnrollInitDate;
    }

    public void setVoterEnrollInitDate(Date voterEnrollInitDate) {
        this.voterEnrollInitDate = voterEnrollInitDate;
    }

    public Date getVoterRegisterInitDate() {
        return voterRegisterInitDate;
    }

    public void setVoterRegisterInitDate(Date voterRegisterInitDate) {
        this.voterRegisterInitDate = voterRegisterInitDate;
    }

    public Date getVoteCastInitDate() {
        return voteCastInitDate;
    }

    public void setVoteCastInitDate(Date voteCastInitDate) {
        this.voteCastInitDate = voteCastInitDate;
    }

    public Date getVoteCountInitDate() {
        return voteCountInitDate;
    }

    public void setVoteCountInitDate(Date voteCountInitDate) {
        this.voteCountInitDate = voteCountInitDate;
    }

    public Integer getElectionPhase() {
        return electionPhase;
    }

    public void setElectionPhase(Integer electionPhase) {
        this.electionPhase = electionPhase;
    }

    public Integer getCandidatesCount() {
        return candidatesCount;
    }

    public void setCandidatesCount(Integer candidatesCount) {
        this.candidatesCount = candidatesCount;
    }

    public byte[] getElectionPubKey() {
        return electionPubKey;
    }

    public void setElectionPubKey(byte[] electionPubKey) {
        this.electionPubKey = electionPubKey;
    }

    public byte[] getElectionSignedPubKey() {
        return electionSignedPubKey;
    }

    public void setElectionSignedPubKey(byte[] electionSignedPubKey) {
        this.electionSignedPubKey = electionSignedPubKey;
    }

    public byte[] getElectionSignedInfo() {
        return electionSignedInfo;
    }

    public void setElectionSignedInfo(byte[] electionSignedInfo) {
        this.electionSignedInfo = electionSignedInfo;
    }

    public byte[] getElectionChallenge() {
        return electionChallenge;
    }

    public void setElectionChallenge(byte[] electionChallenge) {
        this.electionChallenge = electionChallenge;
    }

    public byte[] getElectionSignedChallenge() {
        return electionSignedChallenge;
    }

    public void setElectionSignedChallenge(byte[] electionSignedChallenge) {
        this.electionSignedChallenge = electionSignedChallenge;
    }

    public byte[] getElectionResults() {
        return electionResults;
    }

    public void setElectionResults(byte[] electionResults) {
        this.electionResults = electionResults;
    }

    public byte[] getElectionAgregatedVotes() {
        return electionAgregatedVotes;
    }

    public void setElectionAgregatedVotes(byte[] electionAgregatedVotes) {
        this.electionAgregatedVotes = electionAgregatedVotes;
    }

    public byte[] getElectionAggregationVerification() {
        return electionAggregationVerification;
    }

    public void setElectionAggregationVerification(byte[] electionAggregationVerification) {
        this.electionAggregationVerification = electionAggregationVerification;
    }

    public byte[] getElectionDecryptedAggregatedVotes() {
        return electionDecryptedAggregatedVotes;
    }

    public void setElectionDecryptedAggregatedVotes(byte[] electionDecryptedAggregatedVotes) {
        this.electionDecryptedAggregatedVotes = electionDecryptedAggregatedVotes;
    }

    public Integer getVotesCount() {
        return votesCount;
    }

    public void setVotesCount(Integer votesCount) {
        this.votesCount = votesCount;
    }

    @XmlTransient
    public List<Voters> getVotersList() {
        return votersList;
    }

    public void setVotersList(List<Voters> votersList) {
        this.votersList = votersList;
    }

    @XmlTransient
    public List<Trustees> getTrusteesList() {
        return trusteesList;
    }

    public void setTrusteesList(List<Trustees> trusteesList) {
        this.trusteesList = trusteesList;
    }

    @XmlTransient
    public List<Votes> getVotesList() {
        return votesList;
    }

    public void setVotesList(List<Votes> votesList) {
        this.votesList = votesList;
    }

    @XmlTransient
    public List<Candidates> getCandidatesList() {
        return candidatesList;
    }

    public void setCandidatesList(List<Candidates> candidatesList) {
        this.candidatesList = candidatesList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (electionId != null ? electionId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Elections)) {
            return false;
        }
        Elections other = (Elections) object;
        if ((this.electionId == null && other.electionId != null) || (this.electionId != null && !this.electionId.equals(other.electionId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "eviv.bb.entities.Elections[ electionId=" + electionId + " ]";
    }
    
}

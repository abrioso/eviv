/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eviv.bb.entities;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author abrioso
 */
@Entity
@Table(name = "Candidates")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Candidates.findAll", query = "SELECT c FROM Candidates c"),
    @NamedQuery(name = "Candidates.findByCandidateId", query = "SELECT c FROM Candidates c WHERE c.candidatesPK.candidateId = :candidateId"),
    @NamedQuery(name = "Candidates.findByElectionselectionid", query = "SELECT c FROM Candidates c WHERE c.candidatesPK.electionselectionid = :electionselectionid"),
    @NamedQuery(name = "Candidates.findByCandidateInfo", query = "SELECT c FROM Candidates c WHERE c.candidateInfo = :candidateInfo")})
public class Candidates implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CandidatesPK candidatesPK;
    @Size(max = 45)
    @Column(name = "candidate_info", length = 45)
    private String candidateInfo;
    @JoinColumn(name = "Elections_election_id", referencedColumnName = "election_id", nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Elections elections;

    public Candidates() {
    }

    public Candidates(CandidatesPK candidatesPK) {
        this.candidatesPK = candidatesPK;
    }

    public Candidates(int candidateId, String electionselectionid) {
        this.candidatesPK = new CandidatesPK(candidateId, electionselectionid);
    }

    public CandidatesPK getCandidatesPK() {
        return candidatesPK;
    }

    public void setCandidatesPK(CandidatesPK candidatesPK) {
        this.candidatesPK = candidatesPK;
    }

    public String getCandidateInfo() {
        return candidateInfo;
    }

    public void setCandidateInfo(String candidateInfo) {
        this.candidateInfo = candidateInfo;
    }

    public Elections getElections() {
        return elections;
    }

    public void setElections(Elections elections) {
        this.elections = elections;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (candidatesPK != null ? candidatesPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Candidates)) {
            return false;
        }
        Candidates other = (Candidates) object;
        if ((this.candidatesPK == null && other.candidatesPK != null) || (this.candidatesPK != null && !this.candidatesPK.equals(other.candidatesPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "eviv.bb.entities.Candidates[ candidatesPK=" + candidatesPK + " ]";
    }
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eviv.bb.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author abrioso
 */
@Embeddable
public class CandidatesPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "candidate_id", nullable = false)
    private int candidateId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "Elections_election_id", nullable = false, length = 8)
    private String electionselectionid;

    public CandidatesPK() {
    }

    public CandidatesPK(int candidateId, String electionselectionid) {
        this.candidateId = candidateId;
        this.electionselectionid = electionselectionid;
    }

    public int getCandidateId() {
        return candidateId;
    }

    public void setCandidateId(int candidateId) {
        this.candidateId = candidateId;
    }

    public String getElectionselectionid() {
        return electionselectionid;
    }

    public void setElectionselectionid(String electionselectionid) {
        this.electionselectionid = electionselectionid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) candidateId;
        hash += (electionselectionid != null ? electionselectionid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CandidatesPK)) {
            return false;
        }
        CandidatesPK other = (CandidatesPK) object;
        if (this.candidateId != other.candidateId) {
            return false;
        }
        if ((this.electionselectionid == null && other.electionselectionid != null) || (this.electionselectionid != null && !this.electionselectionid.equals(other.electionselectionid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "eviv.bb.entities.CandidatesPK[ candidateId=" + candidateId + ", electionselectionid=" + electionselectionid + " ]";
    }
    
}

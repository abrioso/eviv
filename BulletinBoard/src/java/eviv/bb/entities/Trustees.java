/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eviv.bb.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author abrioso
 */
@Entity
@Table(name = "Trustees")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Trustees.findAll", query = "SELECT t FROM Trustees t"),
    @NamedQuery(name = "Trustees.findByTrusteeId", query = "SELECT t FROM Trustees t WHERE t.trusteeId = :trusteeId"),
    @NamedQuery(name = "Trustees.findByTrusteeCertificate", query = "SELECT t FROM Trustees t WHERE t.trusteeCertificate = :trusteeCertificate")})
public class Trustees implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "trustee_id", nullable = false, length = 10)
    private String trusteeId;
    @Size(max = 1024)
    @Column(name = "trustee_certificate", length = 1024)
    private String trusteeCertificate;
    @ManyToMany(mappedBy = "trusteesList")
    private List<Elections> electionsList;

    public Trustees() {
    }

    public Trustees(String trusteeId) {
        this.trusteeId = trusteeId;
    }

    public String getTrusteeId() {
        return trusteeId;
    }

    public void setTrusteeId(String trusteeId) {
        this.trusteeId = trusteeId;
    }

    public String getTrusteeCertificate() {
        return trusteeCertificate;
    }

    public void setTrusteeCertificate(String trusteeCertificate) {
        this.trusteeCertificate = trusteeCertificate;
    }

    @XmlTransient
    public List<Elections> getElectionsList() {
        return electionsList;
    }

    public void setElectionsList(List<Elections> electionsList) {
        this.electionsList = electionsList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (trusteeId != null ? trusteeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Trustees)) {
            return false;
        }
        Trustees other = (Trustees) object;
        if ((this.trusteeId == null && other.trusteeId != null) || (this.trusteeId != null && !this.trusteeId.equals(other.trusteeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "eviv.bb.entities.Trustees[ trusteeId=" + trusteeId + " ]";
    }
    
}

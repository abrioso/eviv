/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eviv.voterapp;

import eviv.bb.web.ElectionInfo;
import eviv.bb.web.ElectionPartialInfo;
import eviv.commons.rmi.VSTRemote;
import eviv.commons.types.CodeCard;
import eviv.commons.types.VoterElection;
import gsd.inescid.crypto.ElGamalPublicKey;
import java.awt.Component;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import org.apache.commons.lang3.ArrayUtils;


/**
 *
 * @author abrioso
 */
public class VoterAppUtil {

    public static String connectVST() {

        String voterID = null;

        Registry vstRegistry = null;
        VSTRemote vstRemote = null;
        try {
            vstRegistry = LocateRegistry.getRegistry(VSTRemote.REGISTRY_PORT);
            vstRemote = (VSTRemote) vstRegistry.lookup(VSTRemote.REGISTRY_NAME);
        } catch (RemoteException ex) {
            Logger.getLogger(VoterAppUtil.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NotBoundException ex) {
            Logger.getLogger(VoterAppUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            voterID = vstRemote.getVoterID();
        } catch (RemoteException ex) {
            Logger.getLogger(VoterAppUtil.class.getName()).log(Level.SEVERE, null, ex);
        }

        return voterID;

    }
    
    private static VSTRemote connect2VSTRemote() {

//        String voterID = null;

        Registry vstRegistry = null;
        VSTRemote vstRemote = null;
        try {
            vstRegistry = LocateRegistry.getRegistry(VSTRemote.REGISTRY_PORT);
            vstRemote = (VSTRemote) vstRegistry.lookup(VSTRemote.REGISTRY_NAME);
        } catch (RemoteException ex) {
            Logger.getLogger(VoterAppUtil.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NotBoundException ex) {
            Logger.getLogger(VoterAppUtil.class.getName()).log(Level.SEVERE, null, ex);
        }

        return vstRemote;

    } 
    
    public static String getVoterID() {
        
        VSTRemote vst = connect2VSTRemote();
        
        String voterID = null;
        try {
            voterID = vst.getVoterID();
        } catch (RemoteException ex) {
            Logger.getLogger(VoterAppUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return voterID;
    }

    public static java.util.List<eviv.bb.web.ElectionPartialInfo> getElectionList() {
        eviv.er.web.ElectionRegistration_Service service = new eviv.er.web.ElectionRegistration_Service();
        eviv.er.web.ElectionRegistration port = service.getElectionRegistrationPort();
        return port.getElectionList();
    }

    private static ElectionInfo getElectionInfo(java.lang.String electionID) {
        eviv.er.web.ElectionRegistration_Service service = new eviv.er.web.ElectionRegistration_Service();
        eviv.er.web.ElectionRegistration port = service.getElectionRegistrationPort();
        return port.getElectionInfo(electionID);
    }

    
    private static Boolean registerVoter(java.lang.String electionID, java.lang.String voterID) {
        eviv.er.web.ElectionRegistration_Service service = new eviv.er.web.ElectionRegistration_Service();
        eviv.er.web.ElectionRegistration port = service.getElectionRegistrationPort();
        return port.registerVoter(electionID, voterID);
    }
    
    public static Boolean register2Election(String electionID) {
     
        VSTRemote vst = connect2VSTRemote();
        
        ElectionInfo elInfo = getElectionInfo(electionID);
        
        
        byte[] kpub_bytes = ArrayUtils.toPrimitive(elInfo.getElectionPubKey().toArray(new Byte[0]));
        
       
        ObjectInputStream inStream = null;
        ByteArrayInputStream inByteStream = null;

        ElGamalPublicKey kpub = null;
        
        inByteStream = new ByteArrayInputStream(kpub_bytes);
        try {
            inStream = new ObjectInputStream(inByteStream);
        
            kpub = (ElGamalPublicKey) inStream.readObject();
        
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(VoterAppUtil.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(VoterAppUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
       
        
        try {
            //vst.addElection(electionID, elInfo.getNumCandidates(), kpub);
       //TODO: utilizar boolean de retorno do vst.addElection(..)     
            vst.addElection(electionID, elInfo.getElectionCandidates().toArray(new String[0]), kpub);
            
        } catch (RemoteException ex) {
            Logger.getLogger(VoterAppUtil.class.getName()).log(Level.SEVERE, null, ex);
            //TODO: Mostrar DialogBox
            JOptionPane.showMessageDialog(null, "Couldn't connect to VST. \n"
                    + " Please check if it is "
                    + "connected or isn't initiated.\n" + ex.getMessage(),
                    "Couldn't connect to VST" ,
                    JOptionPane.ERROR_MESSAGE);
        }
                
        return registerVoter(electionID, getVoterID());
    } 
    
        public static CodeCard getCodeCard(String electionID) {
        VSTRemote vst = connect2VSTRemote();

        VoterElection election = null;

        try {
            election = vst.getElection(electionID);
        } catch (RemoteException ex) {
            Logger.getLogger(VoterAppUtil.class.getName()).log(Level.SEVERE, null, ex);
        }

        //String[][] codeCardWithCand = new String[election.getNumCandidates()][2];
        //String[] codeCardVST = election.getCodeCard();

        CodeCard codeCard = election.getCodeCard(); 
        
      /*  
        //TODO: Mudar para receber o nome do candidato mm
        if (codeCardVST.length == codeCardWithCand.length) {
            for (int i = 0; i < codeCardVST.length; i++) {
                codeCardWithCand[i][0] = "Cand " + i;
                codeCardWithCand[i][1] = codeCardVST[i];
            }
       

  
        }
       */
          return codeCard;

    }
    
     public static String getVoteReceipt(String electionID) {
        VSTRemote vst = connect2VSTRemote();

        VoterElection election = null;

        try {
            election = vst.getElection(electionID);
        } catch (RemoteException ex) {
            Logger.getLogger(VoterAppUtil.class.getName()).log(Level.SEVERE, null, ex);
        }

      //  String[][] codeCardWithCand = new String[election.getNumCandidates()][2];
      //  String[] codeCardVST = election.getCodeCard();

      //  String voteReceipt = election.getVote().getReceipt();
          String voteReceipt = election.getVoteReceipt();
        
        
        //TODO: Mudar para receber o nome do candidato mm
     /*   if (codeCardVST.length == codeCardWithCand.length) {
            for (int i = 0; i < codeCardVST.length; i++) {
                codeCardWithCand[i][0] = "Cand " + i;
                codeCardWithCand[i][1] = codeCardVST[i];
            }



        } */
        
        return voteReceipt;

    }
    
    
    
    
    public static Boolean vote(String electionID, String voteCode) {
        
        VSTRemote vst = connect2VSTRemote();
        
        byte[] vote = null;
        try {
            vote = vst.generateVote(electionID, voteCode);
        } catch (RemoteException ex) {
            Logger.getLogger(VoterAppUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return sendVote(electionID, getVoterID(), vote);
    }

    
    private static Boolean sendVote(java.lang.String electionID, java.lang.String voterID, byte[] vote) {
        eviv.bbox.web.BallotBox_Service service = new eviv.bbox.web.BallotBox_Service();
        eviv.bbox.web.BallotBox port = service.getBallotBoxPort();
        return port.sendVote(electionID, voterID, vote);
    }
    
   
    public static boolean printCodeCard(String electionID) {
        
        CodeCard codeCard = VoterAppUtil.getCodeCard(electionID);

        //TODO Create a PDF with the codeCard and print

     //   JOptionPane.showMessageDialog(this, codeCard);


        PrinterJob pjob = PrinterJob.getPrinterJob();
        pjob.setPrintable(codeCard);
        boolean ok = pjob.printDialog();
        if (ok) {
            try {
                pjob.print();
            } catch (PrinterException ex) {
                Logger.getLogger(VoterAppUtil.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
//        JOptionPane.showMessageDialog(null, codeCard);

         return ok;
    }
    
    
    public static void refreshEletionLists(JApplet applet) {
        
         //TODO: Mudar para receber as listas dos diferentes webservices (as listas devem ser filtradas ai 
        List<ElectionPartialInfo> elections = VoterAppUtil.getElectionList();

        
        
        if (!elections.isEmpty()) {
            //TODO - Distinguish which Panel is selected
            
            JRootPane rootpane = (JRootPane) applet.getComponent(0);
            
            for (Component capp : applet.getContentPane().getComponents()) {
                if ((capp != null) && (capp.getName() != null)) {
                    if (capp.getName().equalsIgnoreCase("registerElectionPanel")) {
                        for (Component c : ((JPanel) capp).getComponents()) {
                            if ((c != null)
                                    //   && (c.getClass() == JComboBox.class)
                                    && (c.getName() != null)
                                    && (c.getName().equals("electionIDSelection"))) {
                                JComboBox cbox = (JComboBox) c;
                                cbox.removeAllItems();
                                for (ElectionPartialInfo eid : elections) {
                                    cbox.addItem(eid.getElectionID() + " - " + eid.getElectionDesc());
                                }
                            }
                        }
                    } else if (capp.getName().equalsIgnoreCase("VotePanel")) {
                        for (Component c : ((JPanel) capp).getComponents()) {
                            if ((c != null)
                                    //   && (c.getClass() == JComboBox.class)
                                    && (c.getName() != null)
                                    && (c.getName().equals("electionIDSelection"))) {
                                JComboBox cbox = (JComboBox) c;
                                cbox.removeAllItems();
                                for (ElectionPartialInfo eid : elections) {
                                    cbox.addItem(eid.getElectionID() + " - " + eid.getElectionDesc());
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    

    
    
    
    
}



/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eviv.voterapp;

import eviv.voterapp.ui.VoterAppApplet;
import java.awt.BorderLayout;
import javax.swing.JFrame;


/**
 *
 * @author abrioso
 */
public class VoterApp {

    

    
     
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
                JFrame frame = new JFrame("VoterApp");
                // an application has to determine its size (Applet done by .html page)
                frame.setSize(540, 320);
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                // create the applet
                VoterAppApplet applet = new VoterAppApplet();
                // call the init method to initialize GUI
                applet.init();   
                // add the applet to the Frame and make it visible
                frame.add(applet, BorderLayout.CENTER);
                frame.setVisible(true);
        
        
    }
}

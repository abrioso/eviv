/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eviv.VST;

import eviv.commons.rmi.VSTRemote;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.rmi.AccessException;
import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;


       

/**
 *
 * @author abrioso
 */
public class VoterSecurityTokenMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        VoterSecurityToken vst = null;

        //---
        String frameTitle = "VST-Dummy";
        JFrame frame = new JFrame(frameTitle);
        frame.setSize((int) (frameTitle.length()*20), 0);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //--

        // If we have arguments (keys) to load, use the constructor
        if (args.length >= 1) {

            ObjectInputStream inStream = null;
                 
            String voterID = null;

            if (args[0].endsWith(".vst")) {
                voterID = args[0].substring(0, args[0].length() - 5);
            } else {
                voterID = args[0];
            }
            try {
                inStream = new ObjectInputStream(new FileInputStream(voterID + ".vst"));
                vst = (VoterSecurityToken) inStream.readObject();
                inStream.close();


            } catch (ClassNotFoundException ex) {
                Logger.getLogger(VoterSecurityTokenMain.class.getName()).log(Level.SEVERE, null, ex);
                vst = new VoterSecurityToken();
            } catch (IOException ex) {
                Logger.getLogger(VoterSecurityTokenMain.class.getName()).log(Level.SEVERE, null, ex);
                vst = new VoterSecurityToken();
            }



        } else {
            vst = new VoterSecurityToken();
        }

        java.security.Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
        
        
        try {
            VSTRemote vstRemote = (VSTRemote) UnicastRemoteObject.exportObject(vst, 0);

            Registry registry = LocateRegistry.createRegistry(VSTRemote.REGISTRY_PORT);
            //         Registry registry = LocateRegistry.getRegistry();
            registry.bind(VSTRemote.REGISTRY_NAME, vstRemote);


        } catch (AlreadyBoundException ex) {
            Logger.getLogger(VoterSecurityTokenMain.class.getName()).log(Level.SEVERE, null, ex);
        } catch (AccessException ex) {
            Logger.getLogger(VoterSecurityTokenMain.class.getName()).log(Level.SEVERE, null, ex);
        } catch (RemoteException ex) {
            Logger.getLogger(VoterSecurityTokenMain.class.getName()).log(Level.SEVERE, null, ex);
        }


        //TODO: Acrescentar UI
        
        

        //TODO remover teste
//        try {
//            Registry registry = LocateRegistry.getRegistry(VSTRemote.REGISTRY_PORT);
//            VSTRemote vstRemote = (VSTRemote) registry.lookup(VSTRemote.REGISTRY_NAME);
//            if (vstRemote.initVST("A001")) {
//                System.out.println("VST de " + vstRemote.getVoterID()
//                        + " inicializado com kPub:\n" + vstRemote.getKeyPair().getPublic().toString());
////                System.out.println("VST INFO: " + vstRemote.toString());
//
//            }
//            
//            
//
//
//        } catch (RemoteException ex) {
//            Logger.getLogger(VoterSecurityTokenMain.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (NotBoundException ex) {
//            Logger.getLogger(VoterSecurityTokenMain.class.getName()).log(Level.SEVERE, null, ex);
//        }
//
//
//        //vst.initVST("A000");
//





    }
    
//    private X509Certificate genCert(ElGamalPublicKey kpub, ElGamalPrivateKey kpriv) {
//        X509V3CertificateGenerator certGen = new X509V3CertificateGenerator();
//        
//        
//        
//        return certGen.generate((kpriv);
//        
//    }
    
}

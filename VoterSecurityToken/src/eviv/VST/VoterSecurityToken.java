/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eviv.VST;

import eviv.commons.types.VoterElection;
import eviv.commons.rmi.VSTRemote;
import eviv.commons.types.CodeCard;
import gsd.inescid.crypto.ElGamalPublicKey;
import gsd.inescid.markpledge3.MP3Parameters;
import gsd.inescid.markpledge3.MP3VoteAndReceipt;
import gsd.inescid.markpledge3.MP3VoteFactory;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.rmi.RemoteException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.SignedObject;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang3.RandomStringUtils;

/**
 *
 * @author abrioso
 */
public class VoterSecurityToken implements Serializable, VSTRemote {

    public static final int[] KEY_SIZE = {// p ,  q 
        1024, 512};
    public static final String HASH_FUNCTION = "SHA-1";
    String voterID;
//    ElGamalKeyPair keyPair;
    KeyPair keyPair;

    List<VoterElection> electionList = null;
    
    public VoterSecurityToken() {
        this.voterID = null;
        this.keyPair = null;
        this.electionList = new ArrayList<VoterElection>();
    }

    
    @Override
    public KeyPair getKeyPair() {
        return keyPair;
    }

    
    @Override
    public String getVoterID() {
        return voterID;
    }

    public VoterSecurityToken(String keysFile) {
        //TODO: load the files contents and init the variables
    }

    @Override
    public Boolean initVST(String voterID) {

        if(this.voterID != null || this.keyPair != null){
            System.out.println("VST already initiated... Init Aborted!!");
            System.out.println("VoterID: " + getVoterID()
                    + "\n kPub: \n" + getKeyPair().getPublic().toString());
            return false;
            //TODO: Trow Exception
        }
            
            this.voterID = voterID;

            this.keyPair = generateKeys();
            
 //           this.rsaKeyPair = rsaGenerateKeys();
            
        if (keyPair != null) {
            System.out.println("VST Initiated for voterID = " + getVoterID());
            System.out.println("with kPub: \n" + getKeyPair().getPublic().toString());
            System.out.println("with kPriv: \n" + getKeyPair().getPrivate().toString());
            saveData();
            return true;
        } else {
            System.out.println("VST not Initiated");
            this.voterID = null;
            this.keyPair = null;
            return false;
        }

    }

    
    
    
    
      private KeyPair generateKeys()  {
    
        PublicKey kpub = null;
        PrivateKey kpriv = null;
  
        KeyFactory kfact = null;
        
        KeyPair keys = null;
  
        KeyPairGenerator kpg;
        try {
            kpg = KeyPairGenerator.getInstance("RSA", "BC");
             kpg.initialize(1024);
             keys = kpg.genKeyPair();
             
        } catch (NoSuchProviderException ex) {
            Logger.getLogger(VoterSecurityToken.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(VoterSecurityToken.class.getName()).log(Level.SEVERE, null, ex);
        }
   
 
          
        return keys;
    }
    
    
    

    private void saveData() {

        ObjectOutputStream outStream = null;
        
        try {
       
       outStream = new ObjectOutputStream( new FileOutputStream(getVoterID() + ".vst"));
       outStream.writeObject( this );
       outStream.flush();
        } catch (IOException ex) {
            Logger.getLogger(VoterSecurityToken.class.getName()).log(Level.SEVERE, null, ex);
        }

        String fileLocation = new File(getVoterID() + ".vst").getAbsolutePath();
        
        System.out.println("Data saved to " + fileLocation);

    }

    @Override
    public Boolean addElection(String electionID, String[] candDesc, ElGamalPublicKey kpub) throws RemoteException {

        int numCand = candDesc.length;
        
        VoterElection election = new VoterElection();
        
        election.setElectionID(electionID);
        election.setNumCandidates(numCand);
        election.setCandDesc(candDesc);
        election.setKpub(kpub);
        
        //TODO: Verificar  se param deve ser kpuv.g ou outro BigInt gerado pelo Trustee?
        election.setElectionParam(new MP3Parameters(kpub, kpub.g));
        
       
    //    election.setCodeCard(generateCodeCard(numCand));
        
        election.setBallot(generateBallot(election.getElectionParam(), numCand));
        
        // Generate CodeCard
        election.setCodeCard();
  //      election.getCodeCard().setCandidatesDesc(election.getCandDesc());
        
        Boolean added = this.electionList.add(election);
        
        saveData();
        
        return added;
    
    
    }
    
    @Deprecated
    private String[] generateCodeCard(int numCand) {
        
        String[] codes = new String[numCand];
        
        for(int i =0; i < numCand; i++) {
            codes[i] = RandomStringUtils.randomAlphanumeric(5);
        }
        
        return codes;

    }
    
    
    private MP3VoteFactory generateBallot(MP3Parameters param, int numCand) {
       
        
        
        MP3VoteFactory ballot = new MP3VoteFactory(param, HASH_FUNCTION, true);
        try {
            ballot.prepareVote(numCand);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(VoterSecurityToken.class.getName()).log(Level.SEVERE, null, ex);
        }
                
        
        return ballot;
    }

    @Override
    public  VoterElection getElection(String electionID) throws RemoteException {

        VoterElection election = null;
        
        for(VoterElection el : electionList) {
            if(el.getElectionID().equals(electionID))
                election = el;
        }
    
        return election;
        
    }
    

    @Override
     public  byte[] generateVote(String electionID, String voteCode) throws RemoteException {
                
       // String[] codeCard = null;
        CodeCard codeCard = null;
  //      int codeIndex = -1;
        
        VoterElection election = getElection(electionID);
        MP3VoteFactory voteFactory = election.getBallot();
        
        
        MP3VoteAndReceipt vote = null;
        byte[] vote_bytes = null;
        
 /*       codeCard = election.getCodeCard();
        
        for(int i = 0; i < codeCard.length; i++) {
            if(codeCard[i].equals(voteCode)) {
                codeIndex = i;
                break;
            }
        }
    
   */     
        int codeIndex = election.getCodeCard().getCodeIndex(voteCode);
        
        if(codeIndex == -1) 
            return null;
        
        
//TODO: acrescentar o chalenge -> voteFactory.getVoteAndReset(codeIndex, chal)         
        // Generate Vote and Receipt
        vote = voteFactory.getVoteAndReset(codeIndex);
        
        election.setVote(vote);
        
                
        //SignVote
        
        SignedObject so = null;
        
        Signature sig = null;
        
        try {
            sig = Signature.getInstance("MD5WithRSA");
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(VoterSecurityToken.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        try {
            so = new SignedObject(vote, getKeyPair().getPrivate(), sig);
        } catch (IOException ex) {
            Logger.getLogger(VoterSecurityToken.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidKeyException ex) {
            Logger.getLogger(VoterSecurityToken.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SignatureException ex) {
            Logger.getLogger(VoterSecurityToken.class.getName()).log(Level.SEVERE, null, ex);
        }

        
        ObjectOutputStream outStream = null;
        ByteArrayOutputStream outByteStream = null;
        outByteStream = new ByteArrayOutputStream();
        try {
            outStream = new ObjectOutputStream(outByteStream);
            outStream.writeObject(so);
            outStream.flush();
        } catch (IOException ex) {
            Logger.getLogger(VoterSecurityToken.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        byte[] outBytes = null;
        outBytes = outByteStream.toByteArray();

        return outBytes;
    }

  
    
  
    
    
    
    
}

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

SHOW WARNINGS;
DROP SCHEMA IF EXISTS `EVIV_BulletinBoard` ;
CREATE SCHEMA IF NOT EXISTS `EVIV_BulletinBoard` ;
SHOW WARNINGS;
DROP SCHEMA IF EXISTS `EVIV_ElectoralComission` ;
CREATE SCHEMA IF NOT EXISTS `EVIV_ElectoralComission` ;
SHOW WARNINGS;
DROP SCHEMA IF EXISTS `EVIV_BallotBox` ;
CREATE SCHEMA IF NOT EXISTS `EVIV_BallotBox` ;
SHOW WARNINGS;
DROP SCHEMA IF EXISTS `EVIV_ElectionRegistrar` ;
CREATE SCHEMA IF NOT EXISTS `EVIV_ElectionRegistrar` ;
SHOW WARNINGS;
USE `EVIV_BulletinBoard` ;

-- -----------------------------------------------------
-- Table `EVIV_BulletinBoard`.`Voters`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `EVIV_BulletinBoard`.`Voters` ;

SHOW WARNINGS;
CREATE  TABLE IF NOT EXISTS `EVIV_BulletinBoard`.`Voters` (
  `voter_id` VARCHAR(10) NOT NULL ,
  `voter_certificate` VARCHAR(1024) NULL ,
  `voter_certificate_bin` BLOB NULL ,
  PRIMARY KEY (`voter_id`) )
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `EVIV_BulletinBoard`.`Elections`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `EVIV_BulletinBoard`.`Elections` ;

SHOW WARNINGS;
CREATE  TABLE IF NOT EXISTS `EVIV_BulletinBoard`.`Elections` (
  `election_id` VARCHAR(8) NOT NULL ,
  `election_desc` VARCHAR(45) NULL ,
  `voter_enroll_init_date` DATETIME NULL ,
  `voter_register_init_date` DATETIME NULL ,
  `vote_cast_init_date` DATETIME NULL ,
  `vote_count_init_date` DATETIME NULL ,
  `election_phase` INT NULL ,
  `candidates_count` INT NULL ,
  `election_pub_key` BLOB NULL ,
  `election_signed_pub_key` BLOB NULL ,
  `election_signed_info` BLOB NULL ,
  `election_challenge` BLOB NULL ,
  `election_signed_challenge` BLOB NULL ,
  `election_results` BLOB NULL ,
  `election_agregated_votes` BLOB NULL ,
  `election_aggregation_verification` BLOB NULL ,
  `election_decrypted_aggregated_votes` BLOB NULL ,
  `votes_count` INT NULL ,
  PRIMARY KEY (`election_id`) )
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `EVIV_BulletinBoard`.`Elections_has_Voters`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `EVIV_BulletinBoard`.`Elections_has_Voters` ;

SHOW WARNINGS;
CREATE  TABLE IF NOT EXISTS `EVIV_BulletinBoard`.`Elections_has_Voters` (
  `Elections_election_id` VARCHAR(8) NOT NULL ,
  `Voters_voter_id` VARCHAR(10) NOT NULL ,
  PRIMARY KEY (`Elections_election_id`, `Voters_voter_id`) ,
  CONSTRAINT `fk_Elections_has_Voters_Elections`
    FOREIGN KEY (`Elections_election_id` )
    REFERENCES `EVIV_BulletinBoard`.`Elections` (`election_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Elections_has_Voters_Voters1`
    FOREIGN KEY (`Voters_voter_id` )
    REFERENCES `EVIV_BulletinBoard`.`Voters` (`voter_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;
CREATE INDEX `fk_Elections_has_Voters_Voters` ON `EVIV_BulletinBoard`.`Elections_has_Voters` (`Voters_voter_id` ASC) ;

SHOW WARNINGS;
CREATE INDEX `fk_Elections_has_Voters_Elections` ON `EVIV_BulletinBoard`.`Elections_has_Voters` (`Elections_election_id` ASC) ;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `EVIV_BulletinBoard`.`Candidates`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `EVIV_BulletinBoard`.`Candidates` ;

SHOW WARNINGS;
CREATE  TABLE IF NOT EXISTS `EVIV_BulletinBoard`.`Candidates` (
  `candidate_id` INT NOT NULL ,
  `Elections_election_id` VARCHAR(8) NOT NULL ,
  `candidate_info` VARCHAR(45) NULL ,
  PRIMARY KEY (`candidate_id`, `Elections_election_id`) ,
  CONSTRAINT `fk_Candidates_Elections1`
    FOREIGN KEY (`Elections_election_id` )
    REFERENCES `EVIV_BulletinBoard`.`Elections` (`election_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;
CREATE INDEX `fk_Candidates_Elections` ON `EVIV_BulletinBoard`.`Candidates` (`Elections_election_id` ASC) ;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `EVIV_BulletinBoard`.`Ballots`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `EVIV_BulletinBoard`.`Ballots` ;

SHOW WARNINGS;
CREATE  TABLE IF NOT EXISTS `EVIV_BulletinBoard`.`Ballots` (
  `ballot_id` VARCHAR(10) NOT NULL ,
  `url` VARCHAR(60) NOT NULL ,
  PRIMARY KEY (`ballot_id`) )
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `EVIV_BulletinBoard`.`Votes`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `EVIV_BulletinBoard`.`Votes` ;

SHOW WARNINGS;
CREATE  TABLE IF NOT EXISTS `EVIV_BulletinBoard`.`Votes` (
  `Elections_election_id` VARCHAR(8) NOT NULL ,
  `Voters_voter_id` VARCHAR(10) NOT NULL ,
  `vote_ballot` VARCHAR(45) NULL ,
  `vote_info` BLOB NULL ,
  `vote_token` VARCHAR(10) NULL ,
  `vote_receipt_token` VARCHAR(50) NULL ,
  `vote_time` DATETIME NULL ,
  `Ballots_ballot_id` VARCHAR(10) NOT NULL ,
  PRIMARY KEY (`Elections_election_id`, `Voters_voter_id`) ,
  CONSTRAINT `fk_Votes_Elections1`
    FOREIGN KEY (`Elections_election_id` )
    REFERENCES `EVIV_BulletinBoard`.`Elections` (`election_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Votes_Voters1`
    FOREIGN KEY (`Voters_voter_id` )
    REFERENCES `EVIV_BulletinBoard`.`Voters` (`voter_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Votes_Ballots1`
    FOREIGN KEY (`Ballots_ballot_id` )
    REFERENCES `EVIV_BulletinBoard`.`Ballots` (`ballot_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;
CREATE INDEX `fk_Votes_Elections` ON `EVIV_BulletinBoard`.`Votes` (`Elections_election_id` ASC) ;

SHOW WARNINGS;
CREATE INDEX `fk_Votes_Voters` ON `EVIV_BulletinBoard`.`Votes` (`Voters_voter_id` ASC) ;

SHOW WARNINGS;
CREATE INDEX `fk_Votes_Ballots` ON `EVIV_BulletinBoard`.`Votes` (`Ballots_ballot_id` ASC) ;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `EVIV_BulletinBoard`.`Trustees`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `EVIV_BulletinBoard`.`Trustees` ;

SHOW WARNINGS;
CREATE  TABLE IF NOT EXISTS `EVIV_BulletinBoard`.`Trustees` (
  `trustee_id` VARCHAR(10) NOT NULL ,
  `trustee_certificate` VARCHAR(1024) NULL ,
  PRIMARY KEY (`trustee_id`) )
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `EVIV_BulletinBoard`.`Elections_has_Trustees`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `EVIV_BulletinBoard`.`Elections_has_Trustees` ;

SHOW WARNINGS;
CREATE  TABLE IF NOT EXISTS `EVIV_BulletinBoard`.`Elections_has_Trustees` (
  `Elections_election_id` VARCHAR(8) NOT NULL ,
  `Trustees_idTrustees` VARCHAR(10) NOT NULL ,
  PRIMARY KEY (`Elections_election_id`, `Trustees_idTrustees`) ,
  CONSTRAINT `fk_Elections_has_Trustees_Elections1`
    FOREIGN KEY (`Elections_election_id` )
    REFERENCES `EVIV_BulletinBoard`.`Elections` (`election_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Elections_has_Trustees_Trustees1`
    FOREIGN KEY (`Trustees_idTrustees` )
    REFERENCES `EVIV_BulletinBoard`.`Trustees` (`trustee_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;
CREATE INDEX `fk_Elections_has_Trustees_Trustees` ON `EVIV_BulletinBoard`.`Elections_has_Trustees` (`Trustees_idTrustees` ASC) ;

SHOW WARNINGS;
CREATE INDEX `fk_Elections_has_Trustees_Elections` ON `EVIV_BulletinBoard`.`Elections_has_Trustees` (`Elections_election_id` ASC) ;

SHOW WARNINGS;
USE `EVIV_ElectoralComission` ;

-- -----------------------------------------------------
-- Table `EVIV_ElectoralComission`.`Citizens`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `EVIV_ElectoralComission`.`Citizens` ;

SHOW WARNINGS;
CREATE  TABLE IF NOT EXISTS `EVIV_ElectoralComission`.`Citizens` (
  `CitizenID` INT NOT NULL ,
  `Name` VARCHAR(45) NULL ,
  `Address` VARCHAR(45) NULL ,
  PRIMARY KEY (`CitizenID`) )
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `EVIV_ElectoralComission`.`Ballots`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `EVIV_ElectoralComission`.`Ballots` ;

SHOW WARNINGS;
CREATE  TABLE IF NOT EXISTS `EVIV_ElectoralComission`.`Ballots` (
  `ballot_id` VARCHAR(10) NOT NULL ,
  `url` VARCHAR(60) NOT NULL ,
  PRIMARY KEY (`ballot_id`) )
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `EVIV_ElectoralComission`.`Voters`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `EVIV_ElectoralComission`.`Voters` ;

SHOW WARNINGS;
CREATE  TABLE IF NOT EXISTS `EVIV_ElectoralComission`.`Voters` (
  `VoterID` VARCHAR(10) NOT NULL ,
  `Certificate` VARCHAR(200) NULL ,
  `CertificateFile` BLOB NULL ,
  `Ballots_ballot_id` VARCHAR(10) NOT NULL ,
  `Citizens_CitizenID` INT NOT NULL ,
  PRIMARY KEY (`VoterID`, `Ballots_ballot_id`, `Citizens_CitizenID`) ,
  CONSTRAINT `fk_Voters_Ballots1`
    FOREIGN KEY (`Ballots_ballot_id` )
    REFERENCES `EVIV_ElectoralComission`.`Ballots` (`ballot_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Voters_Citizens1`
    FOREIGN KEY (`Citizens_CitizenID` )
    REFERENCES `EVIV_ElectoralComission`.`Citizens` (`CitizenID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;
CREATE INDEX `fk_Voters_Ballots` ON `EVIV_ElectoralComission`.`Voters` (`Ballots_ballot_id` ASC) ;

SHOW WARNINGS;
CREATE INDEX `fk_Voters_Citizens` ON `EVIV_ElectoralComission`.`Voters` (`Citizens_CitizenID` ASC) ;

SHOW WARNINGS;
USE `EVIV_BallotBox` ;

-- -----------------------------------------------------
-- Table `EVIV_BallotBox`.`Votes`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `EVIV_BallotBox`.`Votes` ;

SHOW WARNINGS;
CREATE  TABLE IF NOT EXISTS `EVIV_BallotBox`.`Votes` (
  `electionID` VARCHAR(8) NOT NULL ,
  `voterID` VARCHAR(10) NOT NULL ,
  `castDate` DATETIME NULL ,
  `vote` BLOB NULL ,
  `receipt` BLOB NULL ,
  `sent2BB` TINYINT(1) NULL ,
  PRIMARY KEY (`electionID`, `voterID`) )
ENGINE = InnoDB;

SHOW WARNINGS;
USE `EVIV_ElectionRegistrar` ;

-- -----------------------------------------------------
-- Table `EVIV_ElectionRegistrar`.`RegisteredVoters`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `EVIV_ElectionRegistrar`.`RegisteredVoters` ;

SHOW WARNINGS;
CREATE  TABLE IF NOT EXISTS `EVIV_ElectionRegistrar`.`RegisteredVoters` (
  `electionID` VARCHAR(8) NOT NULL ,
  `voterID` VARCHAR(10) NOT NULL ,
  `registryDate` DATETIME NULL ,
  `sent2BB` TINYINT(1) NOT NULL DEFAULT false ,
  PRIMARY KEY (`electionID`, `voterID`) )
ENGINE = InnoDB;

SHOW WARNINGS;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `EVIV_BulletinBoard`.`Ballots`
-- -----------------------------------------------------
START TRANSACTION;
USE `EVIV_BulletinBoard`;
INSERT INTO `EVIV_BulletinBoard`.`Ballots` (`ballot_id`, `url`) VALUES ('0', 'http://localhost:8080/BallotBox/BallotBox');

COMMIT;

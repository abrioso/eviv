USE `EVIV_BulletinBoard`;
DELETE FROM `Votes`;
DELETE FROM `Elections_has_Voters`;
DELETE FROM `Voters`;
DELETE FROM `Candidates`;
DELETE FROM `Elections`;

USE `EVIV_ElectionRegistrar`;
DELETE FROM `RegisteredVoters`;

USE `EVIV_BallotBox`;
DELETE FROM `Votes`;


/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eviv.ecadminhsm;

import eviv.rmi.HSMSimulRemote;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.rmi.RemoteException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Security;
import java.security.SignatureException;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPrivateKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Date;
import java.util.Hashtable;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bouncycastle.jce.X509Principal;
import org.bouncycastle.openssl.PEMReader;
import org.bouncycastle.openssl.PasswordFinder;
import org.bouncycastle.x509.X509V3CertificateGenerator;

/**
 *
 * @author abrioso
 */
public class HSMSimulImpl implements HSMSimulRemote {

    private X509Certificate cert;
    private PrivateKey kpriv;

    public HSMSimulImpl(String certFile, String keyFile) {

        InputStream inStream;
        try {
            inStream = new FileInputStream(certFile);
            CertificateFactory cf = CertificateFactory.getInstance("X.509", "BC");
            this.cert = ((X509Certificate) cf.generateCertificate(inStream));
            inStream.close();
            
            System.out.println("Certificado lido: \n" + cert.toString());
            
            
            inStream = new FileInputStream(keyFile);
//            PrivateKeyFactory kprivFact = PrivateKeyFactory.createKey(inStream);
  
            KeyFactory kf = KeyFactory.getInstance("X.509", "BC");
  
            byte[] kprivBytes = new byte[(int) keyFile.length()];
  //          inStream.read(kprivBytes);
            
            //this.kpriv = kf.generatePrivate(new X509EncodedKeySpec(kprivBytes));

            
 //           DataInputStream dis = new DataInputStream(inStream);
//.readFully(kprivBytes);
            
 //           PEMReader pemr = new PEMReader(dis);
            
 //           KeyPair kp = (KeyPair) new PEMReader(br).readObject();
   
            BufferedReader br = new BufferedReader(new FileReader(keyFile));
            
    //        final char[] pwd = "evivec".toCharArray();
            
            
            
            KeyPair kp = (KeyPair) new PEMReader(br, new PasswordFinder() {

                @Override
                public char[] getPassword() {
                    return "evivec".toCharArray();
                }
            }).readObject();
            
            
            this.kpriv = kp.getPrivate();
            
//            this.kpriv = kf.generatePrivate(new X509EncodedKeySpec(kprivBytes));
 //           this.kpriv = kf.generatePrivate(new X509EncodedKeySpec(kprivBytes));

            
//            this.kpriv = kf.generatePrivate(PrivateKeyFactory.createKey(inStream));
  
                       
            inStream.close();
            
            
            System.out.println("Chave lida: \n" + kpriv.toString());


        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(HSMSimulImpl.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchProviderException ex) {
            Logger.getLogger(HSMSimulImpl.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(HSMSimulImpl.class.getName()).log(Level.SEVERE, null, ex);
        } catch (CertificateException ex) {
            Logger.getLogger(HSMSimulImpl.class.getName()).log(Level.SEVERE, null, ex);
        }



    }

    @Override
    public X509Certificate getCertificate() throws RemoteException {
        return this.cert;
    }

    @Override
    public X509Certificate createCertificate(String voterID, PublicKey kpub) throws RemoteException {
   
        X509Certificate voterCert = null;
        
 //       X509v3CertificateBuilder
   
        //Mudar de Hastable para outra Collection (HashTable deprecated)
        Hashtable attrs = new Hashtable();
        
        attrs.put(X509Principal.C,"PT");
        attrs.put(X509Principal.O,"EVIV");
        attrs.put(X509Principal.L,"Lisbon");
        attrs.put(X509Principal.ST,"Lisbon");
      //  attrs.put(X509Principal.E,"eviv@ist.utl.pt");
        attrs.put(X509Principal.OU,"Voters");
        attrs.put(X509Principal.CN,voterID);

    //TODO: Substitutir X509V3CertificateGenerator (Deprescated) por X509v3CertificateBuilder      
    //    X509v3CertificateBuilder certBuild = new X509v3CertificateBuilder(null, BigInteger.ONE, null, null, null, null) 
        
        
        X509V3CertificateGenerator certGen = new X509V3CertificateGenerator();
        
        //TODO: SerialNumber unico deve ser incrementado (por servico externo?)
        certGen.setSerialNumber(BigInteger.valueOf(1));
        
  //      certGen.setIssuerDN(PrincipalUtil.getSubjectX509Principal(cert));
        certGen.setIssuerDN(cert.getSubjectX500Principal());
    
        //TODO: Corrigir as datas
        certGen.setNotBefore(new Date(System.currentTimeMillis() - 50000));
        certGen.setNotAfter(new Date(System.currentTimeMillis() + 50000));
        
        certGen.setSubjectDN(new X509Principal(attrs));
        
        certGen.setPublicKey(kpub);
        
        //TODO: Confirmar Algoritmo
        certGen.setSignatureAlgorithm("SHA256WithRSAEmcryption");
        try {
            cert = certGen.generate(kpriv);
        } catch (CertificateEncodingException ex) {
            Logger.getLogger(HSMSimulImpl.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalStateException ex) {
            Logger.getLogger(HSMSimulImpl.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(HSMSimulImpl.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SignatureException ex) {
            Logger.getLogger(HSMSimulImpl.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidKeyException ex) {
            Logger.getLogger(HSMSimulImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return cert;
        
    }
    
    
    //TODO: APAGAR!!
    @Override
    public X509Certificate signCertificate(Certificate crt) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

   
}

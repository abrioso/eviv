/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eviv.ecadminhsm;

import eviv.rmi.HSMSimulRemote;
import java.awt.FileDialog;
import java.awt.Frame;
import java.rmi.AccessException;
import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author abrioso
 */
public class ECAdminHSMSimul {
    
    

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
         FileDialog fd = new FileDialog( new Frame(), 
        "Open Cert file...", FileDialog.LOAD );
      fd.setVisible(true);
      String certFilePath = fd.getDirectory() + fd.getFile();
      
      fd = new FileDialog( new Frame(), 
        "Open Key file...", FileDialog.LOAD );
      fd.setVisible(true);
      String keyFilePath = fd.getDirectory() + fd.getFile();
      
      
      
      System.out.println("Openning Cert file " + certFilePath 
              + "\n Openning Key file " + keyFilePath);
      
      java.security.Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());

     
        try {
            HSMSimulImpl hsm = new HSMSimulImpl(certFilePath, keyFilePath);
            HSMSimulRemote hsmRemote = (HSMSimulRemote) UnicastRemoteObject.exportObject(hsm, 0);
            Registry registry = LocateRegistry.createRegistry(HSMSimulRemote.REGISTRY_PORT);
   //         Registry registry = LocateRegistry.getRegistry();
            registry.bind(HSMSimulRemote.REGISTRY_NAME, hsmRemote);
        } catch (AlreadyBoundException ex) {
            Logger.getLogger(ECAdminHSMSimul.class.getName()).log(Level.SEVERE, null, ex);
        } catch (AccessException ex) {
            Logger.getLogger(ECAdminHSMSimul.class.getName()).log(Level.SEVERE, null, ex);
        } catch (RemoteException ex) {
            Logger.getLogger(ECAdminHSMSimul.class.getName()).log(Level.SEVERE, null, ex);
        }
      
        
        //Teste...
        
            Registry registry;
        try {
            registry = LocateRegistry.getRegistry(HSMSimulRemote.REGISTRY_PORT);
            HSMSimulRemote hsmRemote = (HSMSimulRemote) registry.lookup(HSMSimulRemote.REGISTRY_NAME);
            
            System.out.println("Certificado Activo: \n" + hsmRemote.getCertificate().toString());
            
        } catch (NotBoundException ex) {
            Logger.getLogger(ECAdminHSMSimul.class.getName()).log(Level.SEVERE, null, ex);
        } catch (AccessException ex) {
            Logger.getLogger(ECAdminHSMSimul.class.getName()).log(Level.SEVERE, null, ex);
        } catch (RemoteException ex) {
            Logger.getLogger(ECAdminHSMSimul.class.getName()).log(Level.SEVERE, null, ex);
        }
            
        
        
    }
}
